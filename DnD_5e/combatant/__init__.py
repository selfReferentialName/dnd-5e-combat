import warnings
from typing import Optional
from DnD_5e import armory, attack_class, weapons
from DnD_5e.utility_methods_dnd import ability_to_mod, validate_dice, roll_dice, cr_to_xp, calc_advantage, proficency_bonus_by_cr, \
TYPE_ROLL_RESULT, TYPE_DICE_TUPLE

class Combatant:
    def __init__(self, **kwargs):
        combatant_copy = kwargs.get("copy")
        if combatant_copy:
            self.copy_constructor(combatant_copy, name=kwargs.get("name"))
            return

        self._name = kwargs.get('name')
        if not self._name:
            raise ValueError("Must provide a name")

        self._verbose = kwargs.get("verbose", False)

        vulnerabilities = kwargs.get("vulnerabilities", set())
        if isinstance(vulnerabilities, list):
            vulnerabilities = set(vulnerabilities)
        elif not isinstance(vulnerabilities, set):
            raise ValueError("Vulnerabilities must be a set (or a list to convert to a set)")
        self._vulnerabilities = vulnerabilities

        resistances = kwargs.get("resistances", set())
        if isinstance(resistances, list):
            resistances = set(resistances)
        elif not isinstance(resistances, set):
            raise ValueError("Resistances must be a set (or a list to convert to a set)")
        if not resistances.isdisjoint(self._vulnerabilities):
            raise ValueError("Cannot have duplicate items across vulnerabilities and resistances")
        self._resistances = resistances

        immunities = kwargs.get("immunities", set())
        if isinstance(immunities, list):
            immunities = set(immunities)
        elif not isinstance(immunities, set):
            raise ValueError("Immunities must be a set (or a list to convert to a set)")
        if not immunities.isdisjoint(self._vulnerabilities):
            raise ValueError("Cannot have duplicate items across vulnerabilities and immunities")
        if not immunities.isdisjoint(self._resistances):
            raise ValueError("Cannot have duplicate items across resistances and immunities")
        self._immunities = immunities

        self._ac = kwargs.get('ac')
        if not self._ac or not isinstance(self._ac, int) or self._ac < 1:  # TODO: include threshold
            raise ValueError("Must provide ac as a positive integer")
        self._max_hp = kwargs.get('max_hp')
        if not self._max_hp or not isinstance(self._max_hp, int) or self._max_hp <= 0:
            raise ValueError("Must provide positive max hp")
        self._temp_hp = kwargs.get('temp_hp', 0)
        if not isinstance(self._temp_hp, int) or self._temp_hp < 0:
            raise ValueError("Temp hp must be a non-negative integer")

        self._hit_dice = validate_dice(kwargs.get("hit_dice"))

        self._conditions = kwargs.get('conditions', [])  # set this first in case current hp makes character unconscious
        if not isinstance(self._conditions, list):
            raise ValueError("If conditions provided, must be a list")

        self._current_hp = kwargs.get('current_hp', self._max_hp)  # by default, start with max hp
        if not isinstance(self._current_hp, int):
            raise ValueError("Must provide non-negative integer for current hp")
        if self._current_hp <= 0:
            warnings.warn("Combatant created with 0 or less hp. Going unconscious (and setting hp to 0).")
            self.become_unconscious()
        if self._current_hp > self._max_hp:
            raise ValueError("Current hp cannot be greater than max hp. Use temp hp if needed.")

        # self._hit_dice = validate_dice(kwargs.get('hit_dice'))  # TODO: NOT USED CURRENTLY.
        self._speed = kwargs.get('speed', 25)
        if not isinstance(self._speed, int) or self._speed <= 0:
            raise ValueError("Speed must be a positive integer")

        self._vision = kwargs.get('vision', 'normal')
        if self._vision not in ['normal', 'darkvision', 'blindsight', 'truesight']:
            print("%s not recognized as a valid vision. Setting vision to normal." % self._vision)
            self._vision = "normal"

        strength = kwargs.get('strength')
        if not strength:
            strength_mod = kwargs.get("strength_mod")  # modifiers also accepted
            if strength_mod is not None:
                if isinstance(strength_mod, int):
                    self._strength = strength_mod
                else:
                    raise ValueError("Strength mod must be an integer")
            else:
                raise ValueError("Must provide strength score or modifier")
        else:
            self._strength = ability_to_mod(strength)

        dexterity = kwargs.get('dexterity')
        if not dexterity:
            dexterity_mod = kwargs.get("dexterity_mod")
            if dexterity_mod is not None:
                if isinstance(dexterity_mod, int):
                    self._dexterity = dexterity_mod
                else:
                    raise ValueError("Dexterity mod must be an integer")
            else:
                raise ValueError("Must provide dexterity score or modifier")
        else:
            self._dexterity = ability_to_mod(dexterity)

        constitution = kwargs.get('constitution')
        if not constitution:
            constitution_mod = kwargs.get("constitution_mod")
            if constitution_mod is not None:
                if isinstance(constitution_mod, int):
                    self._constitution = constitution_mod
                else:
                    raise ValueError("Constitution mod must be an integer")
            else:
                raise ValueError("Must provide constitution score or modifier")
        else:
            self._constitution = ability_to_mod(constitution)

        intelligence = kwargs.get('intelligence')
        if not intelligence:
            intelligence_mod = kwargs.get("intelligence_mod")
            if intelligence_mod is not None:
                if isinstance(intelligence_mod, int):
                    self._intelligence = intelligence_mod
                else:
                    raise ValueError("Intelligence mod must be an integer")
            else:
                raise ValueError("Must provide intelligence score or modifier")
        else:
            self._intelligence = ability_to_mod(intelligence)

        wisdom = kwargs.get('wisdom')
        if not wisdom:
            wisdom_mod = kwargs.get("wisdom_mod")
            if wisdom_mod is not None:
                if isinstance(wisdom_mod, int):
                    self._wisdom = wisdom_mod
                else:
                    raise ValueError("Wisdom mod must be an integer")
            else:
                raise ValueError("Must provide wisdom score or modifier")
        else:
            self._wisdom = ability_to_mod(wisdom)

        charisma = kwargs.get('charisma')
        if not charisma:
            charisma_mod = kwargs.get("charisma_mod")
            if charisma_mod is not None:
                if isinstance(charisma_mod, int):
                    self._charisma = charisma_mod
                else:
                    raise ValueError("Charisma mod must be an integer")
            else:
                raise ValueError("Must provide charisma score or modifier")
        else:
            self._charisma = ability_to_mod(charisma)

        self._proficiencies = kwargs.get('proficiencies', [])
        if isinstance(self._proficiencies, (list, tuple)):
            self._proficiencies = set(self._proficiencies)
        elif isinstance(self._proficiencies, set):
            pass
        else:
            raise ValueError("Proficiencies must be provided as a set, list, or tuple")

        self._proficiency_mod = kwargs.get("proficiency_mod", 0)
        if not isinstance(self._proficiency_mod, int) or self._proficiency_mod < 0:
            raise ValueError("Proficiency mod must be non-negative")

        self._saving_throws = {"strength": self._strength, "dexterity": self._dexterity, "constitution": self._constitution, "intelligence": self._intelligence,
                               "wisdom": self._wisdom, "charisma": self._charisma}
        for ability in self._saving_throws:
            if ability in self._proficiencies:
                self._saving_throws[ability] += self._proficiency_mod

        self._adv_to_be_hit = 0

        self._features = kwargs.get('features', set())
        if isinstance(self._features, (list, tuple)):
            self._features = set(self._features)
        elif isinstance(self._features, set):
            pass
        else:
            raise ValueError("Features must be a set (or a list or tuple to convert to a set)")

        self._death_saves = kwargs.get('death_saves', [])  # TODO: implement

        self._attacks = []

        self._weapons = []
        the_weapons = kwargs.get('weapons', [])
        if not isinstance(the_weapons, (list, tuple)):
            raise ValueError("Weapons must be a list or tuple of weapons")
        for weapon in the_weapons:
            self.add_weapon(weapon)

        self._size = kwargs.get("size", "medium")
        if self._size not in ["tiny", "small", "medium", "large", "huge", "gargantuan"]:
            raise ValueError("Size should be tiny, small, medium, large, huge, or gargantuan")

        self._items = kwargs.get('items', [])  # TODO: implement

    def copy_constructor(self, other, **kwargs):
        if not isinstance(other, Combatant):
            raise ValueError("Cannot make self a copy of something that is not a Combatant")

        name = kwargs.get("name")
        if not name or not isinstance(name, str):
            name = other.get_name()

        Combatant.__init__(self=self, verbose=other.get_verbose(), max_hp=other.get_max_hp(), temp_hp=other.get_temp_hp(),
                      conditions=other.get_conditions().copy(), current_hp=other.get_current_hp(),
                      speed=other.get_speed(), vision=other.get_vision(), strength_mod=other.get_strength(),
                      dexterity_mod=other.get_dexterity(), constitution_mod=other.get_constitution(),
                      intelligence_mod=other.get_intelligence(), wisdom_mod=other.get_wisdom(), charisma_mod=other.get_charisma(),
                      proficiencies=other.get_proficiencies().copy(), proficiency_mod=other.get_proficiency_mod(),
                      vulnerabilities=other.get_vulnerabilities().copy(), resistances=other.get_resistances().copy(),
                      immunities=other.get_immunities().copy(), features=other.get_features().copy(),
                      death_saves=other.get_death_saves(), ac=other.get_ac(), hit_dice=other.get_hit_dice(),
                      size=other.get_size(), name=name)

        self._adv_to_be_hit = 0

        for weapon in other.get_weapons():
            self.add_weapon(weapons.Weapon(copy=weapon))

    def get_copy(self, name=""):
        copy_obj = type(self)(copy=self, name=name)
        return copy_obj

    def get_ac(self) -> int:
        return self._ac

    def get_hit_dice(self) -> TYPE_DICE_TUPLE:
        return self._hit_dice

    def get_max_hp(self) -> int:
        return self._max_hp

    def get_temp_hp(self) -> int:
        return self._temp_hp

    def get_current_hp(self) -> int:
        return self._current_hp

    def is_bloodied(self) -> bool:
        return self.get_current_hp() <= self.get_max_hp() // 2

    def is_hp_max(self) -> bool:
        return self.get_current_hp() == self.get_max_hp()

    def get_hp_to_max(self) -> int:
        return self.get_max_hp() - self.get_current_hp()

    def get_speed(self) -> int:
        return self._speed

    def get_conditions(self) -> set:
        return self._conditions

    def has_condition(self, condition) -> bool:
        return condition in self._conditions

    def get_vision(self) -> str:
        return self._vision

    def can_see(self, light_src: str) -> bool:  # pylint: disable=inconsistent-return-statements
        if light_src not in ["normal", "dark", "magic"]:
            raise ValueError("Light source must be normal, dark, or magic")
        if self.has_condition("blinded"):
            return self.get_vision() == "blindsight" and light_src != "magic"
        if light_src == "normal":
            return True
        if self._vision == "normal":  # normal vision can't see anything better than normal light
            return False
        if light_src == "dark":  # darkvision, blindsight, and truesight can all see in the dark
            return True
        if light_src == "magic":
            return self._vision == "truesight"

    def get_ability(self, ability: str) -> int:
        if ability == "strength":
            return self.get_strength()
        if ability == "dexterity":
            return self.get_dexterity()
        if ability == "constitution":
            return self.get_constitution()
        if ability == "intelligence":
            return self.get_intelligence()
        if ability == "wisdom":
            return self.get_wisdom()
        if ability == "charisma":
            return self.get_charisma()
        raise ValueError("Ability score must be strength, dexterity, constitution, intelligence, wisdom, or charisma")

    def get_strength(self) -> int:
        return self._strength

    def get_dexterity(self) -> int:
        return self._dexterity

    def get_constitution(self) -> int:
        return self._constitution

    def get_intelligence(self) -> int:
        return self._intelligence

    def get_wisdom(self) -> int:
        return self._wisdom

    def get_charisma(self) -> int:
        return self._charisma

    def get_proficiencies(self) -> set:
        return self._proficiencies

    def has_proficiency(self, item: str) -> bool:
        return item in self.get_proficiencies()

    def get_proficiency_mod(self) -> int:
        return self._proficiency_mod

    def has_weapon_proficiency(self, weapon: weapons.Weapon) -> bool:
        if not isinstance(weapon, weapons.Weapon):
            raise ValueError("Must provide a weapon to calculate weapon proficiency")
        if isinstance(weapon, armory.SimpleWeapon) and self.has_proficiency("simple weapons"):
            return True
        if isinstance(weapon, armory.MartialWeapon) and self.has_proficiency("martial weapons"):
            return True
        if self.has_proficiency("monk weapons") and armory.is_monk_weapon(weapon):
            return True
        return self.has_proficiency(type(weapon).__name__.lower().replace("_", " "))

    def get_vulnerabilities(self) -> set:
        return self._vulnerabilities

    def is_vulnerable(self, thing) -> bool:
        return thing in self.get_vulnerabilities()

    def get_resistances(self) -> set:
        return self._resistances

    def is_resistant(self, thing) -> bool:
        return thing in self.get_resistances()

    def get_immunities(self) -> set:
        return self._immunities

    def is_immune(self, thing) -> bool:
        return thing in self.get_immunities()

    def get_saving_throw(self, ability: str) -> int:
        try:
            return self._saving_throws[ability]
        except KeyError:
            raise ValueError("Asked for a saving throw that is not an ability score")

    def get_adv_to_be_hit(self) -> int:
        return calc_advantage([self._adv_to_be_hit])

    def get_features(self) -> set:
        return self._features

    def has_feature(self, feature: str) -> bool:
        return feature in self.get_features()

    def get_fighting_styles(self) -> set:
        if not self.has_feature("fighting style"):
            return None
        try:
            return self._fighting_styles
        except AttributeError:
            self._fighting_styles = set()  # pylint: disable=attribute-defined-outside-init
            return self._fighting_styles

    def has_fighting_style(self, fighting_style: str) -> bool:
        if not self.has_feature("fighting style"):
            return False
        return fighting_style in self.get_fighting_styles()

    def get_death_saves(self):
        return self._death_saves

    def get_weapons(self) -> list:
        return self._weapons

    def get_size(self) -> str:
        return self._size

    def get_items(self):
        return self._items

    def get_attacks(self) -> list:
        return self._attacks

    def get_name(self) -> str:
        return self._name

    def get_verbose(self) -> bool:
        return self._verbose

    def set_ac(self, ac: int):
        if not isinstance(ac, int):
            raise ValueError("Ac must be an integer")
        self._ac = ac

    def set_temp_hp(self, hp: int):
        if not isinstance(hp, int) or hp < 0:
            raise ValueError("%s temp hp must be a non-negative integer" % self._name)
        self._temp_hp = hp

    def set_verbose(self, val):
        self._verbose = bool(val)

    def add_vulnerability(self, item):
        self._vulnerabilities.add(item)

    def remove_vulnerability(self, item):
        if item in self._vulnerabilities:
            self._vulnerabilities.remove(item)
        # TODO: raise exception on else?

    def add_resistance(self, item):
        self._resistances.add(item)

    def remove_resistance(self, item):
        if item in self._resistances:
            self._resistances.remove(item)
        # TODO: raise exception on else?

    def add_immunity(self, item):
        self._immunities.add(item)

    def remove_immunity(self, item):
        if item in self._immunities:
            self._immunities.remove(item)
        # TODO: raise exception on else?

    def modify_adv_to_be_hit(self, adv: int):
        if adv in [-1, 0, 1]:
            self._adv_to_be_hit += adv
        else:
            raise ValueError("Advantage must be -1, 0, or 1")

    def add_feature(self, feature: str):
        self._features.add(feature)

    def add_fighting_style(self, fighting_style: str):
        if not self.has_feature("fighting style"):
            raise ValueError("%s cannot add a fighting style because they do not have this feature" % self._name)
        if fighting_style not in ["archery", "defense", "dueling", "great weapon fighting", "protection",
                                  "two-weapon fighting"]:
            raise ValueError(
                'Fighting style must be in "archery", "defense", "dueling", "great weapon fighting", "protection", "two-weapon fighting"')
        if self.has_fighting_style(fighting_style):
            raise ValueError("%s cannot add the same fighting style (%s) twice" % (self._name, fighting_style))
        self.get_fighting_styles().add(fighting_style)

    def add_weapon(self, weapon: weapons.Weapon):
        if not isinstance(weapon, weapons.Weapon):
            raise ValueError("%s tried to add a non-weapon as a weapon" % self._name)
        if weapon in self._weapons:
            warnings.warn("You (%s) already owns weapon %s" %(self._name, weapon.get_name()))
        owner = weapon.get_owner()
        if owner:
            raise ValueError("Weapon %s is owned by %s. Remove it from them first." % (weapon.get_name(), owner.get_name()))
        self._weapons.append(weapon)
        weapon.set_owner(self)
        self.add_weapon_attacks(weapon)
        if self.get_verbose():  # pragma: no cover
            print("%s adds %s weapon" % (self._name, weapon.get_name()))

    def remove_weapon(self, weapon: weapons.Weapon):
        if weapon not in self.get_weapons():
            raise ValueError("%s tried to remove a weapon they don't have" % self._name)
        self._weapons.remove(weapon)
        weapon.set_owner(None)  # Dobby is a free weapon!
        self.remove_weapon_attacks(weapon)
        if self.get_verbose():  # pragma: no cover
            print("%s removes %s weapon" % (self._name, weapon.get_name()))

    def remove_all_weapons(self):
        to_remove = self._weapons.copy()  # make a copy so I can iterate through the list
        for weapon in to_remove:
            self.remove_weapon(weapon)

    def add_weapon_attacks(self, weapon: weapons.Weapon):
        # Warning: if a Combatant has multiple weapons with the exact same name,
            # problems will arise because weapon attack names will not be unique
        if weapon not in self.get_weapons():
            raise ValueError("%s cannot add attacks for a weapon they don't own" % self._name)
        if weapon.has_prop("finesse"):
            mod = max(self._strength, self._dexterity)
        else:
            mod = self._strength
        attack_mod = weapon.get_attack_bonus() + mod
        if self.has_weapon_proficiency(weapon):
            attack_mod += self._proficiency_mod
        damage_mod = weapon.get_damage_bonus() + mod
        if weapon.get_range():
            if self.has_fighting_style("archery") and isinstance(weapon, armory.RangedWeapon):
                range_attack_mod = attack_mod + 2
            else:
                range_attack_mod = attack_mod
            self.add_attack(attack_class.Attack(damage_dice=weapon.get_damage_dice(), attack_mod=range_attack_mod, damage_mod=damage_mod,
                                                name="%s_range" % weapon.get_name(), damage_type=weapon.get_damage_type(),
                                                range=weapon.get_range()[0], weapon=weapon))
            self.add_attack(attack_class.Attack(damage_dice=weapon.get_damage_dice(), attack_mod=range_attack_mod, damage_mod=damage_mod,
                                                name="%s_range_disadvantage" % weapon.get_name(), damage_type=weapon.get_damage_type(),
                                                range=weapon.get_range()[1], adv=-1, weapon=weapon))
        if weapon.get_melee_range():
            self._attacks.append(
                attack_class.Attack(damage_dice=weapon.get_damage_dice(), attack_mod=attack_mod, damage_mod=damage_mod,
                                    name="%s_melee" % weapon.get_name(), damage_type=weapon.get_damage_type(),
                                    melee_range=weapon.get_melee_range(), weapon=weapon))
        if weapon.get_versatile():
            self._attacks.append(
                attack_class.Attack(damage_dice=weapon.get_versatile(), attack_mod=attack_mod, damage_mod=damage_mod,
                                    name="%s_versatile" % weapon.get_name(), damage_type=weapon.get_damage_type(),
                                    melee_range=weapon.get_melee_range(), weapon=weapon))

    def add_attack(self, attack: attack_class.Attack):
        if attack in self._attacks:
            pass
        self._attacks.append(attack)

    def remove_weapon_attacks(self, weapon: weapons.Weapon):
        self._attacks[:] = [attack for attack in self._attacks if attack.get_weapon() is not weapon]

    def add_condition(self, condition: str):
        if not isinstance(condition, str):
            raise ValueError("Condition must be a string")
        if condition not in self._conditions:
            if self.is_immune(condition):
                if self.get_verbose():  # pragma: no cover
                    print("%s is immune to the %s condition" % (self._name, condition))
                return
            self._conditions.append(condition)
            if self.get_verbose():  # pragma: no cover
                print("%s is now %s" % (self._name, condition))

    def remove_condition(self, condition: str):
        try:
            self._conditions.remove(condition)
            if self.get_verbose():  # pragma: no cover
                print("%s is no longer %s" % (self._name, condition))
        except ValueError:
            if self.get_verbose():  # pragma: no cover
                print("%s didn't have %s to begin with" % (self._name, condition))
            # TODO: change later?

    def change_vision(self, vision: str):  # in case of special vision changing magic? also useful for testing
        if vision in ["normal", "darkvision", "blindsight", "truesight"]:
            self._vision = vision
            if self.get_verbose():  # pragma: no cover
                print("%s changes vision to %s" % (self._name, vision))
        else:
            raise ValueError("Vision type not recognized")

    def set_size(self, size: str):
        if size not in ["tiny", "small", "medium", "large", "huge", "gargantuan"]:
            raise ValueError("Size must be tiny, small, medium, large, huge, or gargantuan")
        self._size = size

    def send_attack(self, target, attack: attack_class.Attack, adv=0) -> Optional[int]:
        try:
            adv = calc_advantage([adv, target.get_adv_to_be_hit()])
            return attack.make_attack(self, target, adv=adv)
        except NameError:
            raise ValueError("%s tried to make an attack with something that can't make attacks" % self._name)

    def take_attack(self, attack_result: TYPE_ROLL_RESULT, source=None, attack=None):  # pylint: disable=unused-argument
        hit_val, crit_val = attack_result
        if crit_val == -1:  # critical fails auto-miss
            return False
        if crit_val == 1:  # critical successes auto-hit
            return True
        return hit_val >= self.get_ac()

    def take_saving_throw(self, save_type: str, dc: int, attack=None, adv=0):  # pylint: disable=unused-argument
        adv = calc_advantage([adv])
        return self.make_saving_throw(save_type, adv) >= dc

    def make_saving_throw(self, save_type: str, adv=0) -> int:
        modifier = self.get_saving_throw(save_type)
        adv = calc_advantage([adv])
        result = roll_dice(dice_type=20, modifier=modifier, adv=adv)[0]
        if self.get_verbose():  # pragma: no cover
            print("%s rolls a %d for a %s saving throw." % (self._name, result, save_type), end=" ")
        return result

    def take_damage(self, damage: int, damage_type=None) -> int:
        if self.is_vulnerable(damage_type):
            if self.get_verbose():  # pragma: no cover
                print("%s is vulnerable to %s!" % (self._name, damage_type))
            damage *= 2
        elif self.is_resistant(damage_type):
            if self.get_verbose():  # pragma: no cover
                print("%s is resistant to %s." % (self._name, damage_type))
            damage //=2
        elif self.is_immune(damage_type):
            if self.get_verbose():  # pragma: no cover
                print("%s is immune to %s." % (self._name, damage_type))
                return 0
        if self.get_verbose():  # pragma: no cover
            print("%s takes %d damage" % (self._name, damage))
        damage_taken = damage
        if self.get_temp_hp():
            if damage <= self.get_temp_hp():
                self._temp_hp -= damage
                return damage_taken
            damage -= self.get_temp_hp()  # empty out temp hp
            self._temp_hp = 0
        self._current_hp -= damage
        if self.get_current_hp() <= self.get_max_hp() * -1:  # if remaining damage meets or exceeds your max hp
            self.die()
        elif self.get_current_hp() <= 0:
            self.become_unconscious()
        return damage_taken

    def take_healing(self, healing: int) -> int:
        if self.get_current_hp() + healing > self.get_max_hp():
            healing = self.get_max_hp() - self.get_current_hp()
        self._current_hp += healing
        if self.get_verbose():  # pragma: no cover
            print("%s is healed for %d." % (self._name, healing))
        if self.has_condition("unconscious"):
            self.remove_condition("unconscious")
        return healing

    def become_unconscious(self):
        self.add_condition("unconscious")
        self._current_hp = 0

    def die(self):
        self._conditions.clear()
        self.add_condition("dead")  # TODO: change death later?
        self._current_hp = 0
        self._temp_hp = 0

class Creature(Combatant):
    def __init__(self, **kwargs):
        # do this before super so that there aren't multiple calls to copy_constructor
        copy_creature = kwargs.get("copy")
        if copy_creature:
            self.copy_constructor(copy_creature, **kwargs)
            return
        self._cr = kwargs.get("cr", 0)
        if not isinstance(self._cr, (int, float)) or self._cr <= 0:
            raise ValueError("Challenge rating must be a non-negative number")
        self._xp = kwargs.get("xp", cr_to_xp(self._cr))
        proficiency_mod = proficency_bonus_by_cr(self._cr)
        kwargs.update({"proficiency_mod": proficiency_mod})
        self._creature_type = kwargs.get("creature_type")
        if self._creature_type not in ["aberration", "beast", "celestial", "construct", "dragon", "elemental", "fey",
                                       "fiend", "giant", "monstrosity", "ooze", "plant", "undead"]:
            if not isinstance(self._creature_type, str) or "humanoid" not in self._creature_type:
                raise ValueError('Invalid creature type. Valid types are "aberration", "beast", "celestial", "construct", '
                                 '"dragon", "elemental", "fey", "fiend", "giant", "monstrosity", "ooze", "plant", "undead",'
                                 'and various types of "humanoid"')
        super().__init__(**kwargs)

    def copy_constructor(self, other, **kwargs):  # pylint: disable=arguments-differ
        super().copy_constructor(other, **kwargs)
        if isinstance(other, Creature):
            self._cr = other.get_cr()
            self._xp = other.get_xp()
            self._creature_type = other.get_creature_type()
        else:
            cr = kwargs.get("cr", 0)
            xp = kwargs.get("xp", 0)
            if not isinstance(cr, (int, float)) or cr < 0:
                raise ValueError("Challenge rating must be a non-negative number")
            self._cr = cr
            if xp:
                if not isinstance(xp, int) or xp < 0:
                    raise ValueError("XP must be a non-negative integer")
                self._xp = xp
            else:
                self._xp = cr_to_xp(self._cr)
            self._creature_type = kwargs.get("creature_type")
            if self._creature_type not in ["aberration", "beast", "celestial", "construct", "dragon", "elemental",
                                           "fey", "fiend", "giant", "monstrosity", "ooze", "plant", "undead"]:
                if not isinstance(self._creature_type, str) or "humanoid" not in self._creature_type:
                    raise ValueError(
                        'Invalid creature type. Valid types are "aberration", "beast", "celestial", "construct", '
                        '"dragon", "elemental", "fey", "fiend", "giant", "monstrosity", "ooze", "plant", "undead",'
                        'and various types of "humanoid"')

    def get_cr(self) -> int:
        return self._cr

    def get_xp(self) -> int:
        return self._xp

    def get_creature_type(self):
        return self._creature_type

class Character(Combatant):
    def __init__(self, **kwargs):
        copy_character = kwargs.get("copy")
        if copy_character:
            self.copy_constructor(copy_character, name=kwargs.get("name"), level=kwargs.get("level"))
            return
        super().__init__(**kwargs)
        self._level = kwargs.get("level")
        if not self._level:
            raise ValueError("No level provided or level is 0")
        if not isinstance(self._level, int) or self._level < 1 or self._level > 20:
            raise ValueError("Level must be an integer between 1 and 20")

    def copy_constructor(self, other, **kwargs):
        super().copy_constructor(other, **kwargs)
        self._level = kwargs.get("level")
        if isinstance(other, Character):
            self._level = other.get_level()
        else:
            if not self._level:
                raise ValueError("No level provided or level is 0")
            if not isinstance(self._level, int) or self._level < 1 or self._level > 20:
                raise ValueError("Level must be an integer between 1 and 20")

    def get_level(self) -> int:
        return self._level

class SpellCaster(Combatant):
    def __init__(self, **kwargs):
        copy = kwargs.get("copy")
        if copy:
            self.copy_constructor(other=copy, **kwargs)
            return
        super().__init__(**kwargs)
        spell_ability = kwargs.get("spell_ability", "wisdom")
        try:
            self._spell_ability_mod = self.get_ability(spell_ability)
            self._spell_ability = spell_ability
        except ValueError:
            raise ValueError("Spell ability must be strength, dexterity, constitution, intelligence, wisdom, or charisma")
        self._spell_save_dc = 8 + self._proficiency_mod + self._spell_ability_mod
        self._spell_attack_mod = self._proficiency_mod + self._spell_ability_mod
        spell_slots = kwargs.get("spell_slots")
        if isinstance(spell_slots, dict):
            for key in spell_slots:
                if key not in list(range(1, 10)) or not (isinstance(spell_slots[key], int) and spell_slots[key] > -1):
                    raise ValueError("Spell slots must be a dictionary mapping spell levels (1-20) to slots (0 or more)")
        elif not spell_slots:
            for i in range(1, 10):
                value = kwargs.get("level_%d" % i)
                if value:
                    if isinstance(value, int):
                        spell_slots[i] = value
                    else:
                        raise ValueError("Spell slot number for level %d must be an integer" % i)
        else:
            raise ValueError(
                "Spell slots must be a dictionary. {1: 3, 2:1} would mean 3 1st level spells and 1 2nd level spell")
        self._spell_slots = spell_slots
        self._full_spell_slots = dict()
        self._full_spell_slots.update(self._spell_slots)
        self._spells = []
        spells = kwargs.get("spells")
        if not spells:
            warnings.warn("Created a SpellCaster with no spells")
        elif not isinstance(spells, (list, tuple)):
            raise ValueError("Spells must be a list or tuple")
        else:
            for spell in spells:
                try:
                    self.add_spell(spell)
                except ValueError:
                    raise ValueError("Spells must contain only Spell objects")

    def copy_constructor(self, other, **kwargs):  # pylint: disable=arguments-differ
        super().copy_constructor(other, name=kwargs.get("name"))
        if isinstance(other, SpellCaster):
            self._spell_ability = other.get_spell_ability()
            self._spell_ability_mod = other.get_spell_ability_mod()
            self._spell_save_dc = other.get_spell_save_dc()
            self._spell_attack_mod = other.get_spell_attack_mod()
            self._spell_slots = dict()
            self._spell_slots.update(other.get_spell_slots())
            self._full_spell_slots = dict()
            self._full_spell_slots.update(self._spell_slots)
            self._spells = []
            for spell in other.get_spells():
                new_spell = type(spell)(copy=spell)  # makes sure we get the right kind of spell
                self._spells.append(new_spell)
        else:
            spell_ability = kwargs.get("spell_ability", "intelligence")
            try:
                self._spell_ability_mod = self.get_ability(spell_ability)
                self._spell_ability = spell_ability
            except ValueError:
                raise ValueError(
                    "Spell ability must be strength, dexterity, constitution, intelligence, wisdom, or charisma")
            self._spell_save_dc = 8 + self._proficiency_mod + self._spell_ability_mod
            self._spell_attack_mod = self._proficiency_mod + self._spell_ability_mod
            spell_slots = kwargs.get("spell_slots")
            if spell_slots and not isinstance(spell_slots, dict):
                raise ValueError(
                    "Spell slots must be a dictionary. {1: 3, 2:1} would mean 3 1st level spells and 1 2nd level spell")
            if not spell_slots:
                for i in range(1, 10):
                    value = kwargs.get("level_%d" % i)
                    if value:
                        if isinstance(value, int):
                            spell_slots[i] = value
                        else:
                            raise ValueError("Spell slot number for level %d must be an integer" % i)
            self._spell_slots = spell_slots
            self._spells = []
            spells = kwargs.get("spells")
            if not spells:
                warnings.warn("Created a SpellCaster with no spells")
            elif not isinstance(spells, (list, tuple)):
                raise ValueError("Spells must be a list or tuple")
            else:
                for spell in spells:
                    if not isinstance(spell, attack_class.Spell):
                        raise ValueError("Spells must contain only Spell objects")
                    self.add_spell(attack_class.Spell(copy=spell))

    def get_spell_ability(self) -> str:
        return self._spell_ability

    def get_spell_ability_mod(self) -> int:
        return self._spell_ability_mod

    def get_spell_save_dc(self) -> int:
        return self._spell_save_dc

    def get_spell_attack_mod(self) -> int:
        return self._spell_attack_mod

    def get_spell_slots(self) -> dict:
        return self._spell_slots

    def get_level_spell_slots(self, level: int) -> int:
        if not isinstance(level, int):
            raise ValueError("Level provided must be an integer")
        try:
            return self._spell_slots[level]
        except KeyError:
            return 0

    def get_spells(self) -> list:
        return self._spells

    def add_spell(self, spell: attack_class.Spell):
        if isinstance(spell, attack_class.Spell):
            if isinstance(spell, attack_class.HealingSpell):
                spell.set_damage_mod(self._spell_ability_mod)
            spell.set_attack_mod(self._spell_attack_mod)
            self._spells.append(spell)
        else:
            raise ValueError("Cannot add a non-Spell object as an attack.")

    def spend_spell_slot(self, level: int, spell=None):  # pylint: disable=unused-argument
        if level == 0:
            return  # don't spend a slot
        if not isinstance(level, int) or not (0 < level < 10):
            raise ValueError("Level must be an integer between 1 and 9")
        if self.get_level_spell_slots(level) > 0:
            self._spell_slots[level] -= 1
        else:
            raise ValueError("%s tried to use a level %d spell slot that they don't have" % (self._name, level))

    def reset_spell_slots(self):
        if self.get_verbose():  # pragma: no cover
            print("%s resetting spell slots" % self._name)
        spell_slots = dict()
        spell_slots.update(self._full_spell_slots)
        self._spell_slots = spell_slots
