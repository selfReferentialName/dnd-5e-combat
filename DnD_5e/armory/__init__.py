from DnD_5e.weapons import Weapon

class SimpleWeapon(Weapon):
    """Mostly used to determine if a character has proficiency with a weapon"""
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

class MartialWeapon(Weapon):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

class MeleeWeapon(Weapon):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

class RangedWeapon(Weapon):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

# Simple Melee

class Club(SimpleWeapon, MeleeWeapon):  # pragma: no cover
    def __init__(self, **kwargs):
        if not kwargs.get("name"):
            kwargs.update({"name": "Club"})
        kwargs.update({"damage_dice": "1d4", "damage_type": "bludgeoning", "light": 1})
        super().__init__(**kwargs)

class Dagger(SimpleWeapon, MeleeWeapon):  # pragma: no cover
    def __init__(self, **kwargs):
        if not kwargs.get("name"):
            kwargs.update({"name": "Dagger"})
        kwargs.update({"damage_dice": "1d4", "damage_type": "piercing", "finesse": 1, "light": 1, "melee_range": 5, "range": (20, 60)})
        super().__init__(**kwargs)

class Greatclub(SimpleWeapon, MeleeWeapon):  # pragma: no cover
    def __init__(self, **kwargs):
        if not kwargs.get("name"):
            kwargs.update({"name": "Greatclub"})
        kwargs.update({"damage_dice": "1d8", "damage_type": "bludgeoning", "two_handed": 1})
        super().__init__(**kwargs)

class Javelin(SimpleWeapon, MeleeWeapon):  # pragma: no cover
    def __init__(self, **kwargs):
        if not kwargs.get("name"):
            kwargs.update({"name": "Javelin"})
        kwargs.update({"damage_dice": "1d6", "damage_type": "piercing", "range": (30, 120), "melee_range": 5})
        super().__init__(**kwargs)

class Light_Hammer(SimpleWeapon, MeleeWeapon):  # pragma: no cover
    def __init__(self, **kwargs):
        if not kwargs.get("name"):
            kwargs.update({"name": "Light Hammer"})
        kwargs.update({"damage_dice": "1d4", "damage_type": "bludgeoning", "light": 1, "range": (20, 60), "melee_range": 5})
        super().__init__(**kwargs)

class Mace(SimpleWeapon, MeleeWeapon):  # pragma: no cover
    def __init__(self, **kwargs):
        if not kwargs.get("name"):
            kwargs.update({"name": "Mace"})
        kwargs.update({"damage_dice": "1d6", "damage_type": "bludgeoning"})
        super().__init__(**kwargs)

class Quarterstaff(SimpleWeapon, MeleeWeapon):  # pragma: no cover
    def __init__(self, **kwargs):
        if not kwargs.get("name"):
            kwargs.update({"name": "Quarterstaff"})
        kwargs.update({"damage_dice": "1d6", "damage_type": "bludgeoning", "versatile": "1d8"})
        super().__init__(**kwargs)

class Sickle(SimpleWeapon, MeleeWeapon):  # pragma: no cover
    def __init__(self, **kwargs):
        if not kwargs.get("name"):
            kwargs.update({"name": "Sickle"})
        kwargs.update({"damage_dice": "1d4", "damage_type": "slashing", "light": 1})
        super().__init__(**kwargs)

class Spear(SimpleWeapon, MeleeWeapon):  # pragma: no cover
    def __init__(self, **kwargs):
        if not kwargs.get("name"):
            kwargs.update({"name": "Spear"})
        kwargs.update({"damage_dice": "1d6", "damage_type": "piercing", "range": (20, 60), "melee_range": 5, "versatile": "1d8"})
        super().__init__(**kwargs)

# Simple Ranged

class Light_Crossbow(SimpleWeapon, RangedWeapon):  # pragma: no cover
    def __init__(self, **kwargs):
        if not kwargs.get("name"):
            kwargs.update({"name": "Light Crossbow"})
        kwargs.update({"damage_dice": "1d8", "damage_type": "piercing", "loading": 1, "two_handed": 1, "range": (80, 320)})
        super().__init__(**kwargs)

class Dart(SimpleWeapon, RangedWeapon):  # pragma: no cover
    def __init__(self, **kwargs):
        if not kwargs.get("name"):
            kwargs.update({"name": "Dart"})
        kwargs.update({"damage_dice": "1d4", "damage_type": "piercing", "finesse": 1, "range": (20, 60)})
        super().__init__(**kwargs)

class Shortbow(SimpleWeapon, RangedWeapon):  # pragma: no cover
    def __init__(self, **kwargs):
        if not kwargs.get("name"):
            kwargs.update({"name": "Shortbow"})
        kwargs.update({"damage_dice": "1d6", "damage_type": "piercing", "two_handed": 1, "range": (80, 320)})
        super().__init__(**kwargs)

class Sling(SimpleWeapon, RangedWeapon):  # pragma: no cover
    def __init__(self, **kwargs):
        if not kwargs.get("name"):
            kwargs.update({"name": "Sling"})
        kwargs.update({"damage_dice": "1d4", "damage_type": "bludgeoning", "range": (30, 120)})
        super().__init__(**kwargs)

# Martial Melee
class Battleaxe(MartialWeapon, MeleeWeapon):  # pragma: no cover
    def __init__(self, **kwargs):
        if not kwargs.get("name"):
            kwargs.update({"name": "Battleaxe"})
        kwargs.update({"damage_dice": "1d8", "damage_type": "slashing", "versatile": "1d10"})
        super().__init__(**kwargs)

class Flail(MartialWeapon, MeleeWeapon):  # pragma: no cover
    def __init__(self, **kwargs):
        if not kwargs.get("name"):
            kwargs.update({"name": "Flail"})
        kwargs.update({"damage_dice": "1d8", "damage_type": "bludgeoning"})
        super().__init__(**kwargs)

class Glaive(MartialWeapon, MeleeWeapon):  # pragma: no cover
    def __init__(self, **kwargs):
        if not kwargs.get("name"):
            kwargs.update({"name": "Glaive"})
        kwargs.update({"damage_dice": "1d10", "damage_type": "slashing", "heavy": 1, "reach": 1, "two_handed": 1})
        super().__init__(**kwargs)

class Greataxe(MartialWeapon, MeleeWeapon):  # pragma: no cover
    def __init__(self, **kwargs):
        if not kwargs.get("name"):
            kwargs.update({"name": "Greataxe"})
        kwargs.update({"damage_dice": "1d12", "damage_type": "slashing", "heavy": 1, "two_handed": 1})
        super().__init__(**kwargs)

class Greatsword(MartialWeapon):  # pragma: no cover
    def __init__(self, **kwargs):
        if not kwargs.get("name"):
            kwargs.update({"name": "Greatsword"})
        kwargs.update({"damage_dice": "2d6", "damage_type": "slashing", "heavy": 1, "two_handed": 1})
        super().__init__(**kwargs)

class Halberd(MartialWeapon, MeleeWeapon):  # pragma: no cover
    def __init__(self, **kwargs):
        if not kwargs.get("name"):
            kwargs.update({"name": "Halberd"})
        kwargs.update({"damage_dice": "1d10", "damage_type": "slashing", "heavy": 1, "reach": 1, "two_handed": 1})
        super().__init__(**kwargs)

class Lance(MartialWeapon, MeleeWeapon):  # pragma: no cover
    # note: Lance has special rules
    def __init__(self, **kwargs):
        if not kwargs.get("name"):
            kwargs.update({"name": "Lance"})
        kwargs.update({"damage_dice": "1d12", "damage_type": "piercing", "reach": 1})
        super().__init__(**kwargs)

class Longsword(MartialWeapon, MeleeWeapon):  # pragma: no cover
    def __init__(self, **kwargs):
        if not kwargs.get("name"):
            kwargs.update({"name": "Longsword"})
        kwargs.update({"damage_dice": "1d8", "damage_type": "slashing", "versatile": "1d10"})
        super().__init__(**kwargs)

class Maul(MartialWeapon, MeleeWeapon):  # pragma: no cover
    def __init__(self, **kwargs):
        if not kwargs.get("name"):
            kwargs.update({"name": "Maul"})
        kwargs.update({"damage_dice": "2d6", "damage_type": "bludgeoning", "heavy": 1, "two_handed": 1})
        super().__init__(**kwargs)

class Morningstar(MartialWeapon, MeleeWeapon):  # pragma: no cover
    def __init__(self, **kwargs):
        if not kwargs.get("name"):
            kwargs.update({"name": "Morningstar"})
        kwargs.update({"damage_dice": "1d8", "damage_type": "piercing"})
        super().__init__(**kwargs)

class Pike(MartialWeapon, MeleeWeapon):  # pragma: no cover
    def __init__(self, **kwargs):
        if not kwargs.get("name"):
            kwargs.update({"name": "Pike"})
        kwargs.update({"damage_dice": "1d10", "damage_type": "piercing", "heavy": 1, "reach": 1, "two_handed": 1})
        super().__init__(**kwargs)

class Rapier(MartialWeapon, MeleeWeapon):  # pragma: no cover
    def __init__(self, **kwargs):
        if not kwargs.get("name"):
            kwargs.update({"name": "Rapier"})
        kwargs.update({"damage_dice": "1d8", "damage_type": "piercing", "finesse": 1})
        super().__init__(**kwargs)

class Scimitar(MartialWeapon, MeleeWeapon):  # pragma: no cover
    def __init__(self, **kwargs):
        if not kwargs.get("name"):
            kwargs.update({"name": "Scimitar"})
        kwargs.update({"damage_dice": "1d6", "damage_type": "slashing", "finesse": 1, "light": 1})
        super().__init__(**kwargs)

class Shortsword(MartialWeapon, MeleeWeapon):  # pragma: no cover
    def __init__(self, **kwargs):
        if not kwargs.get("name"):
            kwargs.update({"name": "Shortsword"})
        kwargs.update({"damage_dice": "1d6", "damage_type": "piercing", "finesse": 1, "light": 1})
        super().__init__(**kwargs)

class Trident(MartialWeapon, MeleeWeapon):  # pragma: no cover
    def __init__(self, **kwargs):
        if not kwargs.get("name"):
            kwargs.update({"name": "Trident"})
        kwargs.update({"damage_dice": "1d6", "damage_type": "piercing", "range": (20, 60), "melee_range": 5, "versatile": "1d8"})
        super().__init__(**kwargs)

class War_Pick(MartialWeapon, MeleeWeapon):  # pragma: no cover
    def __init__(self, **kwargs):
        if not kwargs.get("name"):
            kwargs.update({"name": "War Pick"})
        kwargs.update({"damage_dice": "1d8", "damage_type": "piercing"})
        super().__init__(**kwargs)

class Warhammer(MartialWeapon, MeleeWeapon):  # pragma: no cover
    def __init__(self, **kwargs):
        if not kwargs.get("name"):
            kwargs.update({"name": "Warhammer"})
        kwargs.update({"damage_dice": "1d8", "damage_type": "bludgeoning", "versatile": "1d10"})
        super().__init__(**kwargs)

class Whip(MartialWeapon, MeleeWeapon):  # pragma: no cover
    def __init__(self, **kwargs):
        if not kwargs.get("name"):
            kwargs.update({"name": "Whip"})
        kwargs.update({"damage_dice": "1d4", "damage_type": "slashing", "finesse": 1, "reach": 1})
        super().__init__(**kwargs)

# Martial Ranged
class Blowgun(MartialWeapon, RangedWeapon):  # pragma: no cover
    def __init__(self, **kwargs):
        if not kwargs.get("name"):
            kwargs.update({"name": "Blowgun"})
        kwargs.update({"damage_dice": "1d1", "damage_type": "piercing", "loading": 1, "range": (25, 100)})
        super().__init__(**kwargs)

class Hand_Crossbow(MartialWeapon, RangedWeapon):  # pragma: no cover
    def __init__(self, **kwargs):
        if not kwargs.get("name"):
            kwargs.update({"name": "Hand Crossbow"})
        kwargs.update({"damage_dice": "1d6", "damage_type": "piercing", "light": 1, "loading": 1, "range": (30, 120)})
        super().__init__(**kwargs)

class Heavy_Crossbow(MartialWeapon, RangedWeapon):  # pragma: no cover
    def __init__(self, **kwargs):
        if not kwargs.get("name"):
            kwargs.update({"name": "Heavy Crossbow"})
        kwargs.update({"damage_dice": "1d10", "damage_type": "piercing", "heavy": 1, "loading": 1, "two_handed": 1, "range": (100, 400)})
        super().__init__(**kwargs)

class Longbow(MartialWeapon, RangedWeapon):  # pragma: no cover
    def __init__(self, **kwargs):
        if not kwargs.get("name"):
            kwargs.update({"name": "Longbow"})
        kwargs.update({"damage_dice": "1d8", "damage_type": "piercing", "heavy": 1, "two_handed": 1, "range": (150, 600)})
        super().__init__(**kwargs)

# Don't count Net

def is_monk_weapon(weapon: Weapon) -> bool:
    # placed here instead of in weapons to avoid circular dependency
    if not isinstance(weapon, Weapon):
        return False
    return isinstance(weapon, Shortsword) or (
        isinstance(weapon, SimpleWeapon) and isinstance(weapon, MeleeWeapon) and not weapon.has_prop("two_handed")
        and not weapon.has_prop("heavy"))
