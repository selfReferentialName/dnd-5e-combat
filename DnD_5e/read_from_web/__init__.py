import urllib.request
from bs4 import BeautifulSoup
from DnD_5e.utility_methods_dnd import ability_from_abbreviation

def read_from_d20srd(url, outname=None, indent_size=4):  # pragma: no cover
    if outname:
        outfile = open(outname, "a")
    soup = BeautifulSoup(urllib.request.urlopen(url), "lxml")
    soup = soup.body
    name = soup.h1.get_text()
    if outname:
        outfile.write("class %s(combatant.Creature):\n" % name)
        outfile.write(" "*indent_size + "def __init__(self, **kwargs):\n")
        outfile.write(" "*2*indent_size + "default_kwargs = {")
    else:
        print("Name:", name)
    paragraphs = soup.find_all("p")
    size_race_alignment = paragraphs[0].get_text()
    size_race, alignment = size_race_alignment.split(", ")  # pylint: disable=unused-variable
    size = size_race.split(" ")[0].lower()
    race = size_race[size_race.find(" ")+1:].lower()
    if outname:
        outfile.write('"size": "%s", "creature_type": "%s",\n' % (size, race))
    else:
        print("Size:", size)
        print("Creature type:", race)
    ac_hp_speed = paragraphs[1].contents
    ac = 0
    hp = 0
    speed = 0
    for idx in range(len(ac_hp_speed)):  # pylint: disable=consider-using-enumerate
        line = ac_hp_speed[idx]
        if "Armor Class" in line:
            ac = ac_hp_speed[idx+1].strip()
            ac = ac.split()[0]  # get just the number
            ac = int(ac)
            if outname:
                outfile.write(" "*3*indent_size + '"ac": %d, ' % ac)
            else:
                print("AC:", ac)
        elif "Hit Points" in line:
            hp = ac_hp_speed[idx+1].strip()
            stuff = hp.split()
            hp = stuff[0]
            hp = int(hp)
            hit_dice = stuff[1][1:]
            if outname:
                outfile.write('"max_hp": %d, "hit_dice": "%s", ' % (hp, hit_dice))
            else:
                print("HP:", hp)
                print("Hit Dice:", hit_dice)
        elif "Speed" in line:
            speed = int(ac_hp_speed[idx+1].strip().split()[0])  # TODO: figure out how to handle this data
            if outname:
                outfile.write('"speed": %d,\n' % speed)
            else:
                print("Speed:", speed)
    attribute_table = soup.table.tbody.tr
    scores = [int(x.get_text().split("(")[0]) for x in attribute_table.find_all("td")]
    strength, dexterity, constitution, intelligence, wisdom, charisma = scores
    if outname:
        outfile.write(" "*3*indent_size +
                      '"strength": %d, "dexterity": %d, "constitution": %d, "intelligence": %d, "wisdom": %d, "charisma": %d}\n' %
                      (strength, dexterity, constitution, intelligence, wisdom, charisma))
    else:
        print("Str:", strength)
        print("Dex:", dexterity)
        print("Con:", constitution)
        print("Int:", intelligence)
        print("Wis:", wisdom)
        print("Cha:", charisma)
    save_skill_sense_lang_challenge = paragraphs[2].contents
    proficiencies = set()
    vision = "normal"
    cr = 0
    for idx in range(len(save_skill_sense_lang_challenge)):  # pylint: disable=consider-using-enumerate
        line = save_skill_sense_lang_challenge[idx]
        if "Saving Throws" in line:
            saving_throw_str = save_skill_sense_lang_challenge[idx+1].strip()
            saving_throw_list = saving_throw_str.split(", ")
            for str_value in saving_throw_list:
                ability, mod = str_value.split()  # pylint: disable=unused-variable
                ability = ability.lower()
                ability = ability_from_abbreviation(ability)
                proficiencies.add(ability)
        elif "Senses" in line:
            vision_line = save_skill_sense_lang_challenge[idx+1]
            if "truesight" in vision_line:
                vision = "truesight"
            elif "blindsight" in vision_line:
                vision = "blindsight"
            elif "darkvision" in vision_line:
                vision = "darkvision"
        elif "Challenge" in line:
            challenge_line = save_skill_sense_lang_challenge[idx+1].strip()
            cr = int(challenge_line.split()[0])
    if outname:
        outfile.write(" "*2*indent_size + "default_kwargs.update({\"proficiencies\": %s})\n" % str(proficiencies))
        outfile.write(" "*2*indent_size + "default_kwargs.update({'vision': \"%s\", 'cr': %d})\n" % (vision, cr))
    else:
        print("Proficiencies:", proficiencies)
        print("Vision:", vision)
        print("CR:", cr)
    attack_paragraphs = paragraphs[3:]
    attack_stats = {}
    for attack_stuff in attack_paragraphs:
        text = attack_stuff.get_text().strip()  # remove trailing whitespace
        try:
            attack_name = attack_stuff.em.strong.get_text().replace(".", "").strip()
        except AttributeError:
            continue
        attack_stats[attack_name] = {}
        if "Weapon Attack" in text:
            range_type, text = text.split("Weapon Attack: ")[0:2]
            to_hit, reach = text.split(", ")[:2]  # TODO: care about number of targets
            to_hit = to_hit.split(" to hit")[0]
            sign = to_hit[0]
            if sign == "+":
                to_hit = int(to_hit[1:])
            elif sign == "-":
                to_hit = int(to_hit[1:]) * -1
            attack_stats[attack_name]["attack_mod"] = to_hit
            reach = reach.split("reach ")[1]
            reach = reach.split(" ft")[0]
            reach = int(reach)
            attack_stats[attack_name]["melee_range"] = 0
            attack_stats[attack_name]["range"] = 0
            if "Melee" in range_type:
                attack_stats[attack_name]["melee_range"] = reach
            elif "Ranged" in range_type:
                attack_stats[attack_name]["range"] = reach
            damage_str = text[text.find("Hit: "): text.find("damage.")]
            damage_str = damage_str.replace("Hit: ", "").replace("(", "").replace(")","").replace(" + ", " ")
            avg_damage, dice, damage_mod, damage_type = damage_str.split()  # pylint: disable=unused-variable
            attack_stats[attack_name]["dice"] = dice
            attack_stats[attack_name]["damage_mod"] = int(damage_mod)
            attack_stats[attack_name]["damage_type"] = damage_type
            if text.find("damage.") + 8 < len(text):  # pylint: disable=simplifiable-if-statement
                attack_stats[attack_name]["more_content"] = True
            else:
                attack_stats[attack_name]["more_content"] = False
    if outname:
        outfile.write(" "*2*indent_size +
                      "# the following lines are things that may be features or attacks. Refer to the original source to see what you want to incorporate.\n")
        outfile.write(" "*2*indent_size + '"""\n')
        for attack_name in attack_stats:
            if attack_stats[attack_name]:
                outfile.write(" "*2*indent_size + "attack_class.Attack(dice=%s, attack_mod=%d, damage_mod=%d, damage_type=%s, "
                              "melee_range=%s, range=%s, name=%s)" %
                              (attack_stats[attack_name]["dice"], attack_stats[attack_name]["attack_mod"],
                               attack_stats[attack_name]["damage_mod"], attack_stats[attack_name]["damage_type"],
                               attack_stats[attack_name]["melee_range"], attack_stats[attack_name]["range"], attack_name))
                if attack_stats[attack_name]["more_content"]:
                    outfile.write("  # there is more to this attack. Refer to the original source\n")
                else:
                    outfile.write("\n")
            else:
                outfile.write(" "*2*indent_size + "%s\n" % attack_name)
        outfile.write(" "*2*indent_size + '"""\n')
        outfile.write(" "*2*indent_size + "kwargs.update(default_kwargs)\n")
        outfile.write(" "*2*indent_size + "super().__init__(**kwargs)\n")
    else:
        print(attack_stats)
    if outname:
        outfile.close()

if __name__ == "__main__":  # pragma: no cover
    read_from_d20srd("http://5e.d20srd.org/srd/monsters/aboleth.htm", "../bestiary/__init__.py")
