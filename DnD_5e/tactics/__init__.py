import random

class Tactic:
    def __init__(self, **kwargs):
        tactic_copy = kwargs.get("copy")
        if tactic_copy:
            self.copy_constructor(tactic_copy, name=kwargs.get("name"))
            return
        self._name = kwargs.get("name", type(self).__name__)
        if not isinstance(self._name, str):
            raise ValueError("Name must be a string")
        self._tiebreakers = kwargs.get("tiebreakers", [])
        if not isinstance(self._tiebreakers, list):
            raise ValueError("Tiebreakers must be a list")
        if len(set(self._tiebreakers)) != len(self._tiebreakers):
            raise ValueError("Tiebreakers cannot contain duplicate items")
        for item in self._tiebreakers:
            if not isinstance(item, Tactic):
                raise ValueError("All tiebreakers must be Tactics")

    def copy_constructor(self, other, **kwargs):
        if other == self:  # don't bother copying yourself
            return
        if not isinstance(other, Tactic):
            raise ValueError("Cannot make self a copy of something that is not a Tactic")
        name = kwargs.get("name")
        if not name or not isinstance(name, str):
            name = other.get_name()
        Tactic.__init__(self=self, name=name)
        copy_tiebreakers = kwargs.get("copy_tiebreakers", False)
        for tiebreaker in other.get_tiebreakers():
            if copy_tiebreakers:
                tiebreaker = Tactic(copy=tiebreaker)
            self.append_tiebreaker(tiebreaker)

    def get_name(self) -> str:
        return self._name

    def get_tiebreakers(self) -> list:
        return self._tiebreakers

    def append_tiebreaker(self, tiebreaker):
        if not isinstance(tiebreaker, Tactic):
            raise ValueError("Tiebreaker must be a Tactic")
        if tiebreaker in self.get_tiebreakers():
            raise ValueError("%s already has this tactic: %s" % (self._name, tiebreaker.get_name()) )
        self._tiebreakers.append(tiebreaker)

    def run_tactic(self, choices: list) -> list:  # pylint: disable=no-self-use
        # default tactic is to pick one thing at random. To be overridden in subclasses
        if not isinstance(choices, list):
            raise ValueError("Choices must be a list")
        choice = random.choice(choices)
        return list(choice)

    def make_choice(self, choices: list):  # returns whatever type choices contains
        if not isinstance(choices, list):
            raise ValueError("Choices must be a list")
        new_choices = self.run_tactic(choices)
        if len(new_choices) == 1:
            return new_choices[0]
        if len(new_choices) == 0:  # pylint: disable=len-as-condition
            new_choices = choices
        tie_choices = new_choices
        for tiebreaker in self.get_tiebreakers():
            previous_choices = tie_choices
            tie_choices = tiebreaker.run_tactic(tie_choices, use_tiebreakers=False)
            if len(tie_choices) == 1:
                return tie_choices[0]
            if len(tie_choices) == 0:  # pylint: disable=len-as-condition
                tie_choices = previous_choices
        if len(tie_choices) == 1:
            return tie_choices[0]
        return random.choice(tie_choices)  # need a final clause that guarantees exactly one result is returned
