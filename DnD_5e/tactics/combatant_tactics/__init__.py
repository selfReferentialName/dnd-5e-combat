from typing import List
from DnD_5e.tactics import Tactic

class CombatantTactic(Tactic):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

class LowestAcTactic(CombatantTactic):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def run_tactic(self, choices: list) -> list:
        if not isinstance(choices, list):
            raise ValueError("Choices must be a list")
        lowest_ac = 100
        result = []
        for item in choices:
            try:
                ac = item.get_ac()
            except AttributeError:
                raise ValueError("Items in choices must be Combatants")
            if ac < lowest_ac:
                lowest_ac = ac
                result = [item]
            elif ac == lowest_ac:
                result.append(item)
        return result

class HighestAcTactic(CombatantTactic):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def run_tactic(self, choices: list) -> list:
        if not isinstance(choices, list):
            raise ValueError("Choices must be a list")
        highest_ac = 0
        result = []
        for item in choices:
            try:
                ac = item.get_ac()
            except AttributeError:
                raise ValueError("Items in choices must be Combatants")
            if ac > highest_ac:
                highest_ac = ac
                result = [item]
            elif ac == highest_ac:
                result.append(item)
        return result

class LowAcTactic(CombatantTactic):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._threshold = kwargs.get("threshold")
        if self._threshold is None:
            attack = kwargs.get("attack")
            use_max = kwargs.get("use_max")
            try:
                if use_max:
                    self._threshold = attack.get_max_hit() + 1
                else:
                    self._threshold = attack.get_average_hit() + 1
            except AttributeError:
                raise ValueError("Attack must be an Attack")
        if not isinstance(self._threshold, int):
            raise ValueError("Threshold must be an integer")

    def run_tactic(self, choices: list) -> list:
        if not isinstance(choices, list):
            raise ValueError("Choices must be a list")
        result = []
        for item in choices:
            try:
                if item.get_ac() < self._threshold:
                    result.append(item)
            except AttributeError:
                raise ValueError("Items in choices must be Combatants")
        return result

class HighAcTactic(CombatantTactic):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._threshold = kwargs.get("threshold")
        if not isinstance(self._threshold, int):
            raise ValueError("Threshold must be an integer")

    def run_tactic(self, choices: list) -> list:
        if not isinstance(choices, list):
            raise ValueError("Choices must be a list")
        result = []
        for item in choices:
            try:
                if item.get_ac() > self._threshold:
                    result.append(item)
            except AttributeError:
                raise ValueError("Items in choices must be Combatants")
        return result

class BloodiedTactic(CombatantTactic):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def run_tactic(self, choices: list) -> list:
        if not isinstance(choices, list):
            raise ValueError("Choices must be a list")
        result = []
        for item in choices:
            try:
                if item.is_bloodied():
                    result.append(item)
            except AttributeError:
                raise ValueError("Items in choices must be Combatants")
        return result

class MaxHpTactic(CombatantTactic):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def run_tactic(self, choices: list) -> list:
        if not isinstance(choices, list):
            raise ValueError("Choices must be a list")
        result = []
        for item in choices:
            try:
                if item.is_hp_max():
                    result.append(item)
            except AttributeError:
                raise ValueError("Items in choices must be Combatants")
        return result

class HpToMaxHighTactic(CombatantTactic):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._threshold = kwargs.get("threshold")
        if self._threshold is None:
            healing = kwargs.get("healing")
            use_max = kwargs.get("use_max", True)
            try:
                if use_max:
                    self._threshold = healing.get_max_damage() - 1
                else:
                    self._threshold = healing.get_average_damage() - 1
            except AttributeError:
                raise ValueError("Healing must be an Attack (recommended to be a HealingSpell)")
        if not isinstance(self._threshold, int):
            raise ValueError("Threshold must be an integer")

    def run_tactic(self, choices: list) -> list:
        if not isinstance(choices, list):
            raise ValueError("Choices must be a list")
        result = []
        for item in choices:
            try:
                if item.get_hp_to_max() > self._threshold:
                    result.append(item)
            except AttributeError:
                raise ValueError("Items in choices must be Combatants")
        return result

class MurderHoboTactic(CombatantTactic):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def run_tactic(self, choices: list) -> list:
        if not isinstance(choices, list):
            raise ValueError("Choices must be a list")
        result = []
        for item in choices:
            try:
                if item.has_condition("unconscious"):  # TODO: make this more specific after I write is_unconscious method
                    result.append(item)
            except AttributeError:
                raise ValueError("Items in choices must be Combatants")
        return result

class HasTempHpTactic(CombatantTactic):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def run_tactic(self, choices: list) -> list:
        if not isinstance(choices, list):
            raise ValueError("Choices must be a list")
        result = []
        for item in choices:
            try:
                if item.get_temp_hp() > 0:
                    result.append(item)
            except AttributeError:
                raise ValueError("Items in choices must be Combatants")
        return result

class NoTempHpTactic(CombatantTactic):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def run_tactic(self, choices: list) -> list:
        if not isinstance(choices, list):
            raise ValueError("Choices must be a list")
        result = []
        for item in choices:
            try:
                if item.get_temp_hp() < 1:
                    result.append(item)
            except AttributeError:
                raise ValueError("Items in choices must be Combatants")
        return result

class HasVulnerabilityTactic(CombatantTactic):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._damage_type = kwargs.get("damage_type")
        if not isinstance(self._damage_type, str):
            raise ValueError("Damage type must be a string")

    def run_tactic(self, choices: list) -> list:
        if not isinstance(choices, list):
            raise ValueError("Choices must be a list")
        result = []
        for item in choices:
            try:
                if item.is_vulnerable(self._damage_type):
                    result.append(item)
            except AttributeError:
                raise ValueError("Items in choices must be Combatants")
        return result

class HasResistanceTactic(CombatantTactic):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._damage_type = kwargs.get("damage_type")
        if not isinstance(self._damage_type, str):
            raise ValueError("Damage type must be a string")

    def run_tactic(self, choices: list) -> list:
        if not isinstance(choices, list):
            raise ValueError("Choices must be a list")
        result = []
        for item in choices:
            try:
                if item.is_resistant(self._damage_type):
                    result.append(item)
            except AttributeError:
                raise ValueError("Items in choices must be Combatants")
        return result

class HasImmunityTactic(CombatantTactic):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._damage_type = kwargs.get("damage_type")
        if not isinstance(self._damage_type, str):
            raise ValueError("Damage type must be a string")

    def run_tactic(self, choices: list) -> list:
        if not isinstance(choices, list):
            raise ValueError("Choices must be a list")
        result = []
        for item in choices:
            try:
                if item.is_immune(self._damage_type):
                    result.append(item)
            except AttributeError:
                raise ValueError("Items in choices must be Combatants")
        return result
