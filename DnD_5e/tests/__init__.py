import random
from DnD_5e.utility_methods_dnd import ability_to_mod, validate_dice
from DnD_5e import armory, attack_class, bestiary, character_classes, combatant, weapons, tactics
from DnD_5e.tactics import combatant_tactics

# def test_all():
#     test_utility()
#     test_combatant()
#     test_creature()
#     test_character()
#     test_barbarian()
#     test_fighter()
#     test_monk()
#     test_rogue()
#     test_spellcaster()
#     test_bard()
#     test_cleric()
#     test_druid()
#     test_paladin()
#     test_ranger()
#     test_sorcerer()
#     test_warlock()
#     test_wizard()
#     test_weapon()
#     test_attack()
#     test_saving_throw_attack()
#     test_spell()
#     test_saving_throw_spell()
#     test_healing_spell()
#     test_read_from_web()
#     test_armory()
#     test_tactics()
#     test_combatant_tactics()

def test_utility():
    try:
        ability_to_mod(-5)
        raise Exception("Allowed low ability score")   # pragma: no cover
    except ValueError:
        pass

    try:
        ability_to_mod(70)
        raise Exception("Allowed high ability score")   # pragma: no cover
    except ValueError:
        pass

    try:
        validate_dice(5.2)
    except ValueError:
        pass

    try:
        validate_dice((1, 2, 3))
        raise Exception("Allowed list of len != 2")   # pragma: no cover
    except ValueError:
        pass

    try:
        validate_dice(("a", "b"))
        raise Exception("Allowed list of non-integers")   # pragma: no cover
    except ValueError:
        pass

    try:
        validate_dice(("xdz"))
        raise Exception("Allowed dice with non-integers in string")   # pragma: no cover
    except ValueError:
        pass

    try:
        validate_dice(("1d4d20"))
        raise Exception("Allowed dice with multiple d in string")   # pragma: no cover
    except ValueError:
        pass

    assert(validate_dice((2, 3)) == (2, 3))
    assert(validate_dice("3d4") == (3, 4))

def test_combatant():
    t0 = combatant.Combatant(ac=12, max_hp=20, current_hp=20, temp_hp=2, hit_dice='3d6', speed=20, vision='darkvision',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                             proficiencies=["dexterity", "charisma"], proficiency_mod=2, name='t0', vulnerabilities=["bludgeoning"],
                             resistances=["slashing", "slashing", "piercing"], immunities={"fire"})
    assert(t0.get_name() == 't0')
    assert(t0.get_ac() == 12)
    assert(t0.get_max_hp() == 20)
    assert(t0.get_current_hp() == 20)
    assert(t0.get_hp_to_max() == 0)
    assert(t0.is_hp_max())
    assert(t0.get_temp_hp() == 2)
    assert(t0.get_hit_dice() == (3, 6))
    assert(t0.get_speed() == 20)
    assert(t0.get_vision() == 'darkvision')
    assert(t0.get_strength() == 2)
    assert(t0.get_dexterity() == 3)
    assert(t0.get_constitution() == -1)
    assert(t0.get_intelligence() == 1)
    assert(t0.get_wisdom() == 0)
    assert(t0.get_charisma() == -1)
    assert(t0.has_proficiency("dexterity"))
    assert(t0.has_proficiency("charisma"))
    assert(t0.get_proficiency_mod() == 2)
    assert(t0.get_vulnerabilities() == {"bludgeoning"})
    assert(t0.is_vulnerable("bludgeoning"))
    assert(t0.is_resistant("slashing"))
    assert(t0.is_resistant("piercing"))
    assert(t0.is_immune("fire"))
    assert(t0.get_saving_throw("strength") == 2)
    assert(t0.get_saving_throw("dexterity") == 5)
    assert(t0.get_saving_throw("constitution") == -1)
    assert(t0.get_saving_throw("intelligence") == 1)
    assert(t0.get_saving_throw("wisdom") == 0)
    assert(t0.get_saving_throw("charisma") == 1)
    assert(not t0.get_features())
    assert(not t0.get_death_saves())
    assert(not t0.get_conditions())
    assert(not t0.get_weapons())
    assert(t0.get_size() == "medium")
    assert(not t0.get_items())

    t10 = combatant.Combatant(copy=t0, name="t10")
    assert(t10.get_name() == 't10')
    assert(t10.get_ac() == 12)
    assert(t10.get_max_hp() == 20)
    assert(t10.get_current_hp() == 20)
    assert(t10.get_hp_to_max() == 0)
    assert(t10.is_hp_max())
    assert(t10.get_temp_hp() == 2)
    assert(t10.get_hit_dice() == (3, 6))
    assert(t10.get_speed() == 20)
    assert(t10.get_vision() == 'darkvision')
    assert(t10.get_strength() == 2)
    assert(t10.get_dexterity() == 3)
    assert(t10.get_constitution() == -1)
    assert(t10.get_intelligence() == 1)
    assert(t10.get_wisdom() == 0)
    assert(t10.get_charisma() == -1)
    assert(t10.has_proficiency("dexterity"))
    assert(t10.has_proficiency("charisma"))
    assert(t10.get_proficiency_mod() == 2)
    assert(t10.get_vulnerabilities() == {"bludgeoning"})
    assert(t10.is_vulnerable("bludgeoning"))
    assert(t10.is_resistant("slashing"))
    assert(t10.is_resistant("piercing"))
    assert(t10.is_immune("fire"))
    assert(t10.get_saving_throw("strength") == 2)
    assert(t10.get_saving_throw("dexterity") == 5)
    assert(t10.get_saving_throw("constitution") == -1)
    assert(t10.get_saving_throw("intelligence") == 1)
    assert(t10.get_saving_throw("wisdom") == 0)
    assert(t10.get_saving_throw("charisma") == 1)
    assert(not t10.get_features())
    assert(not t10.get_death_saves())
    assert(not t10.get_conditions())
    assert(not t10.get_weapons())
    assert(t10.get_size() == "medium")
    assert(not t10.get_items())
    assert(t10 is not t0)
    assert(t10.get_conditions() is not t0.get_conditions())
    assert(t10.get_proficiencies() is not t0.get_proficiencies())
    assert(t10.get_vulnerabilities() is not t0.get_vulnerabilities())
    assert(t10.get_resistances() is not t0.get_resistances())
    assert(t10.get_immunities() is not t0.get_immunities())
    assert(t10.get_features() is not t0.get_features())

    t11 = t10.get_copy(name="t11")
    assert(t11.get_name() == 't11')
    assert(t11.get_ac() == 12)
    assert(t11.get_max_hp() == 20)
    assert(t11.get_current_hp() == 20)
    assert(t11.get_hp_to_max() == 0)
    assert(t11.is_hp_max())
    assert(t11.get_temp_hp() == 2)
    assert(t11.get_hit_dice() == (3, 6))
    assert(t11.get_speed() == 20)
    assert(t11.get_vision() == 'darkvision')
    assert(t11.get_strength() == 2)
    assert(t11.get_dexterity() == 3)
    assert(t11.get_constitution() == -1)
    assert(t11.get_intelligence() == 1)
    assert(t11.get_wisdom() == 0)
    assert(t11.get_charisma() == -1)
    assert(t11.has_proficiency("dexterity"))
    assert(t11.has_proficiency("charisma"))
    assert(t11.get_proficiency_mod() == 2)
    assert(t11.get_vulnerabilities() == {"bludgeoning"})
    assert(t11.is_vulnerable("bludgeoning"))
    assert(t11.is_resistant("slashing"))
    assert(t11.is_resistant("piercing"))
    assert(t11.is_immune("fire"))
    assert(t11.get_saving_throw("strength") == 2)
    assert(t11.get_saving_throw("dexterity") == 5)
    assert(t11.get_saving_throw("constitution") == -1)
    assert(t11.get_saving_throw("intelligence") == 1)
    assert(t11.get_saving_throw("wisdom") == 0)
    assert(t11.get_saving_throw("charisma") == 1)
    assert(not t11.get_features())
    assert(not t11.get_death_saves())
    assert(not t11.get_conditions())
    assert(not t11.get_weapons())
    assert(t11.get_size() == "medium")
    assert(not t11.get_items())
    assert(t11 is not t0)
    assert(t11.get_conditions() is not t0.get_conditions())
    assert(t11.get_proficiencies() is not t0.get_proficiencies())
    assert(t11.get_vulnerabilities() is not t0.get_vulnerabilities())
    assert(t11.get_resistances() is not t0.get_resistances())
    assert(t11.get_immunities() is not t0.get_immunities())
    assert(t11.get_features() is not t0.get_features())

    # pylint: disable=unused-variable
    try:
        t1 = combatant.Combatant(name="bob")
        raise Exception("Create combatant succeeded without ability scores")   # pragma: no cover
    except ValueError:
        pass

    try:
        t1 = combatant.Combatant(max_hp=20, current_hp=20, temp_hp=2, hit_dice='3d6', speed=20, vision='darkvision',
                                 strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="t1")
        raise Exception("Create combatant succeeded without AC")   # pragma: no cover
    except ValueError:
        pass

    try:
        t1 = combatant.Combatant(ac="10", max_hp=20, current_hp=20, temp_hp=2, hit_dice='3d6', speed=20, vision='darkvision',
                                 strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="t1")
        raise Exception("Create combatant succeeded with non-integer AC")   # pragma: no cover
    except ValueError:
        pass

    try:
        t1 = combatant.Combatant(max_hp=20, current_hp=20, temp_hp=2, hit_dice='3d6', speed=20, conditions="dog",
                                 strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="t1")
        raise Exception("Create combatant succeeded with non-list conditions")   # pragma: no cover
    except ValueError:
        pass

    try:
        t1 = combatant.Combatant(ac=18, current_hp=5, temp_hp=2, hit_dice='1d4', speed=20, vision='blindsight',
                                 strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="t1")
        raise Exception("Create combatant succeeded without max hp")   # pragma: no cover
    except ValueError:
        pass

    try:
        t1 = combatant.Combatant(ac=18, max_hp=30, current_hp=35, temp_hp=2, hit_dice='1d4', speed=20, vision='blindsight',
                                 strength=9, dexterity=10, constitution=9, intelligence=12, wisdom=11, charisma=8,
                                 name="t1")
        raise Exception("Create combatant succeeded with current hp greater than max hp")   # pragma: no cover
    except ValueError:
        pass

    t1 = combatant.Combatant(ac=18, max_hp=30, current_hp=-5, temp_hp=2, hit_dice='1d4', speed=20, vision='blindsight',
                             strength=9, dexterity=10, constitution=9, intelligence=12, wisdom=11, charisma=8,
                             name="t1")  # should give warning that combatant was created unconscious

    try:
        t1 = combatant.Combatant(ac=18, max_hp=30, current_hp=5, temp_hp=2, hit_dice='1d4', speed=20, vision='blindsight',
                                 strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8)
        raise Exception("Create combatant succeeded without name")   # pragma: no cover
    except ValueError:
        pass

    try:
        t1 = combatant.Combatant(ac=18, max_hp=30, current_hp=5, temp_hp=2, hit_dice='1d4', speed=20, vision='blindsight',
                                 strength=-1, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="t1")
        raise Exception("Create combatant succeeded with low ability score")   # pragma: no cover
    except ValueError:
        pass

    try:
        t1 = combatant.Combatant(ac=18, max_hp=30, current_hp=5, temp_hp=2, hit_dice='1d4', speed=20, vision='blindsight',
                                 strength=7, dexterity=50, constitution=9, intelligence=12, wisdom=11, charisma=8, name="t1")
        raise Exception("Create combatant succeeded with high ability score")   # pragma: no cover
    except ValueError:
        pass
    # pylint: enable=unused-variable

    # testing Combatant vision
    t2 = combatant.Combatant(ac=18, max_hp=30, current_hp=5, temp_hp=2, hit_dice='1d4', speed=20, vision='normal',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                             size="gargantuan", name="t2")
    assert(t2.can_see("normal"))
    assert(not t2.can_see("dark"))
    assert(not t2.can_see("magic"))
    assert(t2.get_size() == "gargantuan")
    t2.add_condition("blinded")
    assert(not t2.can_see("normal"))
    assert(not t2.can_see("dark"))
    assert(not t2.can_see("magic"))
    t2.remove_condition("blinded")
    assert(not t2.has_condition("blinded"))

    t2.set_size("tiny")
    assert(t2.get_size() == "tiny")

    t2.change_vision("blindsight")
    assert(t2.can_see("normal"))
    assert(t2.can_see("dark"))
    assert(not t2.can_see("magic"))
    t2.add_condition("blinded")
    assert(t2.can_see("normal"))
    assert(t2.can_see("dark"))
    assert(not t2.can_see("magic"))
    t2.remove_condition("blinded")

    t2.change_vision("darkvision")
    assert(t2.can_see("normal"))
    assert(t2.can_see("dark"))
    assert(not t2.can_see("magic"))
    t2.add_condition("blinded")
    assert(not t2.can_see("normal"))
    assert(not t2.can_see("dark"))
    assert(not t2.can_see("magic"))
    t2.remove_condition("blinded")

    t2.change_vision("truesight")
    assert(t2.can_see("normal"))
    assert(t2.can_see("dark"))
    assert(t2.can_see("magic"))
    t2.add_condition("blinded")
    assert(not t2.can_see("normal"))
    assert(not t2.can_see("dark"))
    assert(not t2.can_see("magic"))
    t2.remove_condition("blinded")

    assert(t2.take_damage(1) == 1)
    # take less than temp hp
    assert(t2.get_temp_hp() == 1)
    assert(t2.get_current_hp() == 5)
    # take more than temp hp
    assert(t2.take_damage(2) == 2)
    assert(t2.get_temp_hp() == 0)
    assert(t2.get_current_hp() == 4)
    assert(t2.get_hp_to_max() == 26)
    # take damage to current hp
    assert(t2.take_damage(2) == 2)
    assert(t2.get_temp_hp() == 0)
    assert(t2.get_current_hp() == 2)
    assert(t2.get_hp_to_max() == 28)
    # go unconscious but don't die
    assert(t2.take_damage(10) == 10)
    assert(t2.get_temp_hp() == 0)
    assert(t2.get_current_hp() == 0)
    assert(t2.has_condition("unconscious"))
    # heal
    assert(t2.take_healing(20) == 20)
    assert(t2.get_current_hp() == 20)
    assert(not t2.has_condition("unconscious"))
    # heal above max
    assert(t2.take_healing(40) == 10)
    assert(t2.get_current_hp() == t2.get_max_hp() == 30)
    # die
    assert(t2.take_damage(100) == 100)  # suuuper dead.
    assert(t2.get_conditions() == ["dead"])
    assert(t2.get_current_hp() == 0)
    assert(t2.get_temp_hp() == 0)

def test_creature():
    c0 = combatant.Creature(ac=18, max_hp=70, current_hp=60, temp_hp=2, hit_dice='4d10', speed=30, vision='normal',
                            strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="c0",
                            cr=1/2, xp=100, creature_type="elemental")
    assert(c0.get_cr() == 1/2)
    assert(c0.get_xp() == 100)
    assert(c0.get_creature_type() == "elemental")

    c0 = combatant.Creature(ac=18, max_hp=70, current_hp=60, temp_hp=2, hit_dice='4d10', speed=30, vision='normal',
                            strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="c0",
                            cr=1/4, creature_type="monstrosity")
    assert(c0.get_cr() == 1/4)
    assert(c0.get_xp() == 50)

    c1 = combatant.Creature(copy=c0, name="c1")
    assert(c1.get_name() == "c1")
    assert(c1.get_max_hp() == 70)  # just check a stat lol
    assert(c1.get_cr() == 1/4)
    assert(c1.get_xp() == 50)
    assert(c1.get_creature_type() == "monstrosity")
    assert(c0 is not c1)

    t2 = combatant.Combatant(ac=18, max_hp=30, current_hp=5, temp_hp=2, hit_dice='1d4', speed=20, vision='normal',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                            name="t2")
    c2 = combatant.Creature(copy=t2, name="c2", cr=1, xp=200, creature_type="aberration")
    assert(c2.get_name() == "c2")
    assert(c2.get_ac() == 18)
    assert(c2.get_cr() == 1)
    assert(c2.get_xp() == 200)
    assert(c2.get_creature_type() == "aberration")
    assert(c2 is not c0)

    try:
        c0 = combatant.Creature(ac=18, max_hp=70, current_hp=60, temp_hp=2, hit_dice='4d10', speed=30, vision='normal',
                                strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="c0",
                                cr="dog")
        raise Exception("Create Creature succeeded for non-number cr")   # pragma: no cover
    except ValueError:
        pass

    try:
        c0 = combatant.Creature(ac=18, max_hp=70, current_hp=60, temp_hp=2, hit_dice='4d10', speed=30, vision='normal',
                                strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="c0",
                                cr=-2)
        raise Exception("Create Creature succeeded for negative cr")   # pragma: no cover
    except ValueError:
        pass

def test_character():
    c0 = combatant.Character(ac=18, max_hp=70, current_hp=60, temp_hp=2, hit_dice='4d10', speed=30, vision='normal',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="c0",
                             level=5)
    assert(c0.get_level() == 5)

    c1 = combatant.Character(copy=c0, name="c1")
    assert(c1.get_name() == "c1")
    assert(c1.get_temp_hp() == 2)
    assert(c1.get_level() == 5)
    assert(c1 is not c0)

    t0 = combatant.Combatant(ac=12, max_hp=20, current_hp=20, temp_hp=2, hit_dice='3d6', speed=20, vision='darkvision',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                             name='t0')

    c2 = combatant.Character(copy=t0, level=2, name="c2")
    assert(c2.get_name() == "c2")
    assert(c2.get_vision() == "darkvision")
    assert(c2.get_level() == 2)
    assert(c2 is not t0)

    try:
        c0 = combatant.Character(ac=18, max_hp=70, current_hp=60, temp_hp=2, hit_dice='4d10', speed=30, vision='normal',
                                 strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                                 name="c0",
                                 level=30)
        raise Exception("Create Character succeeded with level too high")   # pragma: no cover
    except ValueError:
        pass

    try:
        c0 = combatant.Character(ac=18, max_hp=70, current_hp=60, temp_hp=2, hit_dice='4d10', speed=30, vision='normal',
                                 strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                                 name="c0",
                                 level=-2)
        raise Exception("Create Character succeeded with level too low")   # pragma: no cover
    except ValueError:
        pass

    try:
        c0 = combatant.Character(ac=18, max_hp=70, current_hp=60, temp_hp=2, hit_dice='4d10', speed=30, vision='normal',
                                 strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                                 name="c0",
                                 level="stuff")
        raise Exception("Create Character succeeded with non-int level")   # pragma: no cover
    except ValueError:
        pass

def test_barbarian():
    barb = character_classes.Barbarian(name="barb", ac=18, strength_mod=4, dexterity_mod=4, constitution_mod=4,
                                       wisdom_mod=-2, intelligence_mod=-2, charisma_mod=-3, speed=30, level=20)
    assert(barb.get_level() == 20)
    assert(barb.get_hit_dice() == (20, 12))
    assert(barb.get_max_hp() == 12 + 4 + (7+4)*19)
    assert(barb.has_proficiency("simple weapons"))
    assert(barb.has_proficiency("martial weapons"))
    assert(barb.has_proficiency("strength"))
    assert(barb.has_proficiency("constitution"))
    assert(barb.get_proficiency_mod() == 6)
    assert(barb.get_rage_slots() == 6)
    assert(barb.get_rage_damage_bonus() == 4)
    assert(not barb.is_raging())
    assert(barb.has_feature("rage"))
    assert(barb.has_feature("unarmored defense"))
    assert(barb.has_feature("reckless attack"))
    assert(barb.has_feature("danger sense"))
    assert(barb.has_feature("extra attack"))
    assert(barb.has_feature("fast movement"))
    assert(barb.get_speed() == 40)
    assert(barb.has_feature("feral instinct"))
    assert(barb.has_feature("brutal critical"))
    assert(barb.has_feature("relentless rage"))
    assert(barb.has_feature("persistent rage"))
    assert(barb.has_feature("indomitable might"))

    barb2 = character_classes.Barbarian(copy=barb, name="barb2")
    assert(barb2.get_level() == 20)
    assert(barb2.get_hit_dice() == (20, 12))
    assert(barb2.get_max_hp() == 12 + 4 + (7 + 4) * 19)
    assert(barb2.has_proficiency("simple weapons"))
    assert(barb2.has_proficiency("martial weapons"))
    assert(barb2.has_proficiency("strength"))
    assert(barb2.has_proficiency("constitution"))
    assert(barb2.get_proficiency_mod() == 6)
    assert(barb2.get_rage_slots() == 6)
    assert(barb2.get_rage_damage_bonus() == 4)
    assert(not barb2.is_raging())
    assert(barb2.has_feature("rage"))
    assert(barb2.has_feature("unarmored defense"))
    assert(barb2.has_feature("reckless attack"))
    assert(barb2.has_feature("danger sense"))
    assert(barb2.has_feature("extra attack"))
    assert(barb2.has_feature("fast movement"))
    assert(barb2.get_speed() == 40)
    assert(barb2.has_feature("feral instinct"))
    assert(barb2.has_feature("brutal critical"))
    assert(barb2.has_feature("relentless rage"))
    assert(barb2.has_feature("persistent rage"))
    assert(barb2.has_feature("indomitable might"))
    assert(barb2 is not barb)

def test_fighter():
    f0 = character_classes.Fighter(level=11, name="f0", ac=18, strength_mod=4, dexterity_mod=2, constitution_mod=4,
                                   wisdom_mod=-1, intelligence_mod=-2, charisma_mod=-3, fighting_style="archery")
    assert(f0.get_level() == 11)
    assert(f0.get_hit_dice() == (11, 10))
    assert(f0.get_max_hp() == 10 + 4 + (6+4)*10)
    assert(f0.has_proficiency("simple weapons"))
    assert(f0.has_proficiency("martial weapons"))
    assert(f0.has_proficiency("strength"))
    assert(f0.has_proficiency("constitution"))
    assert(f0.get_proficiency_mod() == 4)
    assert(f0.has_fighting_style("archery"))
    assert(f0.has_feature("fighting style"))
    assert(f0.has_feature("second wind"))
    assert(f0.get_second_wind_slots() == 1)
    assert(f0.has_feature("action surge"))
    assert(f0.get_action_surge_slots() == 1)
    assert(f0.has_feature("martial archetype"))
    assert(f0.has_feature("extra attack"))
    assert(f0.get_extra_attack_num() == 2)
    assert(f0.has_feature("indomitable"))
    assert(f0.get_indomitable_slots() == 1)

    f1 = character_classes.Fighter(copy=f0, name="f1")
    assert(f1.get_hit_dice() == (11, 10))
    assert(f1.get_max_hp() == 10 + 4 + (6 + 4) * 10)
    assert(f1.has_proficiency("simple weapons"))
    assert(f1.has_proficiency("martial weapons"))
    assert(f1.has_proficiency("strength"))
    assert(f1.has_proficiency("constitution"))
    assert(f1.get_proficiency_mod() == 4)
    assert(f1.has_fighting_style("archery"))
    assert(f1.has_feature("fighting style"))
    assert(f1.has_feature("second wind"))
    assert(f1.get_second_wind_slots() == 1)
    assert(f1.has_feature("action surge"))
    assert(f1.get_action_surge_slots() == 1)
    assert(f1.has_feature("martial archetype"))
    assert(f1.has_feature("extra attack"))
    assert(f1.get_extra_attack_num() == 2)
    assert(f1.has_feature("indomitable"))
    assert(f1.get_indomitable_slots() == 1)
    assert(f1 is not f0)

    shortbow = armory.Shortbow()
    assert(isinstance(shortbow, armory.RangedWeapon))
    assert(shortbow.get_range())

    f0.add_weapon(shortbow)
    for attack in f0.get_attacks():
        # both are ranged attacks
        assert(attack.get_attack_mod() == f0.get_strength() + f0.get_proficiency_mod() + 2)  # testing Archer fighting style

def test_monk():
    m0 = character_classes.Monk(name="m0", level=20, ac=16, strength_mod=5, dexterity_mod=5, constitution_mod=4,
                                wisdom_mod=1, intelligence_mod=0, charisma_mod=-2)
    assert(m0.get_level() == 20)
    assert(m0.get_hit_dice() == (20, 8))
    assert(m0.get_max_hp() == (8 + 4) + (5+4)*19)
    assert(m0.has_proficiency("monk weapons"))
    assert(m0.has_proficiency("strength"))
    assert(m0.has_proficiency("dexterity"))
    assert(m0.has_proficiency("constitution"))
    assert(m0.has_proficiency("wisdom"))
    assert(m0.has_proficiency("intelligence"))
    assert(m0.has_proficiency("charisma"))
    assert(m0.get_proficiency_mod() == 6)
    assert(m0.get_martial_arts_dice() == (1, 10))
    assert(m0.has_feature("unarmored defense"))
    assert(m0.has_feature("flurry of blows"))
    assert(m0.has_feature("patient defense"))
    assert(m0.has_feature("step of the wind"))
    assert(m0.has_feature("deflect missiles"))
    assert(m0.has_feature("slow fall"))
    assert(m0.has_feature("extra attack"))
    assert(m0.has_feature("stunning strike"))
    assert(m0.has_feature("ki-empowered strikes"))
    assert(m0.has_feature("stillness of mind"))
    assert(m0.has_feature("evasion"))
    assert(m0.has_feature("purity of body"))
    assert(m0.has_feature("tongue of the sun and moon"))
    assert(m0.has_feature("diamond soul"))
    assert(m0.has_feature("empty body"))
    assert(m0.has_feature("perfect soul"))
    assert(m0.get_ki_points() == 20)
    assert(m0.get_ki_save_dc() == 8 + 6 + 1)
    assert(m0.get_saving_throw("strength") == m0.get_strength() + m0.get_proficiency_mod())
    assert(m0.get_saving_throw("dexterity") == m0.get_dexterity() + m0.get_proficiency_mod())
    assert(m0.get_saving_throw("constitution") == m0.get_constitution() + m0.get_proficiency_mod())
    assert(m0.get_saving_throw("wisdom") == m0.get_wisdom() + m0.get_proficiency_mod())
    assert(m0.get_saving_throw("intelligence") == m0.get_intelligence() + m0.get_proficiency_mod())
    assert(m0.get_saving_throw("charisma") == m0.get_charisma() + m0.get_proficiency_mod())

    m1 = character_classes.Monk(name="m1", copy=m0)
    assert(m1.get_hit_dice() == (20, 8))
    assert(m1.get_max_hp() == (8 + 4) + (5 + 4) * 19)
    assert(m1.has_proficiency("monk weapons"))
    assert(m1.has_proficiency("strength"))
    assert(m1.has_proficiency("dexterity"))
    assert(m1.has_proficiency("constitution"))
    assert(m1.has_proficiency("wisdom"))
    assert(m1.has_proficiency("intelligence"))
    assert(m1.has_proficiency("charisma"))
    assert(m1.get_proficiency_mod() == 6)
    assert(m1.get_martial_arts_dice() == (1, 10))
    assert(m1.has_feature("unarmored defense"))
    assert(m1.has_feature("flurry of blows"))
    assert(m1.has_feature("patient defense"))
    assert(m1.has_feature("step of the wind"))
    assert(m1.has_feature("deflect missiles"))
    assert(m1.has_feature("slow fall"))
    assert(m1.has_feature("extra attack"))
    assert(m1.has_feature("stunning strike"))
    assert(m1.has_feature("ki-empowered strikes"))
    assert(m1.has_feature("stillness of mind"))
    assert(m1.has_feature("evasion"))
    assert(m1.has_feature("purity of body"))
    assert(m1.has_feature("tongue of the sun and moon"))
    assert(m1.has_feature("diamond soul"))
    assert(m1.has_feature("empty body"))
    assert(m1.has_feature("perfect soul"))
    assert(m1.get_ki_points() == 20)
    assert(m1.get_ki_save_dc() == 8 + 6 + 1)
    assert(m1.get_saving_throw("strength") == m1.get_strength() + m1.get_proficiency_mod())
    assert(m1.get_saving_throw("dexterity") == m1.get_dexterity() + m1.get_proficiency_mod())
    assert(m1.get_saving_throw("constitution") == m1.get_constitution() + m1.get_proficiency_mod())
    assert(m1.get_saving_throw("wisdom") == m1.get_wisdom() + m1.get_proficiency_mod())
    assert(m1.get_saving_throw("intelligence") == m1.get_intelligence() + m1.get_proficiency_mod())
    assert(m1.get_saving_throw("charisma") == m1.get_charisma() + m1.get_proficiency_mod())
    assert(m1 is not m0)

def test_rogue():
    r0 = character_classes.Rogue(name="r0", level=20, ac=16, strength_mod=5, dexterity_mod=5, constitution_mod=3,
                                 wisdom_mod=2, intelligence_mod=0, charisma_mod=-2)
    assert(r0.get_level() == 20)
    assert(r0.get_hit_dice() == (20, 8))
    assert(r0.get_max_hp() == 8 + r0.get_constitution() + (5 + r0.get_constitution())*19)
    assert(r0.has_proficiency("simple weapons"))
    assert(r0.has_proficiency("hand crossbow"))
    assert(r0.has_proficiency("longsword"))
    assert(r0.has_proficiency("rapier"))
    assert(r0.has_proficiency("shortsword"))
    assert(r0.has_proficiency("dexterity"))
    assert(r0.has_proficiency("intelligence"))
    assert(r0.get_proficiency_mod() == 6)
    assert(r0.get_sneak_attack_dice() == (10, 6))
    assert(r0.has_feature("sneak attack"))
    assert(r0.has_feature("cunning action"))
    assert(r0.has_feature("uncanny dodge"))
    assert(r0.has_feature("evasion"))
    assert(r0.has_feature("blindsense"))
    assert(r0.has_feature("slippery mind"))
    assert(r0.has_proficiency("wisdom"))
    assert(r0.get_saving_throw("wisdom") == r0.get_wisdom() + r0.get_proficiency_mod())
    assert(r0.has_feature("elusive"))
    assert(r0.has_feature("stroke of luck"))

    assert(r0.can_see("magic"))
    r0.add_condition("deafened")
    assert(not r0.can_see("magic"))
    r0.remove_condition("deafened")

    r0.modify_adv_to_be_hit(1)
    assert(r0.get_adv_to_be_hit() == 0)
    r0.add_condition("incapacitated")
    assert(r0.get_adv_to_be_hit() == 1)
    r0.remove_condition("incapacitated")

    r1 = character_classes.Rogue(copy=r0, name="r1")

    assert(r1.get_max_hp() == 8 + r1.get_constitution() + (5 + r1.get_constitution()) * 19)

    assert(r1.has_proficiency("simple weapons"))
    assert(r1.has_proficiency("hand crossbow"))
    assert(r1.has_proficiency("longsword"))
    assert(r1.has_proficiency("rapier"))
    assert(r1.has_proficiency("shortsword"))
    assert(r1.has_proficiency("dexterity"))
    assert(r1.has_proficiency("intelligence"))
    assert(r1.get_proficiency_mod() == 6)

    assert(r1.get_sneak_attack_dice() == (10, 6))
    assert(r1.has_feature("sneak attack"))
    assert(r1.has_feature("cunning action"))
    assert(r1.has_feature("uncanny dodge"))
    assert(r1.has_feature("evasion"))
    assert(r1.has_feature("blindsense"))
    assert(r1.has_feature("slippery mind"))
    assert(r1.has_proficiency("wisdom"))
    assert(r1.get_saving_throw("wisdom") == r1.get_wisdom() + r1.get_proficiency_mod())
    assert(r1.has_feature("elusive"))
    assert(r1.has_feature("stroke of luck"))

    assert(r1 is not r0)

    assert(r1.can_see("magic"))
    r1.add_condition("deafened")
    assert(not r1.can_see("magic"))
    r1.remove_condition("deafened")

    r1.modify_adv_to_be_hit(1)
    assert(r1.get_adv_to_be_hit() == 0)
    r1.add_condition("incapacitated")
    assert(r1.get_adv_to_be_hit() == 1)
    r1.remove_condition("incapacitated")

def test_spellcaster():
    spell0 = attack_class.Spell(level=1, casting_time="1 minute", components=("v", "s"), duration="instantaneous",
                                school="magic", damage_dice=(1, 8), range=60, name="spell0")

    s0 = combatant.SpellCaster(ac=18, max_hp=70, current_hp=60, temp_hp=2, hit_dice='4d10', speed=30, vision='darkvision',
                               strength=14, dexterity=11, constitution=9, intelligence=12, wisdom=16, charisma=8, name="s0",
                               spell_ability="wisdom", spell_slots={1:3, 2:2, 3:1}, proficiency_mod=2, spells=[spell0])
    assert(s0.get_spell_ability() == "wisdom")
    assert(s0.get_spell_ability_mod() == 3)
    assert(s0.get_spell_save_dc() == 13)
    assert(s0.get_spell_attack_mod() == 5)
    assert(s0.get_level_spell_slots(1) == 3)
    assert(s0.get_level_spell_slots(2) == 2)
    assert(s0.get_level_spell_slots(3) == 1)
    assert(s0.get_level_spell_slots(4) == 0)
    s0.spend_spell_slot(3)
    assert(not s0.get_level_spell_slots(3))
    s0.reset_spell_slots()
    assert(s0.get_spell_slots() == {1:3, 2:2, 3:1})
    assert(spell0 in s0.get_spells())
    assert(spell0.get_attack_mod() == 5)

    s1 = combatant.SpellCaster(copy=s0)
    assert(s1.get_spell_ability() == "wisdom")
    assert(s1.get_spell_ability_mod() == 3)
    assert(s1.get_spell_save_dc() == 13)
    assert(s1.get_spell_attack_mod() == 5)
    assert(s1.get_level_spell_slots(1) == 3)
    assert(s1.get_level_spell_slots(2) == 2)
    assert(s1.get_level_spell_slots(3) == 1)
    assert(s1.get_level_spell_slots(4) == 0)
    s1.spend_spell_slot(3)
    assert(not s1.get_level_spell_slots(3))
    s1.reset_spell_slots()
    assert(s1.get_spell_slots() == {1: 3, 2: 2, 3: 1})
    assert(s1.get_spells()[0].get_attack_mod() == 5)
    assert(s1 is not s0)
    assert(s1.get_spells() is not s0.get_spells())

def test_bard():
    b0 = character_classes.Bard(name="b0", ac=14, strength_mod=-3, dexterity_mod=3, constitution_mod=1, intelligence_mod=0,
                                wisdom_mod=1, charisma_mod=4, level=13)
    assert(b0.get_level() == 13)
    assert(b0.get_hit_dice() == (13, 8))
    assert(b0.get_max_hp() == 8 + 1 + (5+1)*12)
    assert(b0.has_proficiency("simple weapons"))
    assert(b0.has_proficiency("hand crossbow"))
    assert(b0.has_proficiency("longsword"))
    assert(b0.has_proficiency("shortsword"))
    assert(b0.has_proficiency("rapier"))
    assert(b0.has_proficiency("dexterity"))
    assert(b0.has_proficiency("charisma"))
    assert(b0.get_proficiency_mod() == 5)
    assert(b0.get_spell_ability() == "charisma")
    assert(b0.get_spell_slots() == {1: 4, 2: 3, 3: 3, 4: 3, 5: 2, 6: 1, 7: 1})
    assert(b0.has_feature("countercharm"))
    assert(b0.get_inspiration_dice() == (1, 10))
    assert(b0.get_inspiration_slots() == 4)

    b1 = character_classes.Bard(name="b1", copy=b0)
    assert(b1.get_level() == 13)
    assert(b1.get_hit_dice() == (13, 8))
    assert(b1.get_max_hp() == 8 + 1 + (5 + 1) * 12)
    assert(b1.has_proficiency("simple weapons"))
    assert(b1.has_proficiency("hand crossbow"))
    assert(b1.has_proficiency("longsword"))
    assert(b1.has_proficiency("shortsword"))
    assert(b1.has_proficiency("rapier"))
    assert(b1.has_proficiency("dexterity"))
    assert(b1.has_proficiency("charisma"))
    assert(b1.get_proficiency_mod() == 5)
    assert(b1.get_spell_slots() == {1: 4, 2: 3, 3: 3, 4: 3, 5: 2, 6: 1, 7: 1})
    assert(b1.has_feature("countercharm"))
    assert(b1.get_inspiration_dice() == (1, 10))
    assert(b1.get_inspiration_slots() == 4)
    assert(b1 is not b0)

def test_cleric():
    c0 = character_classes.Cleric(name="c0", ac=18, strength_mod=0, dexterity_mod=1, constitution_mod=1, wisdom_mod=3,
                                  intelligence_mod=2, charisma_mod=-2, level=7)
    assert(c0.get_level() == 7)
    assert(c0.get_hit_dice() == (7, 8))
    assert(c0.get_max_hp() == 8 + 1 + (5+1)*6)
    assert(c0.has_proficiency("simple weapons"))
    assert(c0.has_proficiency("wisdom"))
    assert(c0.has_proficiency("charisma"))
    assert(c0.get_proficiency_mod() == 3)
    assert(c0.get_spell_ability() == "wisdom")
    # assert(c0.get_spell_slots == {1: 4, 2: 3, 3: 3, 4: 2})  # TODO: why doesn't this work?
    assert(c0.get_level_spell_slots(1) == 4)
    assert(c0.get_level_spell_slots(2) == 3)
    assert(c0.get_level_spell_slots(3) == 3)
    assert(c0.get_level_spell_slots(4) == 2)
    assert(c0.has_feature("channel divinity"))
    assert(c0.has_feature("destroy undead"))
    assert(c0.get_destroy_undead_cr() == 0.5)

    c1 = character_classes.Cleric(name="c1", copy=c0)
    assert(c1.get_level() == 7)
    assert(c1.get_hit_dice() == (7, 8))
    assert(c1.get_max_hp() == 8 + 1 + (5 + 1) * 6)
    assert(c1.has_proficiency("simple weapons"))
    assert(c1.has_proficiency("wisdom"))
    assert(c1.has_proficiency("charisma"))
    assert(c1.get_proficiency_mod() == 3)
    # assert(c0.get_spell_slots == {1: 4, 2: 3, 3: 3, 4: 2})  TODO: why doesn't this work?
    assert(c1.get_level_spell_slots(1) == 4)
    assert(c1.get_level_spell_slots(2) == 3)
    assert(c1.get_level_spell_slots(3) == 3)
    assert(c1.get_level_spell_slots(4) == 2)
    assert(c1.has_feature("channel divinity"))
    assert(c1.has_feature("destroy undead"))
    assert(c1.get_destroy_undead_cr() == 0.5)
    assert(c1 is not c0)

def test_druid():
    d0 = character_classes.Druid(ac=14, level=5, strength_mod=-1, dexterity_mod=1, constitution_mod=1,
                                 wisdom_mod=3, intelligence_mod=0, charisma_mod=-1, name="d0")
    assert(d0.get_level() == 5)
    assert(d0.get_hit_dice() == (5, 8))
    assert(d0.get_max_hp() == 8 + 1 + (5 + 1)*4)
    assert(d0.has_proficiency("club"))
    assert(d0.has_proficiency("dagger"))
    assert(d0.has_proficiency("javelin"))
    assert(d0.has_proficiency("mace"))
    assert(d0.has_proficiency("quarterstaff"))
    assert(d0.has_proficiency("scimitar"))
    assert(d0.has_proficiency("sickle"))
    assert(d0.has_proficiency("sling"))
    assert(d0.has_proficiency("spear"))
    assert(d0.has_proficiency("intelligence"))
    assert(d0.has_proficiency("wisdom"))
    assert(d0.get_proficiency_mod() == 3)
    assert(d0.get_spell_ability() == "wisdom")
    assert(d0.get_spell_slots() == {1: 4, 2: 3, 3: 2})
    assert(d0.get_wild_shape_slots() == 2)
    assert(d0.get_current_shape() is None)

    d1 = character_classes.Druid(copy=d0, name="d1")
    assert(d1.get_level() == 5)
    assert(d1.get_hit_dice() == (5, 8))
    assert(d1.get_max_hp() == 8 + 1 + (5 + 1) * 4)
    assert(d1.has_proficiency("club"))
    assert(d1.has_proficiency("dagger"))
    assert(d1.has_proficiency("javelin"))
    assert(d1.has_proficiency("mace"))
    assert(d1.has_proficiency("quarterstaff"))
    assert(d1.has_proficiency("scimitar"))
    assert(d1.has_proficiency("sickle"))
    assert(d1.has_proficiency("sling"))
    assert(d1.has_proficiency("spear"))
    assert(d1.has_proficiency("intelligence"))
    assert(d1.has_proficiency("wisdom"))
    assert(d1.get_proficiency_mod() == 3)
    assert(d1.get_spell_slots() == {1: 4, 2: 3, 3: 2})
    assert(d1.get_wild_shape_slots() == 2)
    assert(d1.get_current_shape() is None)
    assert(d1 is not d0)

def test_paladin():
    p0 = character_classes.Paladin(name="p0", level=14, ac=18, strength_mod=1, dexterity_mod=-2, constitution_mod=3,
                                   wisdom_mod=2, intelligence_mod=0, charisma_mod=4, fighting_style="archery")
    assert(p0.get_level() == 14)
    assert(p0.get_hit_dice() == (14, 10))
    assert(p0.get_max_hp() == (10 + 3 + (6+3)*13))
    assert(p0.has_proficiency("simple weapons"))
    assert(p0.has_proficiency("martial weapons"))
    assert(p0.has_proficiency("wisdom"))
    assert(p0.has_proficiency("charisma"))
    assert(p0.get_proficiency_mod() == 5)
    assert(p0.get_spell_slots() == {1: 4, 2: 3, 3: 3, 4: 1})
    assert(p0.has_feature("divine sense"))
    assert(p0.get_divine_sense_slots() == 1 + 4)
    assert(p0.has_feature("lay on hands"))
    assert(p0.get_lay_on_hands_pool() == 14 * 5)
    assert(p0.has_feature("divine smite"))
    assert(p0.has_feature("divine health"))
    assert(p0.has_feature("sacred oath"))
    assert(p0.has_feature("extra attack"))
    assert(p0.has_feature("aura of protection"))
    assert(p0.has_feature("aura of courage"))
    assert(p0.get_aura() == 10)
    assert(p0.has_feature("improved divine smite"))
    assert(p0.has_feature("cleansing touch"))
    assert(p0.get_cleansing_touch_slots() == 4)

    p1 = character_classes.Paladin(copy=p0, name="p1")
    assert(p1.get_level() == 14)
    assert(p1.get_hit_dice() == (14, 10))
    assert(p1.get_max_hp() == (10 + 3 + (6 + 3) * 13))
    assert(p1.has_proficiency("simple weapons"))
    assert(p1.has_proficiency("martial weapons"))
    assert(p1.has_proficiency("wisdom"))
    assert(p1.has_proficiency("charisma"))
    assert(p1.get_proficiency_mod() == 5)
    assert(p1.get_spell_slots() == {1: 4, 2: 3, 3: 3, 4: 1})
    assert(p1.has_feature("divine sense"))
    assert(p1.get_divine_sense_slots() == 1 + 4)
    assert(p1.has_feature("lay on hands"))
    assert(p1.get_lay_on_hands_pool() == 14 * 5)
    assert(p1.has_feature("divine smite"))
    assert(p1.has_feature("divine health"))
    assert(p1.has_feature("sacred oath"))
    assert(p1.has_feature("extra attack"))
    assert(p1.has_feature("aura of protection"))
    assert(p1.has_feature("aura of courage"))
    assert(p1.get_aura() == 10)
    assert(p1.has_feature("improved divine smite"))
    assert(p1.has_feature("cleansing touch"))
    assert(p1.get_cleansing_touch_slots() == 4)
    assert(p1 is not p0)

def test_ranger():
    r0 = character_classes.Ranger(name="r0", level=9, ac=16, strength_mod=3, dexterity_mod=2, constitution_mod=2, wisdom_mod=1,
                                  intelligence_mod=-1, charisma_mod=-2, favored_enemies=("beast", "undead"),
                                  favored_terrains=("forest", "swamp"), fighting_style="defense")
    assert(r0.get_level() == 9)
    assert(r0.get_hit_dice() == (9, 10))
    assert(r0.get_max_hp() == 10 + 2 + (6+2)*8)
    assert(r0.has_proficiency("simple weapons"))
    assert(r0.has_proficiency("martial weapons"))
    assert(r0.has_proficiency("strength"))
    assert(r0.has_proficiency("dexterity"))
    assert(r0.get_proficiency_mod() == 4)
    assert(r0.get_spell_slots() == {1: 4, 2: 3, 3: 2})
    assert(r0.has_feature("favored enemy"))
    assert(r0.has_favored_enemy("beast"))
    assert(r0.has_favored_enemy("undead"))
    assert(r0.has_feature("favored terrain"))
    assert(r0.has_favored_terrain("forest"))
    assert(r0.has_favored_terrain("swamp"))
    assert(r0.has_feature("fighting style"))
    assert(r0.has_fighting_style("defense"))
    assert(r0.has_feature("ranger archetype"))
    assert(r0.has_feature("primeval awareness"))
    assert(r0.has_feature("extra attack"))
    assert(r0.has_feature("land's stride"))

    r1 = character_classes.Ranger(copy=r0, name="r1")
    assert(r1.get_level() == 9)
    assert(r1.get_hit_dice() == (9, 10))
    assert(r1.get_max_hp() == 10 + 2 + (6 + 2) * 8)
    assert(r1.has_proficiency("simple weapons"))
    assert(r1.has_proficiency("martial weapons"))
    assert(r1.has_proficiency("strength"))
    assert(r1.has_proficiency("dexterity"))
    assert(r1.get_proficiency_mod() == 4)
    assert(r1.get_spell_slots() == {1: 4, 2: 3, 3: 2})
    assert(r1.has_feature("favored enemy"))
    assert(r1.has_favored_enemy("beast"))
    assert(r1.has_favored_enemy("undead"))
    assert(r1.get_favored_enemies() is not r0.get_favored_enemies())
    assert(r1.has_feature("favored terrain"))
    assert(r1.has_favored_terrain("forest"))
    assert(r1.has_favored_terrain("swamp"))
    assert(r1.get_favored_terrains() is not r0.get_favored_terrains())
    assert(r1.has_feature("fighting style"))
    assert(r1.has_fighting_style("defense"))
    assert(r1.has_feature("ranger archetype"))
    assert(r1.has_feature("primeval awareness"))
    assert(r1.has_feature("extra attack"))
    assert(r1.has_feature("land's stride"))
    assert(r1 is not r0)

def test_sorcerer():
    s0 = character_classes.Sorcerer(name="s0", level=20, ac=12, strength_mod=-1, dexterity_mod=3, constitution_mod=2,
                                    wisdom_mod=1, intelligence_mod=-1, charisma_mod=3, metamagic=["careful", "distant", "empowered"])
    assert(s0.get_level() == 20)
    assert(s0.get_hit_dice() == (20, 6))
    assert(s0.get_max_hp() == 6 + s0.get_constitution() + (4 + s0.get_constitution())*19)
    assert(s0.has_proficiency("dagger"))
    assert(s0.has_proficiency("dart"))
    assert(s0.has_proficiency("sling"))
    assert(s0.has_proficiency("quarterstaff"))
    assert(s0.has_proficiency("light crossbow"))
    assert(s0.has_proficiency("constitution"))
    assert(s0.has_proficiency("charisma"))
    assert(s0.get_spell_ability() == "charisma")
    assert(s0.get_spell_slots() == {1: 4, 2: 3, 3: 3, 4: 3, 5: 3, 6: 2, 7: 2, 8: 1, 9: 1})
    assert(s0.has_feature("font of magic"))
    assert(s0.get_sorcery_points() == s0.get_full_sorcery_points() == 20)
    assert(s0.has_feature("metamagic"))
    assert(s0.has_metamagic("careful"))
    assert(s0.has_metamagic("distant"))
    assert(s0.has_metamagic("empowered"))

    s1 = character_classes.Sorcerer(copy=s0, name="s1")
    assert(s1.get_level() == 20)
    assert(s1.get_max_hp() == 6 + s1.get_constitution() + (4 + s1.get_constitution()) * 19)
    assert(s1.has_proficiency("dagger"))
    assert(s1.has_proficiency("dart"))
    assert(s1.has_proficiency("sling"))
    assert(s1.has_proficiency("quarterstaff"))
    assert(s1.has_proficiency("light crossbow"))
    assert(s1.has_proficiency("constitution"))
    assert(s1.has_proficiency("charisma"))
    assert(s1.get_spell_slots() == {1: 4, 2: 3, 3: 3, 4: 3, 5: 3, 6: 2, 7: 2, 8: 1, 9: 1})
    assert(s1.has_feature("font of magic"))
    assert(s1.get_sorcery_points() == s1.get_full_sorcery_points() == 20)
    assert(s1.has_feature("metamagic"))
    assert(s1.has_metamagic("careful"))
    assert(s1.has_metamagic("distant"))
    assert(s1.has_metamagic("empowered"))
    assert(s1.get_metamagic() is not s0.get_metamagic())
    assert(s1 is not s0)

def test_warlock():
    w0 = character_classes.Warlock(name="w0", level=20, ac=16, strength_mod=1, dexterity_mod=3, constitution_mod=2,
                                   wisdom_mod=1, intelligence_mod=-1, charisma_mod=3, pact_boon="blade",
                                   eldritch_invocations=["agonizing blast", "armor of shadows", "ascendant step",
                                                         "beast speech", "beguiling influence", "bewitching whispers",
                                                         "book of ancient secrets", "chains of carcerei"])
    assert(w0.get_level() == 20)
    assert(w0.get_hit_dice() == (20, 8))
    assert(w0.get_max_hp() == 8 + w0.get_constitution() + (5 + w0.get_constitution())*19)
    assert(w0.has_proficiency("simple weapons"))
    assert(w0.has_proficiency("wisdom"))
    assert(w0.has_proficiency("charisma"))
    assert(w0.get_proficiency_mod() == 6)
    assert(w0.get_spell_ability() == "charisma")
    assert(w0.get_spell_slots() == {5: 4, 6: 1, 7: 1, 8: 1, 9: 1})
    assert(w0.has_feature("pact boon"))
    assert(w0.get_pact_boon() == "blade")
    assert(w0.has_feature("eldritch invocations"))
    assert(w0.get_eldritch_invocations() == set(["agonizing blast", "armor of shadows", "ascendant step",
            "beast speech", "beguiling influence", "bewitching whispers", "book of ancient secrets", "chains of carcerei"]))

    w1 = character_classes.Warlock(copy=w0, name="w1")
    assert(w1.get_level() == 20)
    assert(w1.get_max_hp() == 8 + w1.get_constitution() + (5 + w1.get_constitution()) * 19)
    assert(w1.has_proficiency("simple weapons"))
    assert(w1.has_proficiency("wisdom"))
    assert(w1.has_proficiency("charisma"))
    assert(w1.get_proficiency_mod() == 6)
    assert(w1.get_spell_slots() == {5: 4, 6: 1, 7: 1, 8: 1, 9: 1})
    assert(w1.has_feature("pact boon"))
    assert(w1.get_pact_boon() == "blade")
    assert(w1.has_feature("eldritch invocations"))
    assert(w1.get_eldritch_invocations() == set(["agonizing blast", "armor of shadows", "ascendant step",
                                                  "beast speech", "beguiling influence", "bewitching whispers",
                                                  "book of ancient secrets", "chains of carcerei"]))
    assert(w1.get_eldritch_invocations() is not w0.get_eldritch_invocations())
    assert(w1 is not w0)

def test_wizard():
    w0 = character_classes.Wizard(name="w0", level=20, ac=12, strength_mod=-2, dexterity_mod=1, constitution_mod=1,
                                  wisdom_mod=3, intelligence_mod=3, charisma_mod=-1, spell_mastery=["a", "b"],
                                  signature_spells=["c", "d"])  # TODO: real spell names instead of placeholders
    assert(w0.get_level() == 20)
    assert(w0.get_hit_dice() == (20, 6))
    assert(w0.get_max_hp() == 6 + w0.get_constitution() + (4 + w0.get_constitution())*19)
    assert(w0.has_proficiency("dagger"))
    assert(w0.has_proficiency("dart"))
    assert(w0.has_proficiency("sling"))
    assert(w0.has_proficiency("light crossbow"))
    assert(w0.has_proficiency("intelligence"))
    assert(w0.has_proficiency("wisdom"))
    assert(w0.get_proficiency_mod() == 6)
    assert(w0.get_spell_ability() == "intelligence")
    assert(w0.get_spell_slots() == {1: 4, 2: 3, 3: 3, 4: 3, 5: 3, 6: 2, 7: 2, 8: 1, 9: 1})
    assert(w0.has_feature("spell mastery"))
    assert(w0.has_spell_mastery("a"))
    assert(w0.has_spell_mastery("b"))
    assert(w0.has_feature("signature spells"))
    assert(w0.has_signature_spell("c"))
    assert(w0.has_signature_spell("d"))

    w1 = character_classes.Wizard(copy=w0, name="w1")
    assert(w1.get_level() == 20)
    assert(w1.get_hit_dice() == (20, 6))
    assert(w1.get_max_hp() == 6 + w1.get_constitution() + (4 + w1.get_constitution()) * 19)
    assert(w1.has_proficiency("dagger"))
    assert(w1.has_proficiency("dart"))
    assert(w1.has_proficiency("sling"))
    assert(w1.has_proficiency("light crossbow"))
    assert(w1.has_proficiency("intelligence"))
    assert(w1.has_proficiency("wisdom"))
    assert(w1.get_proficiency_mod() == 6)
    assert(w1.get_spell_ability() == "intelligence")
    assert(w1.get_spell_slots() == {1: 4, 2: 3, 3: 3, 4: 3, 5: 3, 6: 2, 7: 2, 8: 1, 9: 1})
    assert(w1.get_spell_slots() is not w0.get_spell_slots())
    assert(w1.has_feature("spell mastery"))
    assert(w1.has_spell_mastery("a"))
    assert(w1.has_spell_mastery("b"))
    assert(w1.has_feature("signature spells"))
    assert(w1.has_signature_spell("c"))
    assert(w1.has_signature_spell("d"))
    assert(w1.get_signature_spell_slots() is not w0.get_signature_spell_slots())
    assert(w1 is not w0)

def test_weapon():
    # pylint: disable=unused-variable
    try:
        weapon = weapons.Weapon(name='weapon')
        raise Exception("Create weapon succeeded without damage dice")   # pragma: no cover
    except ValueError:
        pass

    try:
        weapon = weapons.Weapon(damage_dice=(1, 4))
        raise Exception("Create weapon succeeded without name")   # pragma: no cover
    except ValueError:
        pass
    # pylint: enable=unused-variable

    dagger = weapons.Weapon(finesse=1, light=1, range=(20, 60), melee_range=5, name="dagger", damage_dice=(1, 4),
                            attack_bonus=2, damage_bonus=1, damage_type="piercing")
    assert(dagger.get_name() == "dagger")
    props = dagger.get_properties()
    assert("finesse" in props)
    assert(dagger.has_prop("light"))
    assert(dagger.has_prop("range"))
    assert(dagger.get_range() == (20, 60))
    assert(dagger.has_prop("melee"))
    assert(dagger.get_melee_range() == 5)
    assert(dagger.get_damage_dice() == (1, 4))
    assert(not dagger.has_prop("heavy"))
    assert(not dagger.has_prop("load"))
    assert(not dagger.has_prop("reach"))
    assert(not dagger.has_prop("two_handed"))
    assert(not dagger.has_prop("versatile"))
    assert(dagger.get_attack_bonus() == 2)
    assert(dagger.get_damage_bonus() == 1)
    assert(dagger.get_damage_type() == "piercing")

    dagger_2 = weapons.Weapon(copy=dagger, name="dagger_2")
    assert(dagger_2.get_name() == "dagger_2")
    props = dagger_2.get_properties()
    assert("finesse" in props)
    assert(dagger_2.has_prop("light"))
    assert(dagger_2.has_prop("range"))
    assert(dagger_2.get_range() == (20, 60))
    assert(dagger_2.has_prop("melee"))
    assert(dagger_2.get_melee_range() == 5)
    assert(dagger_2.get_damage_dice() == (1, 4))
    assert(not dagger_2.has_prop("heavy"))
    assert(not dagger_2.has_prop("load"))
    assert(not dagger_2.has_prop("reach"))
    assert(not dagger_2.has_prop("two_handed"))
    assert(not dagger_2.has_prop("versatile"))
    assert(dagger_2.get_attack_bonus() == 2)
    assert(dagger_2.get_damage_bonus() == 1)
    assert(dagger_2.get_damage_type() == "piercing")
    assert(dagger is not dagger_2)  # make sure they aren't the same object

    t0 = combatant.Combatant(ac=12, max_hp=20, current_hp=20, temp_hp=2, hit_dice='3d6', speed=20, vision='darkvision',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                             name='t0')

    t0.add_weapon(dagger)
    assert(t0.get_weapons() == [dagger])
    assert(dagger.get_owner() is t0)

    t1 = combatant.Combatant(copy=t0, name="t1")
    assert(len(t1.get_weapons()) == 1)
    dagger_3 = t1.get_weapons()[0]
    assert(dagger_3.get_name() == "dagger")
    props = dagger_3.get_properties()
    assert("finesse" in props)
    assert(dagger_3.has_prop("light"))
    assert(dagger_3.has_prop("range"))
    assert(dagger_3.get_range() == (20, 60))
    assert(dagger_3.has_prop("melee"))
    assert(dagger_3.get_melee_range() == 5)
    assert(dagger_3.get_damage_dice() == (1, 4))
    assert(not dagger_3.has_prop("heavy"))
    assert(not dagger_3.has_prop("load"))
    assert(not dagger_3.has_prop("reach"))
    assert(not dagger_3.has_prop("two_handed"))
    assert(not dagger_3.has_prop("versatile"))
    assert(dagger_3.get_attack_bonus() == 2)
    assert(dagger_3.get_damage_bonus() == 1)
    assert(dagger_3.get_damage_type() == "piercing")
    assert(dagger is not dagger_3)

    try:
        t0.add_weapon('stuff')
        raise Exception("Add weapon succeeded for non-weapon")   # pragma: no cover
    except ValueError:
        pass

def test_attack():
    try:
        a0 = attack_class.Attack(damage_dice=(1, 8), attack_mod=5.2)
        raise Exception("Create Attack succeeded with non-int attack mod")   # pragma: no cover
    except ValueError:
        pass

    try:
        a0 = attack_class.Attack(damage_dice=(1, 8), damage_mod=["fail"])
        raise Exception("Create Attack succeeded with non-int damage mod")   # pragma: no cover
    except ValueError:
        pass

    try:
        a0 = attack_class.Attack(damage_dice=(1, 8), name=1)
        raise Exception("Create Attack succeeded with non-string name")   # pragma: no cover
    except ValueError:
        pass

    a0 = attack_class.Attack(damage_dice=(1, 8), attack_mod=5, damage_mod=2, damage_type="piercing", range=120, adv=-1,
                             melee_range=10, name="a0")
    assert(a0.get_damage_dice() == (1, 8))
    assert(a0.get_attack_mod() == 5)
    assert(a0.get_damage_mod() == 2)
    assert(a0.get_damage_type() == "piercing")
    assert(a0.get_range() == 120)
    assert(a0.get_melee_range() == 10)
    assert(a0.get_adv() == -1)
    assert(a0.get_name() == "a0")
    assert(a0.get_max_hit() == 25)
    assert(a0.get_average_hit() == 15)
    assert(a0.get_max_damage() == 10)
    assert(a0.get_average_damage() == 6)

    dagger = weapons.Weapon(finesse=1, light=1, range=(20, 60), melee_range=5, name="dagger", damage_dice=(1, 4),
                            attack_bonus=2, damage_bonus=1, damage_type="piercing")  # cool damage thing

    t0 = combatant.Combatant(ac=12, max_hp=40, current_hp=40, temp_hp=2, hit_dice='3d6', speed=20, vision='darkvision',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                             name='t0')

    t0.add_weapon(dagger)
    assert(dagger in t0.get_weapons())
    assert(dagger.get_owner() is t0)
    assert(len(t0.get_attacks()) == 3)  # melee and two ranged

    dagger_attacks = t0.get_attacks()

    dagger_attack = dagger_attacks[0]
    assert(dagger_attack.get_name() == "dagger_range")
    assert(dagger_attack.get_weapon() is dagger)
    assert(dagger_attack.get_damage_dice() == (1, 4))
    assert(dagger_attack.get_damage_type() == "piercing")
    assert(dagger_attack.get_attack_mod() == 5)  # 3 + 2
    assert(dagger_attack.get_damage_mod() == 4)  # 3 + 1
    assert(dagger_attack.get_range() == 20)
    assert(dagger_attack.get_melee_range() == 0)
    assert(dagger_attack.get_adv() == 0)

    dagger_attack = attack_class.Attack(copy=dagger_attack)
    assert(dagger_attack.get_name() == "dagger_range")
    assert(dagger_attack.get_weapon() is dagger)
    assert(dagger_attack.get_damage_dice() == (1, 4))
    assert(dagger_attack.get_damage_type() == "piercing")
    assert(dagger_attack.get_attack_mod() == 5)  # 3 + 2
    assert(dagger_attack.get_damage_mod() == 4)  # 3 + 1
    assert(dagger_attack.get_range() == 20)
    assert(dagger_attack.get_melee_range() == 0)
    assert(dagger_attack.get_adv() == 0)

    dagger_attack2 = dagger_attack.get_copy()
    assert(dagger_attack2.get_name() == "dagger_range")
    assert(dagger_attack2.get_weapon() is dagger)
    assert(dagger_attack2.get_damage_dice() == (1, 4))
    assert(dagger_attack2.get_damage_type() == "piercing")
    assert(dagger_attack2.get_attack_mod() == 5)  # 3 + 2
    assert(dagger_attack2.get_damage_mod() == 4)  # 3 + 1
    assert(dagger_attack2.get_range() == 20)
    assert(dagger_attack2.get_melee_range() == 0)
    assert(dagger_attack2.get_adv() == 0)
    assert(dagger_attack2 is not dagger_attack)

    dagger_attack = dagger_attacks[1]
    assert(dagger_attack.get_name() == "dagger_range_disadvantage")
    assert(dagger_attack.get_damage_dice() == (1, 4))
    assert(dagger_attack.get_damage_type() == "piercing")
    assert(dagger_attack.get_attack_mod() == 5)
    assert(dagger_attack.get_damage_mod() == 4)
    assert(dagger_attack.get_range() == 60)
    assert(dagger_attack.get_melee_range() == 0)
    assert(dagger_attack.get_adv() == -1)

    dagger_attack = dagger_attacks[2]
    assert(dagger_attack.get_name() == "dagger_melee")
    assert(dagger_attack.get_damage_dice() == (1, 4))
    assert(dagger_attack.get_damage_type() == "piercing")
    assert(dagger_attack.get_attack_mod() == 5)
    assert(dagger_attack.get_damage_mod() == 4)
    assert(dagger_attack.get_range() == 0)
    assert(dagger_attack.get_melee_range() == 5)
    assert(dagger_attack.get_adv() == 0)

    t0.remove_weapon_attacks(dagger)
    assert(not t0.get_attacks())

    t0.remove_weapon(dagger)
    assert(not t0.get_weapons())
    assert(not t0.get_attacks())

    dagger.set_name("dagger")

    mace = weapons.Weapon(name="mace", damage_dice=(1, 6), damage_type="bludgeoning")
    assert(mace.get_melee_range() == 5)

    t0.add_weapon(dagger)
    t0.add_weapon(mace)

    assert(len(t0.get_attacks()) == 4)
    t0.remove_weapon(mace)
    assert(len(t0.get_attacks()) == 3)

    attack_names = [attack.get_name() for attack in t0.get_attacks()]
    assert(attack_names[0] == "dagger_range")
    assert(attack_names[1] == "dagger_range_disadvantage")
    assert(attack_names[2] == "dagger_melee")

    t0.add_weapon(mace)
    t0.remove_weapon(dagger)
    assert(len(t0.get_attacks()) == 1)
    assert(t0.get_attacks()[0].get_name() == "mace_melee")
    t0.add_weapon(dagger)

    t0.remove_all_weapons()
    assert(not t0.get_weapons())
    assert(not t0.get_attacks())

    dagger.set_attack_bonus(0)
    dagger.set_damage_bonus(0)

    t0.add_weapon(dagger)
    t1 = combatant.Combatant(ac=10, max_hp=40, current_hp=40, temp_hp=2, hit_dice='3d6', speed=20, vision='darkvision',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                             name='t1')  # almost identical to t0, but easier to hit
    t1.add_weapon(mace)

    dagger_melee = t0.get_attacks()[2]
    mace_melee = t1.get_attacks()[0]

#   t0.set_verbose(True)
#   t1.set_verbose(True)

    for i in range(15):  # pylint: disable=unused-variable
        t0.send_attack(t1, dagger_melee)
        if t1.has_condition("unconscious") or t1.has_condition("dead"):  # pragma: no cover
            break
        t1.send_attack(t0, mace_melee)
        if t0.has_condition("unconscious") or t0.has_condition("dead"):  # pragma: no cover
            break

    t0.set_verbose(False)
    t1.set_verbose(False)

def test_saving_throw_attack():
    mace_2 = weapons.Weapon(name="mace", damage_dice=(1, 6), damage_type="bludgeoning")

    # Attack of the clones!
    t2 = combatant.Combatant(ac=12, max_hp=60, current_hp=60, hit_dice='3d6', speed=20, vision='darkvision',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                             name='t2', verbose=False)  # TODO: re-enable verbose
    t2.add_weapon(mace_2)

    t3 = combatant.Combatant(copy=t2, name="t3")
    mace_3 = weapons.Weapon(copy=mace_2)
    t3.add_weapon(mace_3)

    sunburn_2 = attack_class.SavingThrowAttack(damage_dice=(1, 8), dc=12, save_type="dexterity", damage_on_success=False,
                                               attack_mod=0, damage_mod=0, damage_type="radiant", name="Sunburn")
    assert(sunburn_2.get_damage_dice() == (1, 8))
    assert(sunburn_2.get_dc() == 12)
    assert(sunburn_2.get_save_type() == "dexterity")
    assert(not sunburn_2.get_damage_on_success())
    assert(sunburn_2.get_attack_mod() == 0)
    assert(sunburn_2.get_damage_mod() == 0)
    assert(sunburn_2.get_damage_type() == "radiant")
    assert(sunburn_2.get_name() == "Sunburn")
    sunburn_3 = attack_class.SavingThrowAttack(copy=sunburn_2)
    assert(sunburn_3.get_damage_dice() == (1, 8))
    assert(sunburn_3.get_dc() == 12)
    assert(sunburn_3.get_save_type() == "dexterity")
    assert(not sunburn_3.get_damage_on_success())
    assert(sunburn_3.get_attack_mod() == 0)
    assert(sunburn_3.get_damage_mod() == 0)
    assert(sunburn_3.get_damage_type() == "radiant")
    assert(sunburn_3.get_name() == "Sunburn")

    t2.add_attack(sunburn_2)
    t2_attacks = t2.get_attacks()
    t3.add_attack(sunburn_3)
    t3_attacks = t3.get_attacks()

    for i in range(30):  # pylint: disable=unused-variable
        t2.send_attack(t3, random.choice(t2_attacks))
        if t3.has_condition("unconscious") or t3.has_condition("dead"):  # pragma: no cover
            break
        t3.send_attack(t2, random.choice(t3_attacks))
        if t2.has_condition("unconscious") or t2.has_condition("dead"):  # pragma: no cover
            break

def test_spell():
    spell0 = attack_class.Spell(level=1, casting_time="1 minute", components=("v", "s"), duration="instantaneous",
                                school="magic", damage_dice=(1,8), range=60, name="spell0")
    assert(spell0.get_level() == 1)
    assert(spell0.get_casting_time() == 10)
    assert(spell0.get_components() == ["v", "s"])
    assert(spell0.get_duration() == 0)
    assert(spell0.get_school() == "magic")
    spell1 = attack_class.Spell(copy=spell0)
    assert(spell1.get_level() == 1)
    assert(spell1.get_casting_time() == 10)
    assert(spell1.get_components() == ["v", "s"])
    assert(spell1.get_duration() == 0)
    assert(spell1.get_school() == "magic")

def test_saving_throw_spell():
    sunburn0 = attack_class.SavingThrowSpell(damage_dice=(1, 8), level=0, school="evocation",
                                             casting_time="1 action", components=["v", "s"], duration="instantaneous",
                                             dc=12, save_type="dexterity", damage_on_success=False,
                                             damage_type="radiant", name="Sunburn")
    assert(sunburn0.get_damage_dice() == (1, 8))
    assert(sunburn0.get_level() == 0)
    assert(sunburn0.get_school() == "evocation")
    assert(sunburn0.get_casting_time() == "1 action")
    assert(sunburn0.get_components() == ["v", "s"])
    assert(sunburn0.get_duration() == 0)
    assert(sunburn0.get_dc() == 12)
    assert(sunburn0.get_save_type() == "dexterity")
    assert(not sunburn0.get_damage_on_success())
    assert(sunburn0.get_attack_mod() == 0)
    assert(sunburn0.get_damage_mod() == 0)
    assert(sunburn0.get_damage_type() == "radiant")
    assert(sunburn0.get_name() == "Sunburn")

    s0 = combatant.SpellCaster(ac=18, max_hp=70, current_hp=60, temp_hp=2, hit_dice='4d10', speed=30, vision='darkvision',
                               strength=14, dexterity=11, constitution=9, intelligence=12, wisdom=16, charisma=8,
                               name="s0", spell_ability="wisdom", spell_slots={1: 3, 2: 2, 3: 1}, proficiency_mod=2,
                               spells=[sunburn0], verbose=False)  # TODO: re-enable verbose
    s1 = combatant.SpellCaster(copy=s0, name="s1")
    sunburn1 = s1.get_spells()[0]
    for i in range(30):  # pylint: disable=unused-variable
        s0.send_attack(s1, sunburn0)
        if s1.has_condition("unconscious") or s1.has_condition("dead"):  # pragma: no cover
            break
        s1.send_attack(s0, sunburn1)
        if s0.has_condition("unconscious") or s0.has_condition("dead"):  # pragma: no cover
            break

def test_healing_spell():
    t0 = combatant.Combatant(ac=12, max_hp=70, current_hp=5, temp_hp=2, hit_dice='3d6', speed=20, vision='darkvision',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                             proficiencies=["dexterity", "charisma"], proficiency_mod=2, name='t0', verbose=False)  # TODO: re-enable verbose
    t1 = combatant.Combatant(ac=12, max_hp=70, current_hp=0, temp_hp=2, hit_dice='3d6', speed=20, vision='darkvision',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                             proficiencies=["dexterity", "charisma"], proficiency_mod=2, name='t1', verbose=False)  # TODO: re-enable verbose
    cure_wounds = attack_class.HealingSpell(damage_dice=(1, 8), level=1, casting_time="1 action", components=["v", "s"],
                                            duration="instantaneous", name="Cure Wounds")
    s0 = combatant.SpellCaster(ac=18, max_hp=70, current_hp=60, temp_hp=2, hit_dice='4d10', speed=30, vision='darkvision',
                               strength=14, dexterity=11, constitution=9, intelligence=12, wisdom=16, charisma=8,
                               name="s0", spell_ability="wisdom", spell_slots={1: 30}, proficiency_mod=2,
                               spells=[cure_wounds], verbose=False)  # TODO: re-enable verbose
    targets = [t0, t1]
    for i in range(30):  # pylint: disable=unused-variable
        target = random.choice(targets)
        s0.send_attack(target=target, attack=cure_wounds)
        if t0.is_hp_max() or t1.is_hp_max():
            break

def test_read_from_web():
    a0 = bestiary.Aboleth(name="a0")
    assert(isinstance(a0, combatant.Creature))
    assert(isinstance(a0, bestiary.Aboleth))
    assert(a0.get_name() == "a0")
    assert(a0.get_size() == "large")
    assert(a0.get_creature_type() == "aberration")
    assert(a0.get_ac() == 17)
    assert(a0.get_max_hp() == 135)
    assert(a0.get_current_hp() == 135)
    assert(a0.get_speed() == 10)
    assert(a0.get_strength() == 5)
    assert(a0.get_dexterity() == -1)
    assert(a0.get_constitution() == 2)
    assert(a0.get_intelligence() == 4)
    assert(a0.get_wisdom() == 2)
    assert(a0.get_charisma() == 4)
    assert(a0.get_cr() == 10)
    assert(a0.get_proficiency_mod() == 4)
    assert(a0.get_saving_throw("constitution") == 6)
    assert(a0.get_saving_throw("intelligence") == 8)
    assert(a0.get_saving_throw("wisdom") == 6)

def test_armory():
    dagger = armory.Dagger()
    assert(isinstance(dagger, weapons.Weapon))
    assert(isinstance(dagger, armory.SimpleWeapon))
    assert(isinstance(dagger, armory.MeleeWeapon))
    assert(dagger.get_damage_dice() == (1, 4))
    assert(dagger.get_damage_type() == "piercing")
    assert(dagger.has_prop("finesse"))
    assert(dagger.has_prop("light"))
    assert(dagger.get_melee_range() == 5)
    assert(dagger.get_range() == (20, 60))
    assert(armory.is_monk_weapon(dagger))  # not a Monk weapon because it is not ranged

    t0 = combatant.Combatant(ac=12, max_hp=20, current_hp=20, temp_hp=2, hit_dice='3d6', speed=20, vision='darkvision',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                             proficiencies=["dexterity", "charisma", "simple weapons", "war pick"], proficiency_mod=2, name='t0')
    assert(t0.has_weapon_proficiency(dagger))

    war_pick = armory.War_Pick()  # choose this one to make sure multi_word names are handled correctly for proficiency
    assert(isinstance(war_pick, armory.MartialWeapon))
    assert(isinstance(war_pick, armory.MeleeWeapon))
    assert(t0.has_weapon_proficiency(war_pick))
    assert(not armory.is_monk_weapon(war_pick))  # not monk because it is Martial

    shortbow = armory.Shortbow()  # ranged simple
    assert(isinstance(shortbow, armory.RangedWeapon))
    assert(not armory.is_monk_weapon(shortbow))  # not monk because it is Ranged

    shortsword = armory.Shortsword()
    assert(armory.is_monk_weapon(shortsword))

def test_tactics():
    rand_tactic = tactics.Tactic(name="random")
    assert(rand_tactic.get_name() == "random")
    assert(rand_tactic.get_tiebreakers() == [])
    rand_tactic2 = tactics.Tactic(copy=rand_tactic, name="random2")
    assert(rand_tactic2.get_name() == "random2")
    assert(rand_tactic2.get_tiebreakers() == [])
    assert(rand_tactic is not rand_tactic2)

def test_combatant_tactics():
    lowest_ac_tactic = combatant_tactics.LowestAcTactic()
    c10 = combatant.Combatant(ac=10, max_hp=20, current_hp=20, temp_hp=2, hit_dice='3d6', speed=20, vision='darkvision',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="c10")
    d10 = combatant.Combatant(copy=c10, name="d10")
    c11 = combatant.Combatant(ac=11, max_hp=20, current_hp=20, temp_hp=2, hit_dice='3d6', speed=20, vision='darkvision',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="c10")
    c12 = combatant.Combatant(ac=12, max_hp=20, current_hp=20, temp_hp=2, hit_dice='3d6', speed=20, vision='darkvision',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="c10")
    assert(lowest_ac_tactic.run_tactic([c10, c11, c12]) == [c10])
    ac10 = lowest_ac_tactic.run_tactic([c10, d10, c11, c12])
    assert(len(ac10) == 2 and c10 in ac10 and d10 in ac10)

    highest_ac_tactic = combatant_tactics.HighestAcTactic()
    d12 = combatant.Combatant(copy=c12, name="d12")
    assert(highest_ac_tactic.run_tactic([c10, d10, c11, c12]) == [c12])
    ac12 = highest_ac_tactic.run_tactic([c10, d10, c11, c12, d12])
    assert(len(ac12) == 2 and c12 in ac12 and d12 in ac12)

    low_ac_tactic = combatant_tactics.LowAcTactic(threshold=11)
    low_ac_result = low_ac_tactic.run_tactic([c10, d10, c11, c12, d12])
    assert(len(low_ac_result) == 2 and c10 in low_ac_result and d10 in low_ac_result)
    low_ac_tactic = combatant_tactics.LowAcTactic(threshold=12)
    low_ac_result = low_ac_tactic.run_tactic([c10, d10, c11, c12, d12])
    assert(len(low_ac_result) == 3 and c10 in low_ac_result and d10 in low_ac_result and c11 in low_ac_result)
    a0 = attack_class.Attack(damage_dice=(1, 8), attack_mod=5, damage_mod=2, damage_type="piercing", range=120, adv=-1,
                             melee_range=10, name="a0")
    assert(combatant_tactics.LowAcTactic(attack=a0, use_max=True)._threshold == 25 + 1)  # pylint: disable=protected-access
    assert(combatant_tactics.LowAcTactic(attack=a0)._threshold == 15 + 1)  # pylint: disable=protected-access

    high_ac_tactic = combatant_tactics.HighAcTactic(threshold=10)
    high_ac_result = high_ac_tactic.run_tactic([c10, d10, c11, c12, d12])
    assert(len(high_ac_result) == 3 and c11 in high_ac_result and c12 in high_ac_result and d12 in high_ac_result)
    high_ac_tactic = combatant_tactics.HighAcTactic(threshold=11)
    high_ac_result = high_ac_tactic.run_tactic([c10, d10, c11, c12, d12])
    assert(len(high_ac_result) == 2 and c12 in high_ac_result and d12 in high_ac_result)

    bloodied_tactic = combatant_tactics.BloodiedTactic()
    c10 = combatant.Combatant(ac=10, max_hp=20, current_hp=10, hit_dice='3d6', speed=20, vision='darkvision',
                              strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                              name="c10")
    d10 = combatant.Combatant(copy=c10)
    c11 = combatant.Combatant(ac=10, max_hp=30, current_hp=8, hit_dice='3d6', speed=20, vision='darkvision',
                              strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                              name="c11")
    c12 = combatant.Combatant(ac=12, max_hp=20, current_hp=20, hit_dice='3d6', speed=20, vision='darkvision',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="c12")
    assert(bloodied_tactic.run_tactic([c10, c12]) == [c10])
    bloodied_result = bloodied_tactic.run_tactic([c10, d10, c11, c12])
    assert(len(bloodied_result) == 3 and c10 in bloodied_result and d10 in bloodied_result and c11 in bloodied_result)

    max_hp_tactic = combatant_tactics.MaxHpTactic()
    max_hp_result = max_hp_tactic.run_tactic([c10, d10, c11, c12])
    assert(max_hp_result == [c12])
    d12 = combatant.Combatant(ac=12, max_hp=35, current_hp=35, hit_dice='3d6', speed=20, vision='darkvision',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="d12")
    max_hp_result = max_hp_tactic.run_tactic([c10, d10, c11, c12, d12])
    assert(len(max_hp_result) == 2 and c12 in max_hp_result and d12 in max_hp_result)

    hp_to_max_high_tactic = combatant_tactics.HpToMaxHighTactic(threshold=9)
    hp_to_max_high_result = hp_to_max_high_tactic.run_tactic([c10, d10, c11, c12, d12])
    assert(len(hp_to_max_high_result) == 3 and c10 in hp_to_max_high_result and d10 in hp_to_max_high_result and c11 in hp_to_max_high_result)
    healing_spell = attack_class.HealingSpell(damage_dice=(1, 8), damage_mod=1, name="healing", level=1, casting_time="1 action",
                                              duration="1 minute", components=["v", "s"])
    c13 = combatant.Combatant(ac=12, max_hp=35, current_hp=30, hit_dice='3d6', speed=20, strength=14, dexterity=16,
                              constitution=9, intelligence=12, wisdom=11, charisma=8, name="c13")
    hp_to_max_high_tactic = combatant_tactics.HpToMaxHighTactic(healing=healing_spell)
    hp_to_max_high_result = hp_to_max_high_tactic.run_tactic([c10, d10, c11, c12, d12, c13])
    assert(len(hp_to_max_high_result) == 3 and c10 in hp_to_max_high_result and d10 in hp_to_max_high_result and c11 in hp_to_max_high_result)
    hp_to_max_high_tactic = combatant_tactics.HpToMaxHighTactic(healing=healing_spell, use_max=False)
    hp_to_max_high_result = hp_to_max_high_tactic.run_tactic([c10, d10, c11, c12, d12, c13])
    assert(len(hp_to_max_high_result) == 4 and c10 in hp_to_max_high_result and d10 in hp_to_max_high_result
            and c11 in hp_to_max_high_result and c13 in hp_to_max_high_result)

    murder_hobo_tactic = combatant_tactics.MurderHoboTactic()
    c10.take_damage(10)  # go unconscious
    c11.take_damage(68)  # die instantly
    c12.take_damage(20)
    c12.take_damage(40)  # die
    c13.take_damage(31)  # go unconscious
    # d10, d12, and c13 are the same
    murder_hobo_result = murder_hobo_tactic.run_tactic([c10, d10, c11, c12, d12, c13])
    assert(len(murder_hobo_result) == 2 and c10 in murder_hobo_result and c13 in murder_hobo_result)

    has_temp_hp_tactic = combatant_tactics.HasTempHpTactic()
    c10.set_temp_hp(5)
    c12.set_temp_hp(1)
    c13.set_temp_hp(0)
    has_temp_hp_result = has_temp_hp_tactic.run_tactic([c10, d10, c11, c12, d12, c13])
    assert(len(has_temp_hp_result) == 2 and c10 in has_temp_hp_result and c12 in has_temp_hp_result)

    no_temp_hp_tactic = combatant_tactics.NoTempHpTactic()
    no_temp_hp_result = no_temp_hp_tactic.run_tactic([c10, d10, c11, c12, d12, c13])
    assert(len(no_temp_hp_result) == 4 and d10 in no_temp_hp_result and c11 in no_temp_hp_result
            and d12 in no_temp_hp_result and c13 in no_temp_hp_result)

    c10.add_vulnerability("piercing")
    d10.add_resistance("piercing")
    c11.add_immunity("piercing")
    has_vulnerability_tactic = combatant_tactics.HasVulnerabilityTactic(damage_type="piercing")
    assert(has_vulnerability_tactic.run_tactic([c10, d10, c11, c12, d12, c13]) == [c10])
    has_resistance_tactic = combatant_tactics.HasResistanceTactic(damage_type="piercing")
    assert(has_resistance_tactic.run_tactic([c10, d10, c11, c12, d12, c13]) == [d10])
    has_immunity_tactic = combatant_tactics.HasImmunityTactic(damage_type="piercing")
    assert(has_immunity_tactic.run_tactic([c10, d10, c11, c12, d12, c13]) == [c11])
