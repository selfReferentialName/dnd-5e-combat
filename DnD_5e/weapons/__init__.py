from typing import Tuple, Optional
from DnD_5e.utility_methods_dnd import validate_dice, TYPE_DICE_TUPLE

TYPE_RANGE_TUPLE = Tuple[int, int]

class Weapon:
    def __init__(self, **kwargs):
        copy_weapon = kwargs.get("copy")
        if copy_weapon:
            self.copy_constructor(copy_weapon, name=kwargs.get("name"))
            return
        self._name = kwargs.get('name')  # needed for verbose output
        if not self._name:
            raise ValueError("Must provide name")
        finesse = kwargs.get('finesse', 0)
        light = kwargs.get('light', 0)
        heavy = kwargs.get('heavy', 0)
        loading = kwargs.get('loading', 0)
        self._range = kwargs.get('range', 0)  # range is 0 or a tuple. For our purposes it's the same property as thrown
        if isinstance(self._range, tuple):  # lists are mutable and that could be a problem
            if len(self._range) != 2:
                raise ValueError("Must provide exactly two values: normal range and disadvantage range")
            if not isinstance(self._range[0], int) or not isinstance(self._range[1], int):
                raise ValueError("Must provide exactly two integer values: normal range and disadvantage range")
        elif isinstance(self._range, str):
            try:
                self._range = tuple(int(x) for x in self._range.split("/"))
            except ValueError:
                raise ValueError("Must provide range in tuple of two ints (20, 60) or string format 20/60")
            if len(self._range) != 2:
                raise ValueError("Must provide range in tuple of two ints (20, 60) or string format 20/60")
        elif self._range != 0:
            raise ValueError("Must provide range in tuple of two ints (1, 6) or string format 1d6, or 0 if this is melee weapon")
        self._melee_range = kwargs.get('melee_range', 0)
        if not self._range and not self._melee_range:  # assume this is a melee weapon if not otherwise specified
            self._melee_range = 5
        self._reach = kwargs.get('reach', 0)
        if self._reach and self._melee_range:
            self._melee_range += 5
        two_handed = kwargs.get('two_handed', 0)
        self._versatile = kwargs.get('versatile', 0)
        try:
            self._versatile = validate_dice(self._versatile)
        except ValueError:
            self._versatile = 0

        self._props = set()
        if finesse:
            self._props.add("finesse")
        if light:
            self._props.add("light")
        elif heavy:
            self._props.add("heavy")
        if loading:
            self._props.add("loading")
        if self._range:
            self._props.add("range")
        if self._melee_range:
            self._props.add("melee")
        if self._reach:
            self._props.add("reach")
        if two_handed:
            self._props.add("two_handed")
        elif self._versatile:
            self._props.add("versatile")

        self._damage_dice = validate_dice(kwargs.get('damage_dice'))
        self._damage_type = kwargs.get("damage_type")
        if not isinstance(self._damage_type, str):
            raise ValueError("Damage type should be a string")
        self._owner = kwargs.get('owner', None)
        self._attack_bonus = kwargs.get('attack_bonus', 0)
        self._damage_bonus = kwargs.get('damage_bonus', 0)

    def copy_constructor(self, other, name=""):
        """
        Sets instance variables from another Weapon. Warning: this overrides any existing values in self.
        :param other: another Weapon
        :return:
        """
        if other == self:  # don't bother copying yourself
            return
        if not isinstance(other, Weapon):
            raise ValueError("Cannot make self a copy of something that is not a Weapon")
        if not name or not isinstance(name, str):
            name = other.get_name()
        self.__init__(range=other.get_range(), melee_range=other.get_melee_range(), finesse=other.has_prop("finesse"),
                      light=other.has_prop("light"), heavy=other.has_prop("heavy"), loading=other.has_prop("loading"),
                      reach=other.has_prop("reach"), two_handed=other.has_prop("two_handed"), versatile=other.has_prop("versatile"),
                      damage_dice=other.get_damage_dice(), damage_type=other.get_damage_type(), name=name,
                      attack_bonus=other.get_attack_bonus(), damage_bonus=other.get_damage_bonus())
        self._versatile = other.get_versatile()

    def get_properties(self) -> set:
        return self._props

    def has_prop(self, prop) -> bool:
        return prop in self._props

    def get_range(self) -> TYPE_RANGE_TUPLE:
        return self._range

    def get_melee_range(self) -> int:
        return self._melee_range

    def get_damage_dice(self) -> TYPE_DICE_TUPLE:
        return self._damage_dice

    def get_damage_type(self) -> str:
        return self._damage_type

    def get_versatile(self) -> Optional[TYPE_DICE_TUPLE]:  # damage dice for versatile weapon
        return self._versatile

    def get_name(self) -> str:
        return self._name

    def get_owner(self):
        return self._owner

    def get_attack_bonus(self) -> int:
        return self._attack_bonus

    def get_damage_bonus(self) -> int:
        return self._damage_bonus

    def set_name(self, name: str):
        if name == self._name:
            return
        if not isinstance(name, str):
            raise ValueError("Name must be a string")
        self._name = name

    def set_owner(self, owner):
        self._owner = owner

    def set_attack_bonus(self, attack_bonus):
        self._attack_bonus = attack_bonus

    def set_damage_bonus(self, damage_bonus):
        self._damage_bonus = damage_bonus
