import warnings
from typing import Optional
from DnD_5e.combatant import Character, SpellCaster, Combatant, Creature
from DnD_5e.attack_class import Spell
from DnD_5e.utility_methods_dnd import ability_to_mod, proficiency_bonus_per_level, roll_dice, calc_advantage, TYPE_DICE_TUPLE
from DnD_5e import armory


class Barbarian(Character):
    def __init__(self, **kwargs):
        copy = kwargs.get("copy")
        if copy:
            self.copy_constructor(copy, name=kwargs.get("name"))
            return

        level = kwargs.get("level")
        if not level:
            raise ValueError("No level provided or level is 0")
        if not isinstance(level, int) or level < 1 or level > 20:
            raise ValueError("Level must be an integer between 1 and 20")
        hit_dice = (1 * level, 12)

        constitution = kwargs.get('constitution')
        if not constitution:
            constitution_mod = kwargs.get("constitution_mod")
            if constitution_mod is not None:
                if isinstance(constitution_mod, int):
                    self._constitution = constitution_mod
                else:
                    raise ValueError("Constitution mod must be an integer")
            else:
                raise ValueError("Must provide constitution score or modifier")
        else:  # pragma: no cover
            constitution_mod = ability_to_mod(constitution)

        max_hp = kwargs.get("max_hp")
        if max_hp and (not isinstance(max_hp, int) or max_hp <= 0):
            raise ValueError("Must provide positive integer max hp")
        else:
            max_hp = 12 + constitution_mod
            if level > 1:
                max_hp += (7 + constitution_mod) * (level - 1)

        proficiencies = kwargs.get('proficiencies', [])
        if isinstance(proficiencies, (tuple, list)):
            proficiencies = set(proficiencies)
        elif isinstance(proficiencies, set):  # pragma: no cover
            pass
        elif proficiencies is None:  # pragma: no cover
            proficiencies = set()
        else:
            raise ValueError("Proficiencies must be provided as a set, list, or tuple")
        proficiencies.add("simple weapons")
        proficiencies.add("martial weapons")
        proficiencies.add("strength")
        proficiencies.add("constitution")

        proficiency_mod = proficiency_bonus_per_level(level)

        kwargs.update({"hit_dice": hit_dice, "max_hp": max_hp, "proficiencies": proficiencies, "proficiency_mod": proficiency_mod})

        super().__init__(**kwargs)

        if self.get_level() < 3:
            self._rage_slots = 2
        elif self.get_level() < 6:
            self._rage_slots = 3
        elif self.get_level() < 12:
            self._rage_slots = 4
        elif self.get_level() < 17:
            self._rage_slots = 5
        else:
            self._rage_slots = 6

        if self.get_level() < 9:
            self._rage_damage_bonus = 2
        elif self.get_level() < 16:
            self._rage_damage_bonus = 3
        else:
            self._rage_damage_bonus = 4

        self._rage_state = False

        self.add_feature("rage")
        self.add_feature("unarmored defense")  # TODO: implement
        if self.get_level() > 1:
            self.add_feature("reckless attack")
            self._reckless_state = False
            self.add_feature("danger sense")
        if self.get_level() > 2:
            self._specialization = kwargs.get("specialization")  # TODO: implement
        if self.get_level() > 4:
            self.add_feature("extra attack")
            self.add_feature("fast movement")
            # TODO: should this be included?
            self._speed += 10  # ignore the armor thing. TODO: change this
        if self.get_level() > 6:
            self.add_feature("feral instinct")  # TODO: implement
        if self.get_level() > 8:
            self.add_feature("brutal critical")
        if self.get_level() > 10:
            self.add_feature("relentless rage")
            self._relentless_rage_dc = 10
        if self.get_level() > 14:
            self.add_feature("persistent rage")
        if self.get_level() > 17:
            self.add_feature("indomitable might")  # TODO: implement. Requires rearranging of base class.

    def copy_constructor(self, other, **kwargs):
        if not isinstance(other, Barbarian):
            raise ValueError("Cannot copy Barbarian from other class")
        kwargs.update({"level": other.get_level()})
        super().copy_constructor(other, **kwargs)
        self._rage_slots = other.get_rage_slots()
        self._rage_damage_bonus = other.get_rage_damage_bonus()
        self._rage_state = False
        if self.has_feature("reckless attack"):
            self._reckless_state = False

    def get_rage_slots(self) -> int:
        return self._rage_slots

    def get_rage_damage_bonus(self) -> int:
        return self._rage_damage_bonus

    def is_raging(self) -> bool:
        return self._rage_state

    def start_rage(self):
        if self._rage_state:
            warnings.warn("%s cannot start raging because they are already raging" % self._name)
            return
        if self.get_verbose():
            print("%s would like to RAGE!" % self._name)
        self._rage_state = True
        for attack in self._attacks:
            if attack.get_melee_range():
                weapon = attack.get_weapon()
                if not weapon or isinstance(weapon, armory.MeleeWeapon):
                    attack.set_damage_mod(attack.get_damage_mod() + self._rage_damage_bonus)
        self.add_resistance("bludgeoning")
        self.add_resistance("piercing")
        self.add_resistance("slashing")
        # TODO: If you are able to cast Spells, you can't cast them or concentrate on them while raging.

    def stop_rage(self):
        if not self._rage_state:
            warnings.warn("%s cannot stop raging because they are not raging" % self._name)
            return
        if self.get_verbose():
            print("%s has finished raging." % self._name)
        self._rage_state = False
        for attack in self._attacks:
            if attack.get_melee_range():
                attack.set_damage_mod(attack.get_damage_mod() - self._rage_damage_bonus)
        self.remove_resistance("bludgeoning")  # TODO: handle case where barbarian resistant to this damage without rage
        self.remove_resistance("piercing")
        self.remove_resistance("slashing")

    def start_reckless(self):
        if self._reckless_state:
            warnings.warn("%s cannot start recklessly attacking because they are already doing so" % self._name)
            return
        if self.get_verbose():
            print("%s attacks recklessly!" % self._name)
        self.modify_adv_to_be_hit(1)
        self._reckless_state = True

    def stop_reckless(self):
        if not self._reckless_state:
            warnings.warn("%s cannot stop recklessly attacking because they are not currently recklessly attacking" % self._name)
            return
        self.modify_adv_to_be_hit(-1)
        self._reckless_state = False

    def make_saving_throw(self, save_type: str, adv=0):
        modifier = self.get_saving_throw(save_type)
        if save_type == "strength" and self.is_raging():
            local_adv = 1
        elif self.has_feature("danger sense") and save_type == "dexterity" and not self.has_condition("blinded") \
            and not self.has_condition("deafened") and not self.has_condition("incapacitated"):
            local_adv = 1
        else:
            local_adv = 0
        adv = calc_advantage([local_adv])
        result = roll_dice(dice_type=20, modifier=modifier, adv=local_adv)[0]
        if self.get_verbose():
            print("%s rolls a %d." % (self._name, result), end=" ")
        return result

    def become_unconscious(self):
        if self.is_raging():
            if self.has_feature("relentless rage"):  # TODO: make this optional
                if self.take_saving_throw("constitution", self._relentless_rage_dc):
                    self._current_hp = 1
                    if self.get_verbose():
                        print("Fueled by rage, %s staves off unconsciousness and drops to 1 hit point instead.")
                    return
                self._relentless_rage_dc += 5
            self.stop_rage()
        super().become_unconscious()

    def send_attack(self, target, attack, adv=0):
        if attack.get_melee_range() and self.has_feature("reckless attack") and self._reckless_state:
            adv = calc_advantage([adv, 1])
        super().send_attack(target, attack, adv)

class Bard(SpellCaster, Character):
    def __init__(self, **kwargs):
        copy = kwargs.get("copy")
        if copy:
            self.copy_constructor(copy, name=kwargs.get("name"))
            return

        level = kwargs.get("level")
        if not level:
            raise ValueError("No level provided or level is 0")
        if not isinstance(level, int) or level < 1 or level > 20:
            raise ValueError("Level must be an integer between 1 and 20")
        hit_dice = (1 * level, 8)

        constitution = kwargs.get('constitution')
        if not constitution:
            constitution_mod = kwargs.get("constitution_mod")
            if constitution_mod is not None:
                if isinstance(constitution_mod, int):
                    self._constitution = constitution_mod
                else:
                    raise ValueError("Constitution mod must be an integer")
            else:
                raise ValueError("Must provide constitution score or modifier")
        else:  # pragma: no cover
            constitution_mod = ability_to_mod(constitution)

        max_hp = kwargs.get("max_hp")
        if max_hp and (not isinstance(max_hp, int) or max_hp <= 0):
            raise ValueError("Must provide positive integer max hp")
        else:
            max_hp = 8 + constitution_mod
            if level > 1:
                max_hp += (5 + constitution_mod) * (level - 1)

        proficiencies = kwargs.get('proficiencies', [])
        if isinstance(proficiencies, (tuple, list)):
            proficiencies = set(proficiencies)
        elif isinstance(proficiencies, set):  # pragma: no cover
            pass
        elif proficiencies is None:  # pragma: no cover
            proficiencies = set()
        else:
            raise ValueError("Proficiencies must be provided as a set, list, or tuple")
        proficiencies.add("simple weapons")
        proficiencies.add("hand crossbow")
        proficiencies.add("longsword")
        proficiencies.add("rapier")
        proficiencies.add("shortsword")
        proficiencies.add("dexterity")
        proficiencies.add("charisma")

        proficiency_mod = proficiency_bonus_per_level(level)

        features = kwargs.get('features', set())
        if isinstance(features, (list, tuple)):
            features = set(features)
        elif isinstance(features, set):  # pragma: no cover
            pass
        else:
            raise ValueError("Features must be a set (or a list or tuple to convert to a set)")

        if level == 1:
            spell_slots = {1: 2}
        elif level == 2:
            spell_slots = {1: 3}
        elif level == 3:
            spell_slots = {1: 4, 2: 2}
        elif level == 4:
            spell_slots = {1: 4, 2: 3}
        elif level == 5:
            spell_slots = {1: 4, 2: 3, 3: 2}
        else:
            spell_slots = {1: 4, 2: 3, 3: 3}
            features.add("countercharm")
            if level == 7:
                spell_slots.update({4: 1})
            elif level == 8:
                spell_slots.update({4: 2})
            elif level == 9:
                spell_slots.update({4: 3, 5: 1})
            elif level == 10:
                spell_slots.update({4: 3, 5: 2})
            elif level < 13:
                spell_slots.update({4: 3, 5: 2, 6: 1})
            elif level < 15:
                spell_slots.update({4: 3, 5: 2, 6: 1, 7: 1})
            elif level < 17:
                spell_slots.update({4: 3, 5: 2, 6: 1, 7: 1, 8: 1})
            elif level == 17:
                spell_slots.update({4: 3, 5: 2, 6: 1, 7: 1, 8: 1, 9: 1})
            elif level == 18:
                spell_slots.update({4: 3, 5: 3, 6: 1, 7: 1, 8: 1, 9: 1})
            elif level == 19:
                spell_slots.update({4: 3, 5: 3, 6: 2, 7: 1, 8: 1, 9: 1})
            elif level == 20:
                spell_slots.update({4: 3, 5: 3, 6: 2, 7: 2, 8: 1, 9: 1})

        kwargs.update({"hit_dice": hit_dice, "max_hp": max_hp, "proficiencies": proficiencies,
                       "proficiency_mod": proficiency_mod, "spell_ability": "charisma", "spell_slots": spell_slots,
                       "features": features})

        super().__init__(**kwargs)

        if self.get_level() < 5:
            self._inspiration_dice = (1, 6)
        elif self.get_level() < 10:
            self._inspiration_dice = (1, 8)
        elif self.get_level() < 15:
            self._inspiration_dice = (1, 10)
        else:
            self._inspiration_dice = (1, 12)

        self._inspiration_slots = self._charisma

    def copy_constructor(self, other, **kwargs):
        if not isinstance(other, Bard):
            raise ValueError("Cannot copy Bard from other class")
        super().copy_constructor(other, **kwargs)
        self._inspiration_dice = other.get_inspiration_dice()
        self._inspiration_slots = other.get_inspiration_slots()

    def get_inspiration_dice(self) -> TYPE_DICE_TUPLE:
        return self._inspiration_dice

    def get_inspiration_slots(self) -> int:
        return self._inspiration_slots

    def give_inspiration(self, target: Combatant):
        if target is self or not isinstance(target, Combatant):
            raise ValueError("Can only give inspiration to a combatant other than yourself")
        if not self._inspiration_slots:
            raise ValueError("%s cannot inspire %s because %s has no inspiration slots left" % (self._name, target.get_name(), self._name))
        self._inspiration_slots -= 1
        # TODO: finish this method

class Cleric(SpellCaster, Character):
    def __init__(self, **kwargs):
        copy = kwargs.get("copy")
        if copy:
            self.copy_constructor(copy, name=kwargs.get("name"))
            return

        level = kwargs.get("level")
        if not level:
            raise ValueError("No level provided or level is 0")
        if not isinstance(level, int) or level < 1 or level > 20:
            raise ValueError("Level must be an integer between 1 and 20")
        hit_dice = (1 * level, 8)

        constitution = kwargs.get('constitution')
        if not constitution:
            constitution_mod = kwargs.get("constitution_mod")
            if constitution_mod is not None:
                if isinstance(constitution_mod, int):
                    self._constitution = constitution_mod
                else:
                    raise ValueError("Constitution mod must be an integer")
            else:
                raise ValueError("Must provide constitution score or modifier")
        else:  # pragma: no cover
            constitution_mod = ability_to_mod(constitution)

        max_hp = kwargs.get("max_hp")
        if max_hp and (not isinstance(max_hp, int) or max_hp <= 0):
            raise ValueError("Must provide positive integer max hp")
        else:
            max_hp = 8 + constitution_mod
            if level > 1:
                max_hp += (5 + constitution_mod) * (level - 1)

        proficiencies = kwargs.get('proficiencies', [])
        if isinstance(proficiencies, (tuple, list)):
            proficiencies = set(proficiencies)
        elif isinstance(proficiencies, set):  # pragma: no cover
            pass
        elif proficiencies is None:  # pragma: no cover
            proficiencies = set()
        else:
            raise ValueError("Proficiencies must be provided as a set, list, or tuple")
        proficiencies.add("simple weapons")
        proficiencies.add("wisdom")
        proficiencies.add("charisma")

        proficiency_mod = proficiency_bonus_per_level(level)

        if level == 1:
            spell_slots = {1: 2}
        elif level == 2:
            spell_slots = {1: 3}
        elif level == 3:
            spell_slots = {1: 4, 2: 2}
        elif level == 4:
            spell_slots = {1: 4, 2: 3}
        elif level == 5:
            spell_slots = {1: 4, 2: 3, 3: 2}
        elif level == 6:
            spell_slots = {1: 4, 2: 3, 3: 3}
        elif level == 7:
            spell_slots = {1: 4, 2: 3, 3: 3, 4: 2}
        elif level == 8:
            spell_slots = {1: 4, 2: 3, 3: 3, 4: 2}
        elif level == 9:
            spell_slots = {1: 4, 2: 3, 3: 3, 4: 3, 5: 1}
        elif level == 10:
            spell_slots = {1: 4, 2: 3, 3: 3, 4: 3, 5: 2}
        elif level < 13:
            spell_slots = {1: 4, 2: 3, 3: 3, 4: 3, 5: 2, 6: 1}
        elif level < 15:
            spell_slots = {1: 4, 2: 3, 3: 3, 4: 3, 5: 2, 6: 1, 7: 1}
        elif level < 17:
            spell_slots = {1: 4, 2: 3, 3: 3, 4: 3, 5: 2, 6: 1, 7: 1, 8: 1}
        elif level == 17:
            spell_slots = {1: 4, 2: 3, 3: 3, 4: 3, 5: 2, 6: 1, 7: 1, 8: 1, 9: 1}
        elif level == 18:
            spell_slots = {1: 4, 2: 3, 3: 3, 4: 3, 5: 3, 6: 1, 7: 1, 8: 1, 9: 1}
        elif level == 19:
            spell_slots = {1: 4, 2: 3, 3: 3, 4: 3, 5: 3, 6: 2, 7: 1, 8: 1, 9: 1}
        else:
            spell_slots = {1: 4, 2: 3, 3: 3, 4: 3, 5: 3, 6: 2, 7: 2, 8: 1, 9: 1}

        kwargs.update({"hit_dice": hit_dice, "max_hp": max_hp, "proficiencies": proficiencies,
                       "proficiency_mod": proficiency_mod, "spell_ability": "wisdom", "spell_slots": spell_slots})

        super().__init__(**kwargs)

        if self.get_level() > 1:
            self.add_feature("channel divinity")
            if self.get_level() < 6:
                self._channel_divinity_slots = 1
            elif self.get_level() < 18:
                self._channel_divinity_slots = 2
            else:
                self._channel_divinity_slots = 3
            if self.get_level() > 4:
                self.add_feature("destroy undead")
                if self.get_level() < 8:
                    self._destroy_undead_cr = 1/2
                elif self.get_level() < 11:
                    self._destroy_undead_cr = 1
                elif self.get_level() < 14:
                    self._destroy_undead_cr = 2
                elif self.get_level() < 17:
                    self._destroy_undead_cr = 3
                else:
                    self._destroy_undead_cr = 4

    def copy_constructor(self, other, **kwargs):
        if not isinstance(other, Cleric):
            raise ValueError("Cannot copy Cleric from other class")
        super().copy_constructor(other, **kwargs)
        if self.has_feature("channel divinity"):
            self._channel_divinity_slots = other.get_channel_divinity_slots()
            if self.has_feature("destroy undead"):
                self._destroy_undead_cr = other.get_destroy_undead_cr()

    def get_channel_divinity_slots(self) -> int:
        try:
            return self._channel_divinity_slots
        except AttributeError:
            return 0

    def channel_divinity(self, use_type="turn undead"):  # pylint: disable=unused-argument
        if not self.has_feature("channel divinity"):
            raise ValueError("%s does not yet know how to channel divinity" % self._name)
        if not self._channel_divinity_slots:
            raise ValueError("%s has no more charges of channel divinity left" % self._name)
        self._channel_divinity_slots -= 1

    def get_destroy_undead_cr(self) -> int:
        try:
            return self._destroy_undead_cr
        except AttributeError:
            raise ValueError("%s cannot yet destroy undead" % self._name)

class Druid(SpellCaster, Character):
    def __init__(self, **kwargs):
        copy = kwargs.get("copy")
        if copy:
            self.copy_constructor(copy, name=kwargs.get("name"))
            return

        level = kwargs.get("level")
        if not level:
            raise ValueError("No level provided or level is 0")
        if not isinstance(level, int) or level < 1 or level > 20:
            raise ValueError("Level must be an integer between 1 and 20")
        hit_dice = (1 * level, 8)

        constitution = kwargs.get('constitution')
        if not constitution:
            constitution_mod = kwargs.get("constitution_mod")
            if constitution_mod is not None:
                if isinstance(constitution_mod, int):
                    self._constitution = constitution_mod
                else:
                    raise ValueError("Constitution mod must be an integer")
            else:
                raise ValueError("Must provide constitution score or modifier")
        else:  # pragma: no cover
            constitution_mod = ability_to_mod(constitution)

        max_hp = kwargs.get("max_hp")
        if max_hp and (not isinstance(max_hp, int) or max_hp <= 0):
            raise ValueError("Must provide positive integer max hp")
        else:
            max_hp = 8 + constitution_mod
            if level > 1:
                max_hp += (5 + constitution_mod) * (level - 1)

        proficiencies = kwargs.get('proficiencies', [])
        if isinstance(proficiencies, (tuple, list)):
            proficiencies = set(proficiencies)
        elif isinstance(proficiencies, set):
            pass
        elif proficiencies is None:  # pragma: no cover
            proficiencies = set()
        else:  # pragma: no cover
            raise ValueError("Proficiencies must be provided as a set, list, or tuple")
        proficiencies.add("club")
        proficiencies.add("dagger")
        proficiencies.add("javelin")
        proficiencies.add("mace")
        proficiencies.add("quarterstaff")
        proficiencies.add("scimitar")
        proficiencies.add("sickle")
        proficiencies.add("sling")
        proficiencies.add("spear")
        proficiencies.add("intelligence")
        proficiencies.add("wisdom")

        proficiency_mod = proficiency_bonus_per_level(level)

        if level == 1:
            spell_slots = {1: 2}
        elif level == 2:
            spell_slots = {1: 3}
        elif level == 3:
            spell_slots = {1: 4, 2: 2}
        elif level == 4:
            spell_slots = {1: 4, 2: 3}
        elif level == 5:
            spell_slots = {1: 4, 2: 3, 3: 2}
        elif level == 6:
            spell_slots = {1: 4, 2: 3, 3: 3}
        elif level == 7:
            spell_slots = {1: 4, 2: 3, 3: 3, 4: 1}
        elif level == 8:
            spell_slots = {1: 4, 2: 3, 3: 3, 4: 2}
        elif level == 9:
            spell_slots = {1: 4, 2: 3, 3: 3, 4: 3, 5: 1}
        elif level == 10:
            spell_slots = {1: 4, 2: 3, 3: 3, 4: 3, 5: 2}
        elif level < 13:
            spell_slots = {1: 4, 2: 3, 3: 3, 4: 3, 5: 2, 6: 1}
        elif level < 15:
            spell_slots = {1: 4, 2: 3, 3: 3, 4: 3, 5: 2, 6: 1, 7: 1}
        elif level < 17:
            spell_slots = {1: 4, 2: 3, 3: 3, 4: 3, 5: 2, 6: 1, 7: 1, 8: 1}
        elif level == 17:
            spell_slots = {1: 4, 2: 3, 3: 3, 4: 3, 5: 2, 6: 1, 7: 1, 8: 1, 9: 1}
        elif level == 18:
            spell_slots = {1: 4, 2: 3, 3: 3, 4: 3, 5: 3, 6: 1, 7: 1, 8: 1, 9: 1}
        elif level == 19:
            spell_slots = {1: 4, 2: 3, 3: 3, 4: 3, 5: 3, 6: 2, 7: 1, 8: 1, 9: 1}
        else:
            spell_slots = {1: 4, 2: 3, 3: 3, 4: 3, 5: 3, 6: 2, 7: 2, 8: 1, 9: 1}

        kwargs.update({"hit_dice": hit_dice, "max_hp": max_hp, "proficiencies": proficiencies,
                       "proficiency_mod": proficiency_mod, "spell_slots": spell_slots, "spell_ability": "wisdom"})

        super().__init__(**kwargs)

        if self.get_level() > 1:
            self._wild_shape_slots = 2
            wild_shapes = kwargs.get("wild_shapes")
            self._wild_shapes = set()
            if isinstance(wild_shapes, (list, tuple, set)):
                for beast in wild_shapes:
                    if isinstance(beast, Creature):
                        self.add_wild_shape(beast)
                    else:
                        raise ValueError("Wild shapes must be Creatures")
            elif wild_shapes is None:
                warnings.warn("Created a druid of level 2 or above without any wild shapes")
                self._wild_shapes = set()
            else:
                raise ValueError("Wild shapes must be a list, tuple, or set of Creatures")
            self._current_shape = None

    def copy_constructor(self, other, **kwargs):
        if not isinstance(other, Druid):
            raise ValueError("Cannot copy Druid from other class")
        super().copy_constructor(other, **kwargs)
        if self.get_level() > 1:
            self._wild_shape_slots = other.get_wild_shape_slots()
            self._wild_shapes = set()
            for beast in other.get_wild_shapes():
                self.add_wild_shape(type(beast)(copy=beast))
            self._current_shape = None

    def get_wild_shape_slots(self) -> int:
        try:
            return self._wild_shape_slots
        except AttributeError:
            return 0

    def get_wild_shapes(self) -> set:
        try:
            return self._wild_shapes
        except AttributeError:
            return set()

    def get_current_shape(self) -> Optional[Creature]:
        try:
            return self._current_shape
        except AttributeError:
            return None

    def get_ac(self) -> int:
        if self.get_current_shape():
            return self.get_current_shape().get_ac()
        return super().get_ac()

    def get_hit_dice(self) -> TYPE_DICE_TUPLE:
        if self.get_current_shape():
            return self.get_current_shape().get_hit_dice()
        return super().get_hit_dice()

    def get_max_hp(self) -> int:
        if self.get_current_shape():
            return self.get_current_shape().get_max_hp()
        return super().get_max_hp()

    def get_temp_hp(self) -> int:
        if self.get_current_shape():
            return 0  # beasts don't have temp hp. Right?
        return super().get_temp_hp()

    def get_current_hp(self) -> int:
        if self.get_current_shape():
            return self.get_current_shape().get_current_hp()
        return super().get_current_hp()

    def is_bloodied(self) -> bool:
        if self.get_current_shape():
            return self.get_current_shape().is_bloodied()
        return super().is_bloodied()

    def is_hp_max(self) -> bool:
        if self.get_current_shape():
            return self.get_current_shape().is_hp_max()
        return super().is_hp_max()

    def get_speed(self) -> int:
        if self.get_current_shape():
            return self.get_current_shape().get_speed()
        return super().get_speed()

    def get_vision(self) -> str:
        if self.get_current_shape():
            return self.get_current_shape().get_vision()
        return super().get_vision()

    def get_ability(self, ability: str) -> int:
        if self.get_current_shape():
            return self.get_current_shape().get_ability(ability)
        return super().get_ability(ability)

    def get_strength(self) -> int:
        if self.get_current_shape():
            return self.get_current_shape().get_strength()
        return super().get_strength()

    def get_dexterity(self) -> int:
        if self.get_current_shape():
            return self.get_current_shape().get_dexterity()
        return super().get_dexterity()

    def get_constitution(self) -> int:
        if self.get_current_shape():
            return self.get_current_shape().get_constitution()
        return super().get_constitution()

    def get_intelligence(self) -> int:
        if self.get_current_shape():
            return self.get_current_shape().get_intelligence()
        return super().get_intelligence()

    def get_wisdom(self) -> int:
        if self.get_current_shape():
            return self.get_current_shape().get_wisdom()
        return super().get_wisdom()

    def get_charisma(self) -> int:
        if self.get_current_shape():
            return self.get_current_shape().get_charisma()
        return super().get_charisma()

    def get_proficiencies(self) -> set:
        if self.get_current_shape():
            return self.get_current_shape().get_proficiencies()
        return super().get_proficiencies()

    def get_vulnerabilities(self) -> set:
        if self.get_current_shape():
            return self.get_current_shape().get_vulnerabilities()
        return super().get_vulnerabilities()

    def get_resistances(self) -> set:
        if self.get_current_shape():
            return self.get_current_shape().get_resistances()
        return super().get_resistances()

    def get_immunities(self) -> set:
        if self.get_current_shape():
            return self.get_current_shape().get_immunities()
        return super().get_immunities()

    def get_saving_throw(self, ability: str) -> int:
        if self.get_current_shape():
            return self.get_current_shape().get_saving_throw(ability)
        return super().get_saving_throw(ability)

    def get_attacks(self) -> list:
        if self.get_current_shape():
            return self.get_current_shape().get_attacks()
        return super().get_attacks()

    def add_wild_shape(self, beast: Creature):
        if not isinstance(beast, Creature):
            raise ValueError("Wild shape must be a Creature")
        # pylint: disable=protected-access
        beast._intelligence = self._intelligence
        beast._wisdom = self._wisdom
        beast._charisma = self._charisma
        for proficiency in self._proficiencies:
            beast.get_proficiencies().add(proficiency)
        # rebuild saving throws
        beast._saving_throws = {"strength": beast._strength, "dexterity": beast._dexterity,
                               "constitution": beast._constitution, "intelligence": beast._intelligence,
                               "wisdom": beast._wisdom, "charisma": beast._charisma}
        for ability in beast._saving_throws:
            if ability in beast._proficiencies:
                beast._saving_throws[ability] += self._proficiency_mod
        # pylint: enable=protected-access
        self._wild_shapes.add(beast)

    def send_attack(self, target, attack, adv=0) -> Optional[int]:
        if self.get_current_shape() and isinstance(attack, Spell):
            raise ValueError("%s cannot cast spells while in animal form!" % self._name)
        return super().send_attack(target, attack, adv)

    def take_damage(self, damage, damage_type=None):
        if self.get_current_shape():
            beast = self.get_current_shape()
            if beast.is_vulnerable(damage_type):
                if self.get_verbose():
                    print("%s is vulnerable to %s!" % (self._name, damage_type))
                damage *= 2
            elif beast.is_resistant(damage_type):
                if self.get_verbose():
                    print("%s is resistant to %s." % (self._name, damage_type))
                damage //= 2
            elif beast.is_immune(damage_type):
                if self.get_verbose():
                    print("%s is immune to %s." % (self._name, damage_type))
                    return
            if self.get_verbose():
                print("%s takes %d damage" % (self._name, damage))
            # skip temp hp
            if damage <= beast.get_current_hp():
                beast._current_hp -= damage
            else:
                damage -= beast.get_current_hp()
                self.end_shape()
                super().take_damage(damage)
        else:
            super().take_damage(damage, damage_type)

    def start_shape(self, beast: Creature):
        if not isinstance(beast, Creature):
            raise ValueError("%s cannot shift into something that is not a creature" % self._name)
        if not self.get_wild_shape_slots():
            raise ValueError("%s does not have any wild shape slots left" % self._name)
        self._wild_shape_slots -= 1
        if beast not in self.get_wild_shapes():
            warnings.warn("%s is shifting into a Creature not assigned to them. Stats may not be set correctly" % self._name)
        if self.get_verbose():
            print("%s shifts into %s" % (self._name, str(type(beast))))
        self._current_shape = beast

    def end_shape(self):
        if self.get_verbose():
            print("%s shifts out of %s into their original form." % (self._name, str(type(self.get_current_shape()))))
        self._current_shape = None

class Fighter(Character):
    def __init__(self, **kwargs):
        copy = kwargs.get("copy")
        if copy:
            self.copy_constructor(copy, name=kwargs.get("name"))
            return

        level = kwargs.get("level")
        if not level:
            raise ValueError("No level provided or level is 0")
        if not isinstance(level, int) or level < 1 or level > 20:
            raise ValueError("Level must be an integer between 1 and 20")
        hit_dice = (1 * level, 10)

        constitution = kwargs.get('constitution')
        if not constitution:
            constitution_mod = kwargs.get("constitution_mod")
            if constitution_mod is not None:
                if isinstance(constitution_mod, int):
                    self._constitution = constitution_mod
                else:
                    raise ValueError("Constitution mod must be an integer")
            else:
                raise ValueError("Must provide constitution score or modifier")
        else:  # pragma: no cover
            constitution_mod = ability_to_mod(constitution)

        max_hp = kwargs.get("max_hp")
        if max_hp and (not isinstance(max_hp, int) or max_hp <= 0):
            raise ValueError("Must provide positive integer max hp")
        else:
            max_hp = 10 + constitution_mod
            if level > 1:
                max_hp += (6 + constitution_mod) * (level - 1)

        proficiencies = kwargs.get('proficiencies', [])
        if isinstance(proficiencies, (tuple, list)):
            proficiencies = set(proficiencies)
        elif isinstance(proficiencies, set):
            pass
        elif proficiencies is None:  # pragma: no cover
            proficiencies = set()
        else:  # pragma: no cover
            raise ValueError("Proficiencies must be provided as a set, list, or tuple")
        proficiencies.add("simple weapons")
        proficiencies.add("martial weapons")
        proficiencies.add("strength")
        proficiencies.add("constitution")

        proficiency_mod = proficiency_bonus_per_level(level)

        kwargs.update({"max_hp": max_hp, "proficiencies": proficiencies, "proficiency_mod": proficiency_mod, "hit_dice": hit_dice})

        super().__init__(**kwargs)

        self.add_feature("fighting style")
        fighting_style = kwargs.get("fighting_style")
        self.add_fighting_style(fighting_style)

        self.add_feature("second wind")  # TODO: implement
        self._second_wind_slots = 1
        if self.get_level() > 1:
            self.add_feature("action surge")
            self._action_surge_slots = 1
        if self.get_level() > 2:
            self.add_feature("martial archetype")
        if self.get_level() > 4:
            self.add_feature("extra attack")
            self._extra_attack_num = 1
        if self.get_level() > 8:
            self.add_feature("indomitable")
            self._indomitable_slots = 1
        if self.get_level() > 10:
            self._extra_attack_num = 2
        if self.get_level() > 12:
            self._indomitable_slots = 2
        if self.get_level() > 16:
            self._action_surge_slots = 2
            self._indomitable_slots = 3
        if self.get_level() > 19:
            self._extra_attack_num = 3

    def copy_constructor(self, other, **kwargs):
        if not isinstance(other, Fighter):
            raise ValueError("Cannot copy Fighter from other class")
        kwargs.update({"level": other.get_level()})
        super().copy_constructor(other, **kwargs)
        self._second_wind_slots = 1
        self._fighting_styles = other.get_fighting_styles().copy()
        if self.has_feature("action surge"):
            self._action_surge_slots = other.get_action_surge_slots()
        if self.has_feature("extra attack"):
            self._extra_attack_num = other.get_extra_attack_num()
        if self.has_feature("indomitable"):
            self._indomitable_slots = other.get_indomitable_slots()

    def get_second_wind_slots(self) -> int:
        return self._second_wind_slots

    def get_action_surge_slots(self) -> int:
        try:
            return self._action_surge_slots
        except AttributeError:
            return 0

    def get_extra_attack_num(self) -> int:
        try:
            return self._extra_attack_num
        except AttributeError:
            return 0

    def get_indomitable_slots(self) -> int:
        try:
            return self._indomitable_slots
        except AttributeError:
            return 0

    def take_action_surge(self):
        # TODO: complete
        if self.get_action_surge_slots():
            self._action_surge_slots -= 1
        else:
            raise ValueError("%s tried to action surge even though they don't have the slots for it" % self._name)

    def take_extra_attack(self):
        # TODO: complete
        if self.get_extra_attack_num():
            self._extra_attack_num -= 1
        else:
            raise ValueError("%s tried to take an extra attack even though they don't have the slots for it" % self._name)

    def take_indomitable(self):
        # TODO: complete
        if self.get_indomitable_slots():
            self._indomitable_slots -= 1
        else:
            raise ValueError("%s tried to use indomitable feature even though they don't have the slots for it" % self._name)

    def take_second_wind(self):
        # bonus action
        if self.get_second_wind_slots():
            if self.get_verbose():
                print("%s uses second wind!" % self._name, end=" ")
            healing = roll_dice(10, modifier=self.get_level())[0]
            self.take_healing(healing)
            self._second_wind_slots -= 1
        else:
            raise ValueError("%s tried to take second wind even though they don't have the slots for it" % self._name)

class Monk(Character):
    def __init__(self, **kwargs):
        copy = kwargs.get("copy")
        if copy:
            self.copy_constructor(copy, name=kwargs.get("name"))
            return

        level = kwargs.get("level")
        if not level:
            raise ValueError("No level provided or level is 0")
        if not isinstance(level, int) or level < 1 or level > 20:
            raise ValueError("Level must be an integer between 1 and 20")
        hit_dice = (1 * level, 8)

        constitution = kwargs.get('constitution')
        if not constitution:
            constitution_mod = kwargs.get("constitution_mod")
            if constitution_mod is not None:
                if isinstance(constitution_mod, int):
                    self._constitution = constitution_mod
                else:
                    raise ValueError("Constitution mod must be an integer")
            else:
                raise ValueError("Must provide constitution score or modifier")
        else:  # pragma: no cover
            constitution_mod = ability_to_mod(constitution)

        max_hp = kwargs.get("max_hp")
        if max_hp and (not isinstance(max_hp, int) or max_hp <= 0):
            raise ValueError("Must provide positive integer max hp")
        else:
            max_hp = 8 + constitution_mod
            if level > 1:
                max_hp += (5 + constitution_mod) * (level - 1)

        proficiencies = kwargs.get('proficiencies', [])
        if isinstance(proficiencies, (tuple, list)):
            proficiencies = set(proficiencies)
        elif isinstance(proficiencies, set):
            pass
        elif proficiencies is None:  # pragma: no cover
            proficiencies = set()
        else:  # pragma: no cover
            raise ValueError("Proficiencies must be provided as a set, list, or tuple")
        proficiencies.add("monk weapons")
        proficiencies.add("strength")
        proficiencies.add("dexterity")

        proficiency_mod = proficiency_bonus_per_level(level)

        kwargs.update({"hit_dice": hit_dice, "max_hp": max_hp, "proficiencies": proficiencies, "proficiency_mod": proficiency_mod})

        super().__init__(**kwargs)

        if self.get_level() < 5:
            self._martial_arts_dice = (1, 4)
        elif self.get_level() < 11:
            self._martial_arts_dice = (1, 6)
        elif self.get_level() < 17:
            self._martial_arts_dice = (1, 8)
        else:
            self._martial_arts_dice = (1, 10)

        self.add_feature("unarmored defense")
        if self.get_level() > 1:
            self._ki_points = self.get_level()
            self._ki_save_dc = 8 + self.get_proficiency_mod() + self.get_wisdom()
            self.add_feature("flurry of blows")
            self.add_feature("patient defense")
            self.add_feature("step of the wind")
            if self.get_level() > 2:
                self.add_feature("deflect missiles")
            if self.get_level() > 3:
                self.add_feature("slow fall")
            if self.get_level() > 4:
                self.add_feature("extra attack")
                self.add_feature("stunning strike")
            if self.get_level() > 5:
                self.add_feature("ki-empowered strikes")
            if self.get_level() > 6:
                self.add_feature("stillness of mind")
                self.add_feature("evasion")
            if self.get_level() > 9:
                self.add_feature("purity of body")
            if self.get_level() > 12:
                self.add_feature("tongue of the sun and moon")
            if self.get_level() > 13:
                self.add_feature("diamond soul")
                # proficiency in all Saving Throws
                self._proficiencies.add("constitution")
                self._proficiencies.add("intelligence")
                self._proficiencies.add("wisdom")
                self._proficiencies.add("charisma")
                self._saving_throws = {"strength": self.get_strength() + self.get_proficiency_mod(),
                                       "dexterity": self.get_dexterity() + self.get_proficiency_mod(),
                                       "constitution": self.get_constitution() + self.get_proficiency_mod(),
                                       "intelligence": self.get_intelligence() + self.get_proficiency_mod(),
                                        "wisdom": self.get_wisdom() + self.get_proficiency_mod(),
                                       "charisma": self.get_charisma() + self.get_proficiency_mod()}
                # TODO: whenever you make a saving throw and fail, you can spend 1 ki point to reroll it and take the second result.
            if self.get_level() > 17:
                self.add_feature("empty body")
            if self.get_level() == 20:
                self.add_feature("perfect soul")

    def copy_constructor(self, other, **kwargs):
        if not isinstance(other, Monk):
            raise ValueError("Cannot copy Monk from other class")
        kwargs.update({"level": other.get_level()})
        super().copy_constructor(other, **kwargs)
        self._martial_arts_dice = other.get_martial_arts_dice()
        if self.get_level() > 1:
            self._ki_points = other.get_ki_points()
            self._ki_save_dc = other.get_ki_save_dc()
            if self.get_level() > 13:
                self._saving_throws = {"strength": self.get_strength() + self.get_proficiency_mod(),
                                       "dexterity": self.get_dexterity() + self.get_proficiency_mod(),
                                       "constitution": self.get_constitution() + self.get_proficiency_mod(),
                                       "intelligence": self.get_intelligence() + self.get_proficiency_mod(),
                                       "wisdom": self.get_wisdom() + self.get_proficiency_mod(),
                                       "charisma": self.get_charisma() + self.get_proficiency_mod()}

    def get_martial_arts_dice(self) -> TYPE_DICE_TUPLE:
        return self._martial_arts_dice

    def get_ki_points(self) -> int:
        try:
            return self._ki_points
        except AttributeError:
            return 0

    def get_ki_save_dc(self) -> int:
        try:
            return self._ki_save_dc
        except AttributeError:
            raise ValueError("Cannot use a ki saving throw attack")  # TODO: what is appropriate response to this situation?

    def spend_ki_point(self, num: int):
        if num > self.get_ki_points():
            raise ValueError("%s doesn't have enough ki points left to do this" % self._name)
        elif num < 1:
            raise ValueError("%s cannot spend less than one ki point" % self._name)
        self._ki_points -= num

    def add_attack(self, attack):
        if armory.is_monk_weapon(attack.get_weapon()) and self.get_dexterity() > self.get_strength():
            # change from str to dex
            attack.set_attack_mod(attack.get_attack_mod() - self.get_strength() + self.get_dexterity())
            attack.set_damage_mod(attack.get_damage_mod() - self.get_strength() + self.get_dexterity())
        super().add_attack(attack)

class Paladin(SpellCaster, Character):
    def __init__(self, **kwargs):
        copy = kwargs.get("copy")
        if copy:
            self.copy_constructor(copy, name=kwargs.get("name"))
            return

        level = kwargs.get("level")
        if not level:
            raise ValueError("No level provided or level is 0")
        if not isinstance(level, int) or level < 1 or level > 20:
            raise ValueError("Level must be an integer between 1 and 20")
        hit_dice = (1 * level, 10)

        constitution = kwargs.get('constitution')
        if not constitution:
            constitution_mod = kwargs.get("constitution_mod")
            if constitution_mod is not None:
                if isinstance(constitution_mod, int):
                    self._constitution = constitution_mod
                else:
                    raise ValueError("Constitution mod must be an integer")
            else:
                raise ValueError("Must provide constitution score or modifier")
        else:  # pragma: no cover
            constitution_mod = ability_to_mod(constitution)

        max_hp = kwargs.get("max_hp")
        if max_hp and (not isinstance(max_hp, int) or max_hp <= 0):
            raise ValueError("Must provide positive integer max hp")
        else:
            max_hp = 10 + constitution_mod
            if level > 1:
                max_hp += (6 + constitution_mod) * (level - 1)

        proficiencies = kwargs.get('proficiencies', [])
        if isinstance(proficiencies, (tuple, list)):
            proficiencies = set(proficiencies)
        elif isinstance(proficiencies, set):
            pass
        elif proficiencies is None:  # pragma: no cover
            proficiencies = set()
        else:  # pragma: no cover
            raise ValueError("Proficiencies must be provided as a set, list, or tuple")
        proficiencies.add("simple weapons")
        proficiencies.add("martial weapons")
        proficiencies.add("wisdom")
        proficiencies.add("charisma")

        proficiency_mod = proficiency_bonus_per_level(level)

        spell_slots = {1: 0}
        if level > 1:
            spell_slots = {1: 2}
            if level > 2:
                spell_slots.update({1: 3})
            if level > 4:
                spell_slots.update({1: 4, 2: 2})
            if level > 6:
                spell_slots.update({2: 3})
            if level > 8:
                spell_slots.update({3: 2})
            if level > 9:
                spell_slots.update({3: 3})
            if level > 12:
                spell_slots.update({4: 1})
            if level > 14:
                spell_slots.update({4: 2})
            if level > 16:
                spell_slots.update({4: 3, 5: 1})
            if level > 18:
                spell_slots.update({5: 2})

        kwargs.update({"max_hp": max_hp, "proficiencies": proficiencies, "proficiency_mod": proficiency_mod,
                       "hit_dice": hit_dice, "spell_ability": "charisma", "spell_slots": spell_slots})
        # include empty spell slots so that SpellCaster init doesn't throw a fit

        super().__init__(**kwargs)

        self.add_feature("divine sense")
        self._divine_sense_slots = 1 + self.get_charisma()
        self.add_feature("lay on hands")
        self._lay_on_hands_pool = self.get_level() * 5

        if self.get_level() > 1:
            self.add_feature("fighting style")
            fighting_style = kwargs.get("fighting_style")
            if fighting_style not in ["archery", "defense", "dueling", "great weapon fighting", "protection",
                                      "two-weapon fighting"]:
                raise ValueError(
                    'Fighting style must be in "archery", "defense", "dueling", "great weapon fighting", "protection", "two-weapon fighting"')
            if self.has_fighting_style(fighting_style):
                raise ValueError("%s cannot add the same fighting style (%s) twice" % (self._name, fighting_style))
            self.get_fighting_styles().add(fighting_style)

            self.add_feature("divine smite")

            if level > 2:
                self.add_feature("divine health")
                self.add_feature("sacred oath")
            if level > 4:
                self.add_feature("extra attack")
            if level > 5:
                self.add_feature("aura of protection")
                self._aura = 10
            if level > 9:
                self.add_feature("aura of courage")
            if level > 10:
                self.add_feature("improved divine smite")
            if level > 13:
                self.add_feature("cleansing touch")
                self._cleansing_touch_slots = max(self.get_charisma(), 1)
            if level > 17:
                self._aura = 30

    def copy_constructor(self, other, **kwargs):
        if not isinstance(other, Paladin):
            raise ValueError("Cannot copy Paladin from other class")
        super().copy_constructor(other, **kwargs)
        self._divine_sense_slots = other.get_divine_sense_slots()
        self._lay_on_hands_pool = other.get_lay_on_hands_pool()
        self._fighting_styles = other.get_fighting_styles().copy()
        if other.get_level() > 5:
            self._aura = other.get_aura()
        if other.has_feature("cleansing touch"):
            self._cleansing_touch_slots = other.get_cleansing_touch_slots()

    def get_divine_sense_slots(self) -> int:
        return self._divine_sense_slots

    def get_lay_on_hands_pool(self) -> int:
        return self._lay_on_hands_pool

    def get_aura(self) -> int:
        try:
            return self._aura
        except AttributeError:
            return 0

    def get_cleansing_touch_slots(self) -> int:
        try:
            return self._cleansing_touch_slots
        except AttributeError:
            return 0

    def spend_divine_sense_slot(self):
        if self.get_divine_sense_slots():
            self._divine_sense_slots -= 1
        else:
            raise ValueError("%s has no slots left to cast divine sense" % self._name)

    def spend_cleansing_touch_slot(self):
        if not self.get_cleansing_touch_slots():
            raise ValueError("%s does not have any cleansing touch slots" % self._name)
        self._cleansing_touch_slots -= 1

    def send_lay_on_hands(self, hp: int, target=None, use="healing"):  # pylint: disable=inconsistent-return-statements
        # TODO: This feature has no effect on Undead and constructs.
        if self.get_lay_on_hands_pool() >= hp:
            self._lay_on_hands_pool -= hp
            if use == "healing" and isinstance(target, Combatant):
                return target.take_healing(hp)
        else:
            raise ValueError("%s doesn't have enough lay on hands points left for that" % self._name)

    def send_divine_smite(self, target: Combatant, level=1) -> int:
        if not self.has_feature("divine smite"):
            raise ValueError("%s does not have the divine smite feature" % self._name)
        self.spend_spell_slot(level)
        dice_num = max(2 + level-1, 5)
        # TODO: The damage increases by 1d8 if the target is an Undead or a fiend.
        dice_type = 8
        damage = roll_dice(dice_type, num=dice_num)
        return target.take_damage(damage, damage_type="radiant")

class Ranger(SpellCaster, Character):
    def __init__(self, **kwargs):
        copy = kwargs.get("copy")
        if copy:
            self.copy_constructor(copy, name=kwargs.get("name"))
            return

        level = kwargs.get("level")
        if not level:
            raise ValueError("No level provided or level is 0")
        if not isinstance(level, int) or level < 1 or level > 20:
            raise ValueError("Level must be an integer between 1 and 20")
        hit_dice = (1 * level, 10)

        constitution = kwargs.get('constitution')
        if not constitution:
            constitution_mod = kwargs.get("constitution_mod")
            if constitution_mod is not None:
                if isinstance(constitution_mod, int):
                    self._constitution = constitution_mod
                else:
                    raise ValueError("Constitution mod must be an integer")
            else:
                raise ValueError("Must provide constitution score or modifier")
        else:  # pragma: no cover
            constitution_mod = ability_to_mod(constitution)

        max_hp = kwargs.get("max_hp")
        if max_hp and (not isinstance(max_hp, int) or max_hp <= 0):
            raise ValueError("Must provide positive integer max hp")
        else:
            max_hp = 10 + constitution_mod
            if level > 1:
                max_hp += (6 + constitution_mod) * (level - 1)

        proficiencies = kwargs.get('proficiencies', [])
        if isinstance(proficiencies, (tuple, list)):
            proficiencies = set(proficiencies)
        elif isinstance(proficiencies, set):
            pass
        elif proficiencies is None:  # pragma: no cover
            proficiencies = set()
        else:  # pragma: no cover
            raise ValueError("Proficiencies must be provided as a set, list, or tuple")
        proficiencies.add("simple weapons")
        proficiencies.add("martial weapons")
        proficiencies.add("strength")
        proficiencies.add("dexterity")

        proficiency_mod = proficiency_bonus_per_level(level)

        spell_slots = {1: 0}
        if level > 1:
            spell_slots.update({1: 2})
            if level > 2:
                spell_slots.update({1: 3})
            if level > 4:
                spell_slots.update({1: 4, 2: 2})
            if level > 6:
                spell_slots.update({2: 3})
            if level > 8:
                spell_slots.update({3: 2})
            if level > 10:
                spell_slots.update({3: 3})
            if level > 12:
                spell_slots.update({4: 1})
            if level > 14:
                spell_slots.update({4: 2})
            if level > 16:
                spell_slots.update({4: 3, 5: 1})
            if level > 19:
                spell_slots.update({5: 2})

        kwargs.update({"max_hp": max_hp, "proficiencies": proficiencies, "proficiency_mod": proficiency_mod,
                       "hit_dice": hit_dice, "spell_ability": "wisdom", "spell_slots": spell_slots})

        super().__init__(**kwargs)

        self.add_feature("favored enemy")  # TODO: validate favored enemies
        self._favored_enemies = kwargs.get("favored_enemies")
        if isinstance(self._favored_enemies, (list, tuple)):
            self._favored_enemies = set(self._favored_enemies)
        elif isinstance(self._favored_enemies, set):
            pass
        else:
            self._favored_enemies = set()
            favored_enemy = kwargs.get("favored_enemy")
            self._favored_enemies.add(favored_enemy)

        self.add_feature("favored terrain")  # TODO: validate favored terrains
        self._favored_terrains = kwargs.get("favored_terrains")
        if isinstance(self._favored_terrains, (list, tuple)):
            self._favored_terrains = set(self._favored_terrains)
        elif isinstance(self._favored_terrains, set):
            pass
        else:
            self._favored_terrains = set()
            favored_terrain = kwargs.get("favored_terrain")
            self._favored_terrains.add(favored_terrain)

        if self.get_level() > 1:
            self.add_feature("fighting style")
            fighting_style = kwargs.get("fighting_style")
            self.add_fighting_style(fighting_style)
            if self.get_level() > 2:
                self.add_feature("ranger archetype")
                self.add_feature("primeval awareness")
            if self.get_level() > 4:
                self.add_feature("extra attack")
            if self.get_level() > 7:
                self.add_feature("land's stride")
            if self.get_level() > 9:
                self.add_feature("hide in plain sight")
            if self.get_level() > 13:
                self.add_feature("vanish")
            if self.get_level() > 17:
                self.add_feature("feral senses")
            if self.get_level() > 19:
                self.add_feature("foe slayer")

    def copy_constructor(self, other, **kwargs):
        if not isinstance(other, Ranger):
            raise ValueError("Cannot copy Ranger from other class")
        super().copy_constructor(other, **kwargs)
        self._favored_enemies = other.get_favored_enemies().copy()
        self._favored_terrains = other.get_favored_terrains().copy()
        for fighting_style in other.get_fighting_styles():
            self.add_fighting_style(fighting_style)

    def get_favored_enemies(self):
        return self._favored_enemies

    def has_favored_enemy(self, enemy):
        return enemy in self.get_favored_enemies()  # TODO: update to figure out whether a Combatant is favored

    def get_favored_terrains(self):
        return self._favored_terrains

    def has_favored_terrain(self, terrain):
        return terrain in self.get_favored_terrains()

class Rogue(Character):
    def __init__(self, **kwargs):
        copy = kwargs.get("copy")
        if copy:
            self.copy_constructor(copy, name=kwargs.get("name"))
            return

        level = kwargs.get("level")
        if not level:
            raise ValueError("No level provided or level is 0")
        if not isinstance(level, int) or level < 1 or level > 20:
            raise ValueError("Level must be an integer between 1 and 20")
        hit_dice = (1 * level, 8)

        constitution = kwargs.get('constitution')
        if not constitution:
            constitution_mod = kwargs.get("constitution_mod")
            if constitution_mod is not None:
                if isinstance(constitution_mod, int):
                    self._constitution = constitution_mod
                else:
                    raise ValueError("Constitution mod must be an integer")
            else:
                raise ValueError("Must provide constitution score or modifier")
        else:  # pragma: no cover
            constitution_mod = ability_to_mod(constitution)

        max_hp = kwargs.get("max_hp")
        if max_hp and (not isinstance(max_hp, int) or max_hp <= 0):
            raise ValueError("Must provide positive integer max hp")
        else:
            max_hp = 8 + constitution_mod
            if level > 1:
                max_hp += (5 + constitution_mod) * (level - 1)

        proficiencies = kwargs.get('proficiencies', [])
        if isinstance(proficiencies, (tuple, list)):
            proficiencies = set(proficiencies)
        elif isinstance(proficiencies, set):
            pass
        elif proficiencies is None:  # pragma: no cover
            proficiencies = set()
        else:  # pragma: no cover
            raise ValueError("Proficiencies must be provided as a set, list, or tuple")
        proficiencies.add("simple weapons")
        proficiencies.add("hand crossbow")
        proficiencies.add("longsword")
        proficiencies.add("rapier")
        proficiencies.add("shortsword")
        proficiencies.add("dexterity")
        proficiencies.add("intelligence")

        proficiency_mod = proficiency_bonus_per_level(level)

        kwargs.update({"max_hp": max_hp, "proficiencies": proficiencies, "proficiency_mod": proficiency_mod,
                       "hit_dice": hit_dice})

        super().__init__(**kwargs)

        self.add_feature("sneak attack")
        sneak_dice_num = (self.get_level() - 1) // 2 + 1
        self._sneak_attack_dice = (sneak_dice_num, 6)

        if self.get_level() > 1:
            self.add_feature("cunning action")
            if self.get_level() > 4:
                self.add_feature("uncanny dodge")
            if self.get_level() > 6:
                self.add_feature("evasion")
            if self.get_level() > 10:
                self.add_feature("reliable talent")
            if self.get_level() > 13:
                self.add_feature("blindsense")
            if self.get_level() > 14:
                self.add_feature("slippery mind")
                self._proficiencies.add("wisdom")
                self._saving_throws = {"strength": self.get_strength(), "dexterity": self.get_dexterity(),
                                       "constitution": self.get_constitution(), "intelligence": self.get_intelligence(),
                                       "wisdom": self.get_wisdom(), "charisma": self.get_charisma()}
                for ability in self._saving_throws:
                    if ability in self._proficiencies:
                        self._saving_throws[ability] += self._proficiency_mod
            if self.get_level() > 17:
                self.add_feature("elusive")
            if self.get_level() > 19:
                self.add_feature("stroke of luck")
                self._stroke_of_luck_slots = 1

    def copy_constructor(self, other, **kwargs):
        if not isinstance(other, Rogue):
            raise ValueError("Cannot copy Rogue from other class")
        kwargs.update({"level": other.get_level()})
        super().copy_constructor(other, **kwargs)
        self._sneak_attack_dice = other.get_sneak_attack_dice()
        self._stroke_of_luck_slots = other.get_stroke_of_luck_slots()

    def can_see(self, light_src: str) -> bool:
        result = super().can_see(light_src)
        if not result:
            result = self.has_feature("blindsense") and not self.has_condition("deafened")  # TODO: and the creature is within 10 feet
        return result

    def get_adv_to_be_hit(self) -> int:
        super_adv = super().get_adv_to_be_hit()
        if self.has_feature("elusive") and not self.has_condition("incapacitated") and super_adv == 1:
            return 0
        return super_adv

    def get_stroke_of_luck_slots(self) -> int:
        try:
            return self._stroke_of_luck_slots
        except AttributeError:
            return 0

    def get_sneak_attack_dice(self) -> TYPE_DICE_TUPLE:
        return self._sneak_attack_dice

    def can_make_sneak_attack(self, weapon, target, adv) -> bool:  # pylint: disable=unused-argument,no-self-use
        # Note: this assumes the attack hit
        if not (weapon.has_prop("finesse") or isinstance(weapon, armory.RangedWeapon)):
            return False
        if adv == 1:
            return True
        return False
        # return adv != -1 and not target.has_prop("incapacitated")  # TODO: and another enemy of the target is within 5 feet of it and is not incapacitated

    def roll_sneak_attack_dice(self) -> int:
        return roll_dice(num=self.get_sneak_attack_dice()[0], dice_type=self.get_sneak_attack_dice()[1])

    def take_stroke_of_luck(self):
        if not self.get_stroke_of_luck_slots():
            raise ValueError("%s has no slots left for stroke of luck" % self._name)
        self._stroke_of_luck_slots -= 1

    def send_attack(self, target, attack, adv=0) -> Optional[int]:
        weapon = attack.get_weapon()
        adv_calc = calc_advantage([adv, target.get_adv_to_be_hit()])  # TODO: fix code duplication
        damage = super().send_attack(attack=attack, target=target)
        if damage is not None and self.can_make_sneak_attack(weapon, target, adv_calc):
            damage += target.take_damage(self.roll_sneak_attack_dice(), weapon.get_damage_type())
        return damage

class Sorcerer(SpellCaster, Character):
    def __init__(self, **kwargs):
        copy = kwargs.get("copy")
        if copy:
            self.copy_constructor(copy, name=kwargs.get("name"))
            return

        level = kwargs.get("level")
        if not level:
            raise ValueError("No level provided or level is 0")
        if not isinstance(level, int) or level < 1 or level > 20:
            raise ValueError("Level must be an integer between 1 and 20")
        hit_dice = (1 * level, 6)

        constitution = kwargs.get('constitution')
        if not constitution:
            constitution_mod = kwargs.get("constitution_mod")
            if constitution_mod is not None:
                if isinstance(constitution_mod, int):
                    self._constitution = constitution_mod
                else:
                    raise ValueError("Constitution mod must be an integer")
            else:
                raise ValueError("Must provide constitution score or modifier")
        else:  # pragma: no cover
            constitution_mod = ability_to_mod(constitution)

        max_hp = kwargs.get("max_hp")
        if max_hp and (not isinstance(max_hp, int) or max_hp <= 0):
            raise ValueError("Must provide positive integer max hp")
        else:
            max_hp = 6 + constitution_mod
            if level > 1:
                max_hp += (4 + constitution_mod) * (level - 1)

        proficiencies = kwargs.get('proficiencies', [])
        if isinstance(proficiencies, (tuple, list)):
            proficiencies = set(proficiencies)
        elif isinstance(proficiencies, set):
            pass
        elif proficiencies is None:  # pragma: no cover
            proficiencies = set()
        else:  # pragma: no cover
            raise ValueError("Proficiencies must be provided as a set, list, or tuple")
        proficiencies.add("dagger")
        proficiencies.add("dart")
        proficiencies.add("sling")
        proficiencies.add("quarterstaff")
        proficiencies.add("light crossbow")
        proficiencies.add("constitution")
        proficiencies.add("charisma")

        proficiency_mod = proficiency_bonus_per_level(level)

        spell_slots = {1: 2}
        if level > 1:
            spell_slots.update({1: 3})
            if level > 2:
                spell_slots.update({1:4, 2: 2})
            if level > 3:
                spell_slots.update({2: 3})
            if level > 4:
                spell_slots.update({3: 2})
            if level > 5:
                spell_slots.update({3: 3})
            if level > 6:
                spell_slots.update({4: 1})
            if level > 7:
                spell_slots.update({4: 2})
            if level > 8:
                spell_slots.update({4: 3, 5: 1})
            if level > 9:
                spell_slots.update({5: 2})
            if level > 10:
                spell_slots.update({6: 1})
            if level > 12:
                spell_slots.update({7: 1})
            if level > 14:
                spell_slots.update({8: 1})
            if level > 16:
                spell_slots.update({9: 1})
            if level > 17:
                spell_slots.update({5: 3})
            if level > 18:
                spell_slots.update({6: 2})
            if level > 19:
                spell_slots.update({7: 2})

        kwargs.update({"max_hp": max_hp, "proficiencies": proficiencies, "proficiency_mod": proficiency_mod,
                       "hit_dice": hit_dice, "spell_ability": "charisma", "spell_slots": spell_slots})

        super().__init__(**kwargs)

        if self.get_level() > 1:
            self.add_feature("font of magic")
            self._sorcery_points = self.get_level()
            self._full_sorcery_points = self._sorcery_points
            if self.get_level() > 2:
                self.add_feature("metamagic")
                self._metamagic = set()
                metamagic = kwargs.get("metamagic", ())
                for item in metamagic:
                    self.add_metamagic(item)
                # TODO: warn if len(metamagic) is less than expected

    def copy_constructor(self, other, **kwargs):
        if not isinstance(other, Sorcerer):
            raise ValueError("Cannot copy Sorcerer from other class")
        super().copy_constructor(other, **kwargs)
        if self.get_level() > 1:
            self._sorcery_points = other.get_sorcery_points()
            self._full_sorcery_points = other.get_full_sorcery_points()
            if self.get_level() > 2:
                self._metamagic = other.get_metamagic().copy()

    def get_sorcery_points(self) -> int:
        if self.has_feature("font of magic"):
            return self._sorcery_points
        return 0

    def get_full_sorcery_points(self) -> int:
        if self.has_feature("font of magic"):
            return self._full_sorcery_points
        return 0

    def get_metamagic(self) -> set:
        try:
            return self._metamagic
        except AttributeError:
            return set()

    def has_metamagic(self, item: str) -> bool:
        if not isinstance(item, str):
            raise ValueError("Metamagic name must be a string")
        return item in self.get_metamagic()

    def spend_sorcery_points(self, points: int):
        if not isinstance(points, int) or points < 1:
            raise ValueError("Sorcery points must be a positive integer")
        if points > self.get_sorcery_points():
            raise ValueError("%s doesn't have enough sorcery points to spend" % self._name)

    def reset_sorcery_points(self):
        if self.has_feature("font of magic"):
            if self.get_verbose():
                print("%s resetting sorcery points" % self._name)
            self._sorcery_points = self.get_full_sorcery_points()

    def spell_slot_to_sorcery_points(self, level: int):
        if not isinstance(level, int) or not (0 < level < 10):
            raise ValueError("Spell slot level must be an integer between 1 and 9")
        self.spend_spell_slot(level)
        self._sorcery_points += level

    def sorcery_points_to_spell_slot(self, level: int):
        if not isinstance(level, int):
            raise ValueError("Spell slot level must be an integer between 1 and 5")
        if level == 1:
            self.spend_sorcery_points(2)
        elif level == 2:
            self.spend_sorcery_points(3)
        elif level == 3:
            self.spend_sorcery_points(5)
        elif level == 4:
            self.spend_sorcery_points(6)
        elif level == 5:
            self.spend_sorcery_points(7)
        else:
            raise ValueError("Spell slot level must be an integer between 1 and 5")

    def add_metamagic(self, item: str):
        if not self.has_feature("metamagic"):
            raise ValueError("%s can't do metamagic yet" % self._name)
        if len(self.get_metamagic()) >= 2 + (self.get_level() - 3) // 7:
            raise ValueError("%s can't add another metamagic option; they aren't a high enough level")
        if item in ["careful", "distant", "empowered", "extended", "heightened", "quickened", "subtle", "twinned"]:
            if self.has_metamagic(item):
                warnings.warn("%s already has that kind of metamagic (%s). It cannot be added again." % (self._name, item))
                return
            self._metamagic.add(item)
        else:
            raise ValueError('Metamagic must be one of these kinds: "careful", "distant", "empowered", "extended", "heightened", "quickened", "subtle", "twinned"')

class Warlock(SpellCaster, Character):
    def __init__(self, **kwargs):
        copy = kwargs.get("copy")
        if copy:
            self.copy_constructor(copy, name=kwargs.get("name"))
            return

        level = kwargs.get("level")
        if not level:
            raise ValueError("No level provided or level is 0")
        if not isinstance(level, int) or level < 1 or level > 20:
            raise ValueError("Level must be an integer between 1 and 20")
        hit_dice = (1 * level, 8)

        constitution = kwargs.get('constitution')
        if not constitution:
            constitution_mod = kwargs.get("constitution_mod")
            if constitution_mod is not None:
                if isinstance(constitution_mod, int):
                    self._constitution = constitution_mod
                else:
                    raise ValueError("Constitution mod must be an integer")
            else:
                raise ValueError("Must provide constitution score or modifier")
        else:  # pragma: no cover
            constitution_mod = ability_to_mod(constitution)

        max_hp = kwargs.get("max_hp")
        if max_hp and (not isinstance(max_hp, int) or max_hp <= 0):
            raise ValueError("Must provide positive integer max hp")
        else:
            max_hp = 8 + constitution_mod
            if level > 1:
                max_hp += (5 + constitution_mod) * (level - 1)

        proficiencies = kwargs.get('proficiencies', [])
        if isinstance(proficiencies, (tuple, list)):
            proficiencies = set(proficiencies)
        elif isinstance(proficiencies, set):
            pass
        elif proficiencies is None:  # pragma: no cover
            proficiencies = set()
        else:  # pragma: no cover
            raise ValueError("Proficiencies must be provided as a set, list, or tuple")
        proficiencies.add("simple weapons")
        proficiencies.add("wisdom")
        proficiencies.add("charisma")

        proficiency_mod = proficiency_bonus_per_level(level)

        if level == 1:
            spell_slots = {1: 1}
        elif level == 2:
            spell_slots = {1: 2}
        elif level < 5:
            spell_slots = {2: 2}
        elif level < 7:
            spell_slots = {3: 2}
        elif level < 9:
            spell_slots = {4: 2}
        elif level < 11:
            spell_slots = {5: 2}
        elif level < 17:
            spell_slots = {5: 3}
        else:
            spell_slots = {5: 4}
        if level > 10:
            spell_slots.update({6: 1})  # combine Mystic Arcanum with regular spell slots because they are about the same
            if level > 12:
                spell_slots.update({7: 1})
            if level > 14:
                spell_slots.update({8: 1})
            if level > 16:
                spell_slots.update({9: 1})

        kwargs.update({"max_hp": max_hp, "proficiencies": proficiencies, "proficiency_mod": proficiency_mod,
                       "hit_dice": hit_dice, "spell_ability": "charisma", "spell_slots": spell_slots})

        super().__init__(**kwargs)

        if self.get_level() > 1:
            self.add_feature("eldritch invocations")
            self._eldritch_invocations = kwargs.get("eldritch_invocations")
            if isinstance(self._eldritch_invocations, (tuple, list)):
                self._eldritch_invocations = set(self._eldritch_invocations)
            elif not isinstance(self._eldritch_invocations, set):
                raise ValueError("Eldritch invokations must be a set (or a list or tuple to convert to set)")

            if self.get_level() > 2:
                self.add_feature("pact boon")
                self._pact_boon = kwargs.get("pact_boon")
                if not self._pact_boon:
                    raise ValueError("Must provide a pact boon")

    def copy_constructor(self, other, **kwargs):
        if not isinstance(other, Warlock):
            raise ValueError("Cannot copy Warlock from other class")
        super().copy_constructor(other, **kwargs)
        if self.has_feature("eldritch invocations"):
            self._eldritch_invocations = other.get_eldritch_invocations().copy()
        if self.has_feature("pact boon"):
            self._pact_boon = other.get_pact_boon()

    def get_pact_boon(self) -> Optional[str]:
        if self.has_feature("pact boon"):
            return self._pact_boon
        return None

    def get_eldritch_invocations(self) -> Optional[set]:
        if self.has_feature("eldritch invocations"):
            return self._eldritch_invocations
        return None

    def has_eldritch_invocation(self, invocation: str) -> bool:
        if self.has_feature("eldritch invocations"):
            return invocation in self.get_eldritch_invocations()
        return False

class Wizard(SpellCaster, Character):
    def __init__(self, **kwargs):
        copy = kwargs.get("copy")
        if copy:
            self.copy_constructor(copy, name=kwargs.get("name"))
            return

        level = kwargs.get("level")
        if not level:
            raise ValueError("No level provided or level is 0")
        if not isinstance(level, int) or level < 1 or level > 20:
            raise ValueError("Level must be an integer between 1 and 20")
        hit_dice = (1 * level, 6)

        constitution = kwargs.get('constitution')
        if not constitution:
            constitution_mod = kwargs.get("constitution_mod")
            if constitution_mod is not None:
                if isinstance(constitution_mod, int):
                    self._constitution = constitution_mod
                else:
                    raise ValueError("Constitution mod must be an integer")
            else:
                raise ValueError("Must provide constitution score or modifier")
        else:  # pragma: no cover
            constitution_mod = ability_to_mod(constitution)

        max_hp = kwargs.get("max_hp")
        if max_hp and (not isinstance(max_hp, int) or max_hp <= 0):
            raise ValueError("Must provide positive integer max hp")
        else:
            max_hp = 6 + constitution_mod
            if level > 1:
                max_hp += (4 + constitution_mod) * (level - 1)

        proficiencies = kwargs.get('proficiencies', [])
        if isinstance(proficiencies, (tuple, list)):
            proficiencies = set(proficiencies)
        elif isinstance(proficiencies, set):
            pass
        elif proficiencies is None:  # pragma: no cover
            proficiencies = set()
        else:  # pragma: no cover
            raise ValueError("Proficiencies must be provided as a set, list, or tuple")
        proficiencies.add("dagger")
        proficiencies.add("dart")
        proficiencies.add("sling")
        proficiencies.add("quarterstaff")
        proficiencies.add("light crossbow")
        proficiencies.add("intelligence")
        proficiencies.add("wisdom")

        proficiency_mod = proficiency_bonus_per_level(level)

        spell_slots = {1: 2}
        if level > 1:
            spell_slots.update({1: 3})
            if level > 2:
                spell_slots.update({1:4, 2: 2})
            if level > 3:
                spell_slots.update({2: 3})
            if level > 4:
                spell_slots.update({3: 2})
            if level > 5:
                spell_slots.update({3: 3})
            if level > 6:
                spell_slots.update({4: 1})
            if level > 7:
                spell_slots.update({4: 2})
            if level > 8:
                spell_slots.update({4: 3, 5: 1})
            if level > 9:
                spell_slots.update({5: 2})
            if level > 10:
                spell_slots.update({6: 1})
            if level > 12:
                spell_slots.update({7: 1})
            if level > 14:
                spell_slots.update({8: 1})
            if level > 16:
                spell_slots.update({9: 1})
            if level > 17:
                spell_slots.update({5: 3})
            if level > 18:
                spell_slots.update({6: 2})
            if level > 19:
                spell_slots.update({7: 2})

        kwargs.update({"max_hp": max_hp, "proficiencies": proficiencies, "proficiency_mod": proficiency_mod,
                       "hit_dice": hit_dice, "spell_ability": "intelligence", "spell_slots": spell_slots})

        super().__init__(**kwargs)

        if self.get_level() > 17:
            self.add_feature("spell mastery")
            self._spell_mastery_names = set()
            spell_names = kwargs.get("spell_mastery")
            if not isinstance(spell_names, (set, list, tuple)):
                raise ValueError("Spell mastery names must be provided in a set, list, or tuple")
            for name in spell_names:
                if not isinstance(name, str):
                    if isinstance(name, Spell):
                        name = name.get_name()
                    else:
                        raise ValueError("Spell mastery must be names of spells (or actual spells to get names from)")
                if len(self._spell_mastery_names) < 2:
                    self._spell_mastery_names.add(name)
                else:
                    raise ValueError("Too many spell mastery spells/names provided")
            if self.get_level() > 19:
                self.add_feature("signature spells")
                self._signature_spell_slots = dict()
                signature_spell_names = kwargs.get("signature_spells")
                if not isinstance(signature_spell_names, (set, list, tuple)):
                    raise ValueError("Signature spell names must be provided in a set, list, or tuple")
                for name in signature_spell_names:
                    if not isinstance(name, str):
                        if isinstance(name, Spell):
                            name = name.get_name()
                        else:
                            raise ValueError("Signature spell must be names of spells (or actual spells to get names from)")
                    if len(self._signature_spell_slots) < 2:
                        self._signature_spell_slots[name] = 1
                    else:
                        raise ValueError("Too many spell mastery spells/names provided")

    def copy_constructor(self, other, **kwargs):
        super().copy_constructor(other, **kwargs)
        if self.has_feature("spell mastery"):
            self._spell_mastery_names = other.get_spell_mastery_names()
        if self.has_feature("signature spells"):
            self._signature_spell_slots = other.get_signature_spell_slots().copy()

    def get_spell_mastery_names(self) -> set:
        return self._spell_mastery_names

    def has_spell_mastery(self, item) -> bool:
        if not self.has_feature("spell mastery"):
            return False
        if isinstance(item, str):
            name = item
        elif isinstance(item, Spell):
            name = item.get_name()
        else:
            raise ValueError("Must pass a string or a Spell to has_spell_mastery")
        return name in self.get_spell_mastery_names()

    def get_signature_spell_slots(self) -> dict:
        return self._signature_spell_slots

    def has_signature_spell(self, item) -> bool:
        if not self.has_feature("signature spells"):
            return False
        if isinstance(item, str):
            name = item
        elif isinstance(item, Spell):
            name = item.get_name()
        else:
            raise ValueError("Must pass a string or a Spell to has_signature_spell")
        return name in self.get_signature_spell_slots()

    def spend_spell_slot(self, level: int, spell=None):
        if self.has_spell_mastery(spell) and level == spell.get_level():
            return
        if self.has_signature_spell(spell) and level == spell.get_level():
            if self._signature_spell_slots[spell.get_name()]:
                self._signature_spell_slots[spell.get_name()] -= 1
                return
        super().spend_spell_slot(level)
