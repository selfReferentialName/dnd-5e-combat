# Character classes

This file contains the 12 character classes (as subclasses of Character). Basic functionality is implemented, 
but some class features are still in development. See 5E SRD for full documentation of what each class can do;
this file just notes particularly important/unexpected code implementations

## Barbarian class
Subclass of Character

### Instance variables

All of the instance variables from Character, plus:

* <b>rage_slots</b> - an integer indicating the number of uses of rage <em>self</em> has
* <b>rage_damage_bonus</b> - an integer indicating the number to add to damage rolls made by <em>self</em> while raging
* <b>rage_state</b> - a boolean indicating whether <em>self</em> is currently raging
* <b>reckless_state</b> (level 2+) - a boolean indicating whether <em>self</em> is recklessly attacking
* <b>specialization</b> - not yet implemented
* <b>relentless_rage_dc</b> (level 11+) - an integer indicating the dc for <em>self's</em> Relentless Rage feature

### Methods

All of the methods from Character, plus several others. Notable methods are:

#### Other

```start_rage(self)```

<b>Parameters</b>: None

<b>Return value</b>: None

<b>Postcondition</b>: <em>self._rage_state</em>, the damage modifiers for <em>self's</em> attack, 
and <em>self's resistances</em> are set correctly

1. Set the aforementioned variables/details according to the rules in the 5E SRD

```stop_rage(self)```

<b>Parameters</b>: None

<b>Return value</b>: None

<b>Postcondition</b>: <em>self._rage_state</em>, the damage modifiers for <em>self's</em> attack, 
and <em>self's resistances</em> are set correctly

1. Basically undo everything that was done in <em>start_rage</em>

## Bard class
Subclass of SpellCaster and Character

### Instance variables

All of the instance variables from Character, plus:

* <b>inspiration_dice</b> - the dice for inspiration that <em>self</em> can give to others
* <b>inspiration_slots</b> - an integer indicating the number of slots/uses for giving inspiration

### Methods

All of the methods from SpellCaster and Character, plus several others. Notable methods are:

#### Other

```give_inspiration(self, target: Combatant)```

<b>Parameters</b>:

* target, a Combatant

<b>Return</b>: None

1. Give a copy of <em>self._inspiration_dice</em> to target (Note: this part is currently not implemented)
2. Decrease self._inspiration_slots by 1

## Cleric
Subclass of SpellCaster and Character

### Instance variables

All of the instance variables from Character, plus:

* <b>channel_divinity_slots</b> (level 2+) - an integer indicating how many uses of channel divinity <em>self</em> has left
* <b>destroy_undead_cr</b> (level 5+) - an integer indicating the maximum challenge rating that destroy undead affects

### Methods

All of the methods from SpellCaster and Character, plus several others. Notable methods are:

#### Other

```channel_divinity(self, use_type="turn undead")```

<b>Parameters</b>:
* use_type, a string indicating the way to use channel divinity

<b>Return</b>: None

1. Decrease <em>self._channel_divinity_slots</em> by 1

## Druid class
Subclass of SpellCaster and Character

### Instance variables

All of the instance variables from Character, plus:

* <b>wild_shapes</b> (level 2+) - a set of Creatures that <em>self</em> can transform into
* <b>wild_shape_slots</b> (level 2+) - an integer indicating the number of slots <em>self</em> has left for Wild Shape
* <b>current_shape</b> (level 2+) - the current Creature that <em>self</em> is in the form of, 
or None if <em>self</em> is in their natural state

### Methods

All of the methods from SpellCaster and Character, plus several others. Notable methods are:

#### Mutators/setters

```add_wild_shape(self, beast: Creature)```

<b>Parameters</b>:
* beast, a Creature

<b>Return</b>: None

<b>Postcondition</b>: <em>self._current_shape</em> is <em>beast</em>

1. Modify <em>beast</em>'s intelligence, wisdom, charisma, and proficiencies
2. Add <em>beast</em> to <em>self._wild_shapes</em>

#### Other

```take_damage(self, damage, damage_type=None)```

<b>Parameters</b>:
* damage, an integer containing the points of damage
* damage_type, a string indicating the damage type

<b>Return value</b>: None (should be an integer)

1. If <em>self._current_shape</em> is not none, first apply the damage to <em>self._current_shape</em>, 
then end the shape and take the excess damage in <em>self</em> if necessary

```start_shape(self, beast: Creature)```

<b>Parameters</b>:
* beast, the Creature to turn into

<b>Return value</b>: None

1. Decrease <em>self._wild_shape_slots</em> by 1
2. If set <em>self._current_shape</em> to <em>beast</em>

## Fighter class
Subclass of Character

### Instance variables
All of the instance variables from Character, plus:

* <b>second_wind_slots</b> - an integer indicating the number of slots left for Second Wind
* <b>action_surge_slots</b> (level 2+) - an integer indicating the number of slots left for Action Surge
* <b>extra_attack_num</b> (level 5+) - an integer indicating the number of extra attacks allowed
* <b>indomitable_slots</b> (level 9+) - an integer indicating the number of slots left for Indomitable

### Methods
All of the methods from Character, plus several others. Notable methods are:

#### Other

```take_action_surge(self)```

<b>Parameters</b>: None

<b>Return value</b>: None

1. Decrease <em>self._action_surge_slots</em> by 1

```take_extra_attack(self)```

<b>Parameters</b>: None

<b>Return value</b>: None

1. Decrease <em>self._extra_attack_slots</em> by 1

```take_indomitable(self)```

<b>Parameters</b>: None

<b>Return value</b>: None

1. Decrease <em>self._indomitable_slots</em> by 1

## Monk class
Subclass of Character

### Instance variables
All of the instance variables from Character, plus:

* <b>martial_arts_dice</b> - the damage dice for unarmed strike or monk weapon
* <b>ki_points</b> (level 2+) - an integer indicating the number of ki points <em>self</em> has
* <b>ki_save_dc</b> (level 2+) - an integer indicating the dc for saving throw effects that use ki

### Methods
All of the methods from Character, plus several others. Notable methods are:

#### Mutators/setters

```add_attack(self, attack)```

<b>Parameters</b>:
* attack, an Attack

<b>Return value</b>: None

1. If <em>attack</em> is tied to a monk weapon and <em>self's</em> dexterity mod is greater than their strength mod, 
change the attack and damage mods to <em>self's</em> dexterity mod

## Paladin class
Subclass of SpellCaster and Character

### Instance variables
All of the instance variables from SpellCaster and Character, plus:

* <b>divine_sense_slots</b> - an integer indicating the number of slots left for Divine Sense
* <b>lay_on_hands_pool</b> - an integer indicating the number of hit points in <em>self's</em> Lay on Hands pool
* <b>aura</b> (level 6+) - an integer indicating the range of <em>self's</em> Aura of Protection and Aura of Courage
* <b>cleansing_touch_slots</b> (level 14+) - an integer indicating the number of slots left for Cleansing Touch

### Methods
All of the methods from SpellCaster and Character, plus several others. Notable methods are:

#### Other

```send_lay_on_hands(self, hp: int, target=None, use="healing")```

<b>Parameters</b>:
* hp, an integer indicating the number of hit points to take from <em>self's</em> Lay on Hands pool
* target, a Combatant to apply the effects to
* use, a string indicating what to do (e.g., "healing" heals the target)

<b>Return value</b>: an integer indicating the points <em>target</em> was healed for (if "healing" is the use)

1. Take <em>hp</em> number of points out of <em>self's</em> Lay on Hands pool
2. If <em>use</em> is "healing" and <em>target</em> is a Combatant, heal <em>target</em> for <em>hp</em> hit points

```send_divine_smite(self, target: Combatant, level=1)```

<b>Parameters</b>:
* target, a Combatant to apply the effects to
* level, an integer indicating the level to cast Divine Smite at

<b>Return value</b>: an integer indicating the points of damage dealt

1. Calculate how many dice to roll based on <em>level</em>
2. Roll damage
3. <em>target</em> takes the damage

## Ranger
Subclass of SpellCaster and Combatant

### Instance variables
All of the instance variables from SpellCaster and Character, plus:

* <b>favored_enemies</b> - a set containing strings indicating <em>self's</em> favored enemies
* <b>favored_terrains</b> - a set containing strings indicating <em>self's</em> favored terrains

### Methods
All of the methods from SpellCaster and Character, plus several others. Notable methods are:

## Rogue
Subclass of Combatant

### Instance variables
All of the instance variables from Character, plus:

* <b>sneak_attack_dice</b> - damage dice for <em>self's</em> damage dice
* <b>stroke_of_luck_slots</b> (level 20+) - an integer indicating the number of slots left for Stroke of Luck

### Methods
All of the methods from Character, plus several others. Notable methods are:

#### Other

```can_see(self, light_src: str) -> bool```

<b>Parameters</b>:
* light_src, a string indicating the light source

<b>Return value</b>: an boolean indicating whether <em>self</em> can see the square

1. Same as the superclass method, except this also checks to see if <em>self</em> has blindsense 
and isn't deafened and the target is within 10ft (that last part not implemented yet)

```get_adv_to_be_hit(self) -> int```

<b>Parameters</b>: None

<b>Return value</b>: an indeger indicating advantage

1. Same as the superclass method, except this also applies rules for elusive if necessary

```can_make_sneak_attack(self, weapon, target, adv) -> bool```

<b>Parameters</b>:
* weapon, the weapon used for the attack
* target, the Combatant that <em>self</em> is attacking
* adv, an integer indicating the advantage on the attack

<b>Return value</b>: an boolean indicating whether <em>self</em> can make a sneak attack on <em>target</em> with <em>weapon</em>

1. Check to see if <em>weapon</em> can be used for a sneak attack
2. If the attack has advantage (based on <em>adv</em>), return True
3. If the attack doesn't have disadvantage and there is an enemy of <em>target</em> within 10 feet of <em>target</em> that isn't incapacitated, 
return True (not implemented yet)
4. Else, return False

```send_attack(self, target, attack, adv=0) -> Optional[int]```

<b>Parameters</b>:
* target, the Combatant that <em>self</em> is attacking
* attack, the Attack being used
* adv, an integer indicating the advantage on the attack

<b>Return value</b>: an boolean indicating the damage taken (or None if the attack failed to hit)

1. Call the superclass method
2. If the attack hit, see if you can add sneak attack damage using <em>self.can_make_sneak_attack</em>
    1. If so, roll sneak attack damage and have <em>target</em> take the damage
3. Return the total damage taken (including sneak attack damage)

## Sorcerer
Subclass of SpellCaster and Combatant

### Instance variables
All of the instance variables from SpellCaster and Character, plus:

* <b>sorcery_points</b> (level 2+) - an integer indicating the number of sorcery points <em>self</em> has
* <b>sorcery_points</b> (level 2+) - an integer indicating the maximum number of sorcery points <em>self</em> can have (doesn't change when spending sorcery points)
* <b>metamagic</b> (level 3+) - a set of strings representing the types of metamagic <em>self</em> can use

### Methods
All of the methods from SpellCaster and Character, plus several others. Notable methods are:

#### Other

```spell_slot_to_sorcery_points(self, level: int)```

<b>Parameters</b>:
* level, an integer indicating the level of the spell slot being used

<b>Return value</b>: None

1. Spend a <em>level</em> level spell slot
2. Increase <em>self._sorcery_points</em> by <em>level</em>

```sorcery_points_to_spell_slot(self, level: int)```

<b>Parameters</b>:
* level, an integer indicating the level of the spell slot desired

<b>Return value</b>: None

1. Spend a number of sorcery points determined by (but not equal to) <em>level</em>
2. Gain one <em>level</em> level spell slot

```add_metamagic(self, item: str)```

<b>Parameters</b>:
* item, a string representing the type of metamagic to add

<b>Return value</b>: None

1. Check to see if <em>self</em> can take another metamagic type (based on <em>self._level</em>)
2. Check to see if <em>item</em> is a valid metamagic type
3. Warn the user if <em>self</em> already has this metamagic type
4. Add the metamagic type to <em>self._metamagic</em>

## Warlock

Subclass of SpellCaster and Combatant

### Instance variables
All of the instance variables from SpellCaster and Character, plus:

* <b>pact_boon</b> (level 2+) - a string indicating <em>self's</em> Pact Boon
* <b>eldritch_invocations</b> (level 2+) - a set containing strings indicating <em>self's</em> Eldritch Invocations

### Methods
All of the methods from SpellCaster and Character, plus several others. Notable methods are:

## Wizard

Subclass of SpellCaster and Combatant

### Instance variables
All of the instance variables from SpellCaster and Character, plus:

* <b>spell_mastery_names</b> (level 18+) - a set of strings indicating <em>self's</em> mastered spells
* <b>signature_spell_slots</b> (level 2+) - a dictionary mapping strings indicating <em>self's</em> Signature Spells to 
        integers indicating the number of uses of Signature Spell left for that spell

### Methods
All of the methods from SpellCaster and Character, plus several others. Notable methods are:

```spend_spell_slot(self, level: int, spell=None)```

<b>Parameters</b>:
* level, an integer indicating the spell level that is being used
* spell, the Spell that is being used

<b>Return value</b>: None

1. Check to see if <em>spell</em> is one that <em>self</em> has mastery of. If so, don't spend a spell slot.
2. Check to see if <em>spell</em> is one of <em>self's</em> Signature Spells. If so, see if the signature spell slot is available.
3. If neither 1 or 2 succeeded, call the superclass method
