# Weapon class

This class represents weapons

#### Instance variables
* <b>name</b> - a string indicating the name of the weapon
* <b>range</b> - either 0 (not a weapon that can do ranged attacks),
or a tuple (first value is an integer for normal range (ft), second value is
an integer for disadvantage range (ft))
* <b>melee_range</b> - range (ft) for a melee attack with this weapon
* <b>reach</b> - an integer indicating how many feet to increase <em>melee_range</em> by
* <b>versatile</b> - the damage die for when the weapon is used two-handed
* <b>props</b> - a set containing weapon properties/tags (finesse, two-handed, etc)
* <b>damage_dice</b> - dice used for calculating damage
* <b>damage_type</b> - a string indicating the type of damage (bludgeoning, piercing, slashing, etc)
this weapon deals
* <b>owner</b> - the Combatant that currently owns <em>self</em>
* <b>attack_bonus</b> - the modifier to attack rolls made with this weapon (e.g., a +1 sword has +1 to attacks)
* <b>damage_bonus</b> - the modifier to damage rolls made with this weapon

#### Methods

##### Initialization

```
__init__(self, **kwargs)
```
<b>Parameters</b>:
* keyword arguments (see [weapons.py](../weapons.py) for implementation)

<b>Return value</b>: None

<b>Postcondition</b>: <em>self</em> has all instance variables set correctly

Validate the input and set the instance variables, calling <em>copy_constructor</em> if necessary

```
copy_constructor(self, other, name="")
```
<b>Parameters</b>:
* other, another Combatant
* name, a string - if not specific, <em>name</em> will be set to other.get_name()

<b>Return value</b>: None

<b>Postcondition</b>: <em>self</em> has all instance variables set correctly

Make <em>self</em> a deep copy of <em>other</em>

##### Mutators/setters

```
set_name(self, name: str)
```
<b>Parameters</b>:
* name, a string

<b>Return value</b>: None

<b>Postcondition</b>: <em>self._name</em> is <em>name</em>

Set <em>self._name</em> to <em>name</em>. Warning: if the weapon
has already been added to a Combatant, the names of its attacks will not change.
To solve this, simply remove the weapon and add it back.

```
set_owner(self, owner)
```
<b>Parameters</b>:
* owner, usually a Combatant

<b>Return value</b>: None

<b>Postcondition</b>: <em>self._owner</em> is <em>owner</em>

Set <em>self._owner</em> to <em>owner</em>. Warning: currently
no type checking is performed (in case you want to give a weapon to
something other than a Combatant), so make sure you are using it correctly