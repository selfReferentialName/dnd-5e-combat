# Tactics class

This is a class designed to use an algorithm to select the best option from a list

#### Instance variables
* <b>name</b> - a string indicating what <em>self</em> is called. By default it's just the class name.
* <b>tiebreakers</b> - a list of Tactic objects. In the event that <em>self's</em> algorithm does not select exactly one item,
these tactics will be applied in order until a single item is chosen

#### Methods

##### Initialization

```
__init__(self, **kwargs)
```

<b>Parameters</b>:
* keyword arguments (see [tactics](../tactics/__init__.py) for implementation)

<b>Return value</b>: None

<b>Postcondition</b>: <em>self</em> has all instance variables set correctly

Validate the input and set the instance variables, calling <em>copy_constructor</em> if necessary

```
copy_constructor(self, other, name="")
```
<b>Parameters</b>:
* other, the Tactic to be copied
* keyword arguments (see [tactics](../tactics/__init__.py) for implementation)

<b>Return value</b>: None

<b>Postcondition</b>: <em>self</em> has all instance variables set correctly

Make <em>self</em> a deep copy of <em>other</em>. 

If copy_tactics is True, 
Tactic objects in <em>other.get_tiebreakers()</em> are copied (using the copy constructor). 
Otherwise, Tactics are added but not copied to be different instances

##### Mutators/setters

```append_tiebreaker(self, tiebreaker)```

<b>Parameters</b>:
* tiebreaker, a Tactic

<b>Return value</b>: None

<b>Postcondition</b>: <em>tiebreaker</em> is the last Tactic in <em>self._tiebreakers</em>

1. Check that <em>tiebreaker</em> is a Tactic not already in <em>self._tiebreakers</em>
2. Append <em>tiebreaker</em> to <em>self._tiebreakers</em>

##### Other

```run_tactic(self, choices: list) -> list```

<b>Parameters</b>:
* choices, a list

<b>Return value</b>: a list

This method is the one that will be overridden in subclasses to implement the subclass's algorithm. 
Default algorithm is this:

Pick one random element from <em>choices</em> and return that element

```make_choice(self, choices: list)```

<b>Parameters</b>:
* choices, a list

<b>Return value</b>: one element from <em>choices</em>

1. Call <em>self.run_tactic(choices)</em> to narrow down the list
2. If step 1 resulted in exactly one element, return that element
3. Else, repeat steps 1 and 2 for each Tactic in <em>self._tiebreakers</em> in order
4. If you still have more than one element left, select one element randomly from whatever is left, then return that element
