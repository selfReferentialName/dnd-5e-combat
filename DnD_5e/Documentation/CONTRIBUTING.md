### How to contribute
1. Read the rest of the documentation in this folder to understand how the code works
2. Look at the issues on GitLab - see the [Issue board](https://gitlab.com/cmd16/dnd-5e-combat/boards?=) and 
[Issue list](https://gitlab.com/cmd16/dnd-5e-combat/issues) to find issues that you want to work on. 
    1. Issues labeled "good first issue" and/or "help wanted" are a good place to start
    2. Other issues might be okay, but ask first, especially if the issue is labeled "refactoring"
    3. If you want to do something that is not based on a posted issue, these are the steps:
        1. Are you adding content that you are legally authorized to share (5e SRD, homebrew you own, etc.)? 
        If so, make sure the relevant license is included in [Lisences](../Licenses). If you aren't authorized to add the content,
        then don't add it.
        2. Are you fixing an issue or adding a feature that is not tied to a posted issue? If so, then create an issue 
        and wait for my (cmd16, owner) comment to confirm that your idea is reasonable before proceeding
3. If you are not already a member, create an issue with the label "membership request" with your username, 
the membership permissions you are requesting (if you want to modify code, you need "Developer"), and what you intend to do
4. Clone the repository if you haven't already. Make sure you are using the most recent version.
5. Make a separate branch for your changes
6. Make your changes, being sure to add inline comments as necessary. Acceptable changes are:
    * Address one or more posted issues
    * Add content (creatures, classes, etc.) that is in the 5e SRD
    * Add content that you are authorized to share because you are the creator or have written permission from the creator
        * If you do this, add file(s) to the Licenses folder to show the licensing details of the content you are adding
7. Update the relevant documentation to include your changes
8. Make sure you are still using the most recent version. If not, use git pull to update your code.
9. Run all tests to make sure you didn't break anything. See [README.md](../../README.md) to see how to run the tests.
10. Use a descriptive commit message, then push to your branch
11. Submit a merge request - see [GitLab documentation](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html) for details
    * If your changes are to address a posted issue, make sure to include a link to the issue(s)