# Combatant class

This class represents anything with a stat block that can fight

#### Instance variables
* <b>name</b> - a string indicating what <em>self</em> is called. A unique name is recommended but not required
* <b>verbose</b> - if this flag is turned on, logging information is printed to the console
* <b>vulnerabilities</b> - a set of damage types (in string format) <em>self</em> is vulnerable to
* <b>resistances</b> - a set of damage types (in string format) <em>self</em> is resistant to
* <b>immunities</b> - a set of damage types (in string format) <em>self</em> is immune to
* <b>ac</b> - a positive integer indicating <em>self</em>'s armor class
* <b>max_hp</b> - a positive integer indicating <em>self</em>'s max hit points
* <b>temp_hp</b> - a non-negative integer indicating <em>self</em>'s temporary hit points
* <b>hit_dice</b> - a tuple indicating <em>self</em>'s hit dice (e.g., (1, 8) for 1d8)
* <b>conditions</b> - a set of conditions (in string format)
* <b>current_hp</b> - a non-negative integer indicating <em>self</em>'s current hit points
* <b>speed</b> - a non-negative integer indicating <em>self</em>'s walking speed (other speeds currently not implemented)
* <b>vision</b> - a string indicating <em>self</em>'s vision.
Options are "normal", "darkvision", "blindsight", and "truesight"
* <b>strength</b> - an integer indicating <em>self</em>'s strength modifier
* <b>dexterity</b> - an integer indicating <em>self</em>'s dexterity modifier
* <b>constitution</b> - an integer indicating <em>self</em>'s constitution modifier
* <b>intelligence</b> - an integer indicating <em>self</em>'s intelligence modifier
* <b>wisdom</b> - an integer indicating <em>self</em>'s wisdom modifier
* <b>charisma</b> - an integer indicating <em>self</em>'s charisma modifier
* <b>proficiencies</b> - a set of proficiencies (in string format). Types of proficiencies:
    * abilities (used for saving throws)
    * weapons (simple weapons, martial weapons, monk weapons, or specific weapons)
* <b>proficiency_mod</b> - a non-negative integer indicating <em>self</em>'s proficiency mod
* <b>saving_throws</b> - a dictionary of saving throws. Each ability name
is mapped to an integer indicating the saving throw bonus for that ability.
* <b>adv_to_be_hit</b> - an integer the tracks the sum of advantage and
disadvantage conditions to attacks against self.
(For example, attacks against a raging Barbarian have advantage.)
Each advantage adds one, and each disadvantage subtracts one.
* <b>features</b> - class/race features. Currently only used by character classes
* <b>death_saves</b> - contains the number of failed and successful death saves. Not yet implemented.
* <b>attacks</b> - a list of attacks that <em>self</em> can use. See [attack_class.md](attack_class.md) for details.
* <b>weapons</b> - a list of weapons that <em>self</em> can use. See [weapons.md](weapons.md) for details.
* <b>size</b> - a string indicating the size of <em>self<em>
* <b>items</b> - a list of items that <em>self</em> can use. Not implemented yet.
* <b>fighting_styles</b> (some Character classes have this) - a set of strings indicating fighting styles

#### Methods

##### Initialization

```
__init__(self, **kwargs)
```
<b>Parameters</b>:
* keyword arguments (see [combatant](../combatant/__init__.py) for implementation)

<b>Return value</b>: None

<b>Postcondition</b>: <em>self</em> has all instance variables set correctly

Validate the input and set the instance variables, calling <em>copy_constructor</em> if necessary

```copy_constructor(self, other, **kwargs)```
<b>Parameters</b>:
* other, another Combatant
* name, a string - if not specific, <em>name</em> will be set to other.get_name()

<b>Return value</b>: None

<b>Postcondition</b>: <em>self</em> has all instance variables set correctly

Make <em>self</em> a deep copy of <em>other</em>. A few things are not copied over:
* Non-weapon attacks
* adv_to_be_hit (it is reset to 0)

```get_copy(self, name=None)```
<b>Parameters</b>:
* other, another Combatant
* name, a string - if not specific, <em>name</em> will be set to other.get_name()

1. Create a copy of <em>self</em> using <em>self's</em> copy constructor
2. Return that copy

##### Accessor/getter methods
Most methods simply return an instance variable, but others are more complicated and thus merit documentation

```
is_bloodied(self)
```
<b>Parameters</b>: None

<b>Return value</b>: a boolean

Return True if <em>self</em>'s current hp is less than or
equal to half <em>self</em>'s max hp (rounded down) and False otherwise

```
is_hp_max(self)
```
<b>Parameters</b>: None

<b>Return value</b>: a boolean

Return True if <em>self</em>'s current hp is equal to <em>self</em>'s max hp and False otherwise

```get_hp_to_max(self) -> int```

<b>Parameters</b>: None

<b>Return value</b>: an integer indicating the difference between <em>self.get_max_hp()</em> and self.get_current_hp()</em>

Return <em>self.get_max_hp()</em> - self.get_current_hp()</em>

```
can_see(self, light_src: str)
```
<b>Parameters</b>:
* light_src, a string (must be "normal", "dark", or "magic") indicating the type of light

<b>Return value</b>: a bool

Return True if <em>self</em> can see something with that light source

```
get_ability(self, ability: str)
```
<b>Parameters</b>:
* ability, a string

<b>Return value</b>: an integer

Return the modifier for the ability indicated in <em>ability</em>

```
get_adv_to_be_hit(self)
```
<b>Parameters</b>: None

<b>Return value</b>: a boolean

Return 1 if attacks against <em>self</em> have advantage,
-1 if attacks against <em>self</em> have disadvantage, and
0 if attacks against <em>self</em> don't have advantage or disadvantage

```
get_fighting_styles(self)
```
<b>Parameters</b>: None

<b>Return value</b>: a set

1. Confirm that <em>self</em> can have fighting styles
2. Create the instance variable <em>fighting_styles</em> if it doesn't already exist
3. Return the instance variable

##### Mutator/setter methods
Most methods simply modify an instance variable (after validating the input),
but others are more complicated and thus merit documentation

```
add_weapon(self, weapon)
```
<b>Parameters</b>:
* weapon, a Weapon

<b>Return value</b>: None

<b>Postcondition</b>: <em>weapon</em> is in <em>self._weapons</em> and its attacks are in <em>self._attacks</em>

1. Confirm that <em>weapon</em> is not already owned by someone/something else
2. Add the weapon to self._weapons and add its attacks to <em>self._attacks</em>
(by calling <em>add_weapon_attacks</em>)

```
remove_weapon(self, weapon)
```
<b>Parameters</b>:
* weapon, a Weapon

<b>Return value</b>: None

<b>Postcondition</b>: <em>weapon</em> is no longer in <em>self._weapons</em>
and its attacks are no longer in <em>self._attacks</em>

1. Confirm that <em>weapon</em> is owned by <em>self</em>
2. Remove the weapon from self._weapons and remove its attacks from <em>self._attacks</em>
(by calling <em>remove_weapon_attacks</em>)

```
add_weapon_attacks(self, weapon)
```
<b>Parameters</b>: weapon, a Weapon

<b>Return value</b>: None

<b>Postcondition</b>: <em>weapon</em>'s attacks are in <em>self._attacks</em>

1. Confirm that <em>weapon</em> is not already owned by
someone/something else
2. Create Attack objects for the various attacks.
Look at finesse, range (regular and disadvantage), melee range, versatile,
and whether <em>self</em> has the archery fighting style

##### Other methods

```
send_attack(self, attack, target, adv=0)
```
<b>Parameters</b>:
* attack, an Attack
* target, a Combatant
* adv, an integer indicating advantage/disadvantage

<b>Return value</b>: None

1. Calculate the advantage for <em>self</em> to hit <em>target</em> with attack
2. Call <em>attack</em>'s <em>make_attack</em> method

```
take_attack(self, attack_result, source=None, attack=None)
```
<b>Parameters</b>:
* attack_result, a tuple that contains the number rolled (including modifiers)
and an integer that indicates whether the hit was a critical hit
* source, a Combatant
* attack, the attack that <em>self</em> is getting hit with

<b>Return value</b>: a boolean

1. Check the crit value. Critical fail is an auto-miss,
and critical success is an auto-hit.
2. If the roll is not a crit,
then the roll value is compared against <em>self</em>'s ac
3. Return True if the attack hits and False otherwise

```
take_saving_throw(self, save_type, dc, attack=None, adv=0)
```
<b>Parameters</b>:
* save_type, a string that indicates the ability to use for the saving throw
* dc, the DC for the saving throw
* attack, the attack that the saving throw is tied to
* adv, an integer indicating whether <em>self</em> has advantage on the saving throw

<b>Return value</b>: a boolean

Call <em>make_saving_throw</em> and compare the result to <em>dc</em>,
returning True if the result is greater than or equal to <em>dc</em> and False otherwise

```
make_saving_throw(self, save_type, adv=0)
```
<b>Parameters</b>:
* save_type, a string that indicates the ability to use for the saving throw
* adv, an integer indicating whether <em>self</em> has advantage on the saving throw

<b>Return value</b>: an integer

Roll a d20 using the saving throw modifier for <em>save_type</em>
and the advantage provided in <em>adv</em>, then return the result

```
take_damage(self, damage, damage_type=None)
```
<b>Parameters</b>:
* damage, an integer containing the points of damage
* damage_type, a string indicating the damage type

<b>Return value</b>: an integer

1. Check if <em>self</em> is vulnerable, resistant, or immune to <em>damage_type</em>
and modify <em>damage</em> accordingly.
2. Reduce hp, starting with temp hp,
until all damage has been taken or current_hp is less than 1.
    1. If that second condition is met, check to see whether to go unconscious or die instantly
based on the amount of damage taken.

```
take_healing(self, healing)
```
<b>Parameters</b>:
* healing, an integer containing the points of healing

<b>Return value</b>: an integer

1. Increase current hit points by the minimum of <em>healing</em> and self's
hp max
2. Return the hit points healed (which may be less than <em>healing</em>)

```
become_unconscious(self)
```
<b>Parameters</b>: None

<b>Return value</b>: None

Add the unconscious condition and set current hp to 0

```
die(self)
```
<b>Parameters</b>: None

<b>Return value</b>: None

1. Clear out <em>self</em>'s conditions
2. Add the dead condition
3. Set current hp and temp hp to 0

## Creature class
Subclass of Combatant

#### Instance variables
All of the instance variables in Combatant, plus:
* <b>cr</b> - a number (integer or float) indicating the challenge rating of <em>self</em>
* <b>xp</b> - an integer indicating the experience points <em>self</em> is worth

#### Methods
Noteworthy changes are:

##### Initialization

```
__init__(self, **kwargs)
```
<b>Parameters</b>:
* keyword arguments (see [combatant.py](../combatant.py) for implementation)

<b>Return value</b>: None

<b>Postcondition</b>: <em>self</em> has all instance variables set correctly

1. Set cr
2. Set xp (if xp is not provided, calculate it using the cr_to_xp method in utility_methods_dnd.py)
3. Set proficiency mod using the proficiency_bonus_by_cr method in utility_methods_dnd.py
4. Call the superclass's constructor

## Character class
Subclass of Combatant

#### Instance variables
All of the instance variables in Combatant, plus:
* <b>level</b> - an integer between 1 and 20 indicating <em>self</em>'s level

## SpellCaster class
Subclass of Combatant

#### Instance variables
All of the instance variables in Combatant, plus:
* <b>spell_ability</b> - a string indicating the ability used for spellcasting
* <b>spell_ability_mod</b> - an integer indicating the ability mod for <em>spell_ability</em>
* <b>spell_save_dc</b> - an integer indicating the dc for saving throw spells
* <b>spell_attack_mod</b> - an integer indicating the attack mod for spells
* <b>spell_slots</b> - a dictionary that maps each spell level (integer)
to the number of slots available for that level (integer)
* <b>full_spell_slots</b> - the same as <em>spell_slots</em> except that it tracks the maximum slots
instead of the currently available slots
* <b>spells</b> - a list of Spell objects that <em>self</em> can cast. See [attack_class.md] for details.

#### Methods
Noteworthy changes are:

##### Initialization

```
__init__(self, **kwargs)
```
<b>Parameters</b>:
* keyword arguments (see [combatant.py](../combatant.py) for implementation)

<b>Return value</b>: None

<b>Postcondition</b>: <em>self</em> has all instance variables set correctly

1. Call the superclass's constructor
2. Set class-specific instance variables
3. For each spell, call <em>add_spell</em>

##### Mutators/setters

```
add_spell(self, spell)
```
<b>Parameters</b>:
* spell, a Spell

<b>Return value</b>: None

<b>Postcondition</b>: <em>spell</em> is in <em>self._spells</em>

1. Confirm that <spell> is a Spell
2. Add it to <em>self._spells</em>

```
spend_spell_slot(self, level, spell=None)
```
<b>Parameters</b>:
* level, an integer indicating the spell level that is being used
* spell, the Spell that is being used

<b>Return value</b>: None

<b>Postcondition</b>: <em>self._spell_slots</em>\[<em>level</em>\] is decreased by 1

Decrease <em>self._spell_slots</em>\[<em>level</em>\] by 1

```
reset_spell_slots(self)
```
<b>Parameters</b>: None

<b>Return value</b>: None

<b>Postcondition</b>: <em>self._spell_slots</em> is back to its original values

Reset the spell slots for each level based on the info in <em>self._full_spell_slots</em>