# Attack class

This class represents attacks

#### Instance variables
* <b>name</b> - a string indicating what <em>self</em> is called. A unique name is recommended but not required
* <b>damage_dice</b> - a tuple indicating the of damage dice this attack uses.
See [attack_class](../attack_class/__init__.py) or hit_dice in [combatant.md] for examples
* <b>attack_mod</b> - an integer modifier to attack rolls
* <b>damage_mod</b> - an integer modifier to damage rolls
* <b>damage_type</b> - a string indicating the type of damage self deals
* <b>range</b> - a positive integer indicating the range for a ranged attack
* <b>melee_range</b> - a positive integer indicating the range for a melee attack
* <b>adv</b> - an integer (-1, 0, or 1) indicating self's advantage to hit
* <b>weapon</b> - a Weapon that self is tied to

#### Methods

##### Accessors/getters

```get_max_hit(self) -> int```

<b>Parameters</b>: None

<b>Return value</b>: an integer indicating the max number that could result from an attack roll

Return 20 + <em>self.get_attack_mod()</em>

```get_average_hit(self) -> int```

<b>Parameters</b>: None

<b>Return value</b>: an integer indicating the average number that would result from an attack roll

Return 10 + <em>self.get_attack_mod()</em>

```get_max_damage(self) -> int```

<b>Parameters</b>: None

<b>Return value</b>: an integer indicating the max damage that could result from a damage roll (not counting extra damage from a critical hit)

Return <em>self's</em> dice number * <em>self's</em> dice type + <em>self.get_damage_mod()</em>

```get_average_damage(self) -> int```

<b>Parameters</b>: None

<b>Return value</b>: an integer indicating the average damage that would result from a damage roll

Return <em>self's</em> dice number * (<em>self's</em> dice type divided by 2) + <em>self.get_damage_mod()</em>

##### Other

```
roll_attack(self, adv=0)
```
<b>Parameters</b>:
* adv (indicates advantage or disadvantage on the attack roll)

<b>Return value</b>: an tuple containing an integer for the attack roll
and an integer (-1, 0, or 1) to indicate the crit status of the roll

1. Roll a d20 (with advantage/disadvantage as computed with
<em>adv</em> and <em>self._adv</em>), adding <em>self._attack_mod</em>
2. Return the roll and an indication of whether it was a critical hit,
critical miss, or neither

```
roll_damage(self, crit=0, crit_multiplier=2)
```
<b>Parameters</b>:
* crit, an integer indicating whether the hit was a crit
* crit_multiplier, an integer

<b>Return value</b>: an tuple containing an integer for the attack roll
and an integer (-1, 0, or 1) to indicate the crit status of the roll

1. Roll <em>self._damage_dice</em> (if crit == 1,
multiply the number of dice to roll by <em>crit_multiplier</em>)
2. Add <em>self._damage_mod</em>

```
make_attack(self, source, target, adv=0)
```
<b>Parameters</b>:
* source, the Combatant that is making the attack
* target, the Combatant that <em>source</em> is attacking
* adv, an integer that indicates advantage on the attack roll

<b>Return value</b>: an integer

1. Roll attack using <em>self.roll_attack(adv)</em> and store the result
in variable <em>result</em>
2. Call <em>target.take_attack(result)</em> to see if the attack hits
(see [combatant.md] for documentation of <em>take_attack</em>)
3. If the attack hits, call <em>self._send_damage</em>
4. Return the damage taken (if the attack misses, damage taken is 0)

```
send_damage(self, target, crit=0, crit_multiplier=2)
```
<b>Parameters</b>:
* target, the Combatant that is taking the damage
* crit, an integer indicating whether the hit was a crit
* crit_multiplier, an integer

<b>Return value</b>: an integer

1. Roll damage using <em>self.roll_damage</em> and store the result in variable <em>damage</em>
2. Call <em>target.take_damage</em>, passing in <em>damage</em> and <em>self._damage_type</em>
3. Return the damage taken (as returned by <em>target.take_damage</em>)

## SavingThrowAttack class
Subclass of Attack

#### Instance variables
All of the instance variables in Attack, plus:
* <b>dc</b> - an integer indicating the save dc for <em>self</em>
* <b>save_type</b> - a string indicating the ability to use for the saving throw

#### Methods
Noteworthy changes are:

##### Other

```
make_attack(self, source, target, adv=0)
```
<b>Parameters</b>:
* source, the Combatant that is making the attack
* target, the Combatant that <em>source</em> is attacking
* adv, an integer that indicates advantage on the attack roll

<b>Return value</b>: an integer

1. Call <em>target.take_saving_throw</em>
2. Call <em>self.send_damage</em>, passing in <em>saved=True</em>
if <em>target</em> made their saving throw
3. Return the value returned by <em>self.send_damage</em>

```
send_damage(self, saved=False)
```
<b>Parameters</b>:
* target, the Combatant that is taking the damage
* saved, a boolean indicating whether the saving throw succeeded

<b>Return value</b>: an integer

1. Roll damage using <em>self.roll_damage</em> and store the result in variable <em>damage</em>
2. Figure out how much damage <em>target</em> should take:
    1. If <em>saved</em> is False, <em>target</em> takes full damage
    2. Else if <self._damage_on_success>, <em>target</em> takes half damage (rounded down)
    3. Else, <em>target</em> takes no damage
3. Call <em>target.take_damage</em> using the value calculated in steps 1 and 2

## Spell class
Subclass of Attack

#### Instance variables
All of the instance variables in Attack, plus:
* <b>level</b> - an integer indicating the spell level
* <b>school</b> a string indicating the spell's school of magic (currently not used)
* <b>casting_time</b> - a string indicating the casting time
("1 action", "1 bonus action", or "1 reaction")
* <b>components</b> - a list containing "v", "s", and/or "m" (the components needed to cast the spell)
* <b>duration</b> - an integer indicating the number of rounds the spell lasts for
(instantaneous durations are represented as 0)

#### Methods
Noteworthy changes are:

##### Other

```
make_attack(self, source, target, adv=0, level=None)
```
<b>Parameters</b>:
* source, the Combatant making the attack
* target, the Combatant that is being attacked
* adv, an integer indicating advantage
* level, an integer indicating the level the spell is being cast at

<b>Return value</b>: an integer (or None if the attack didn't hit)

1. Spend a spell slot of level <em>level</em>
2. Call the superclass's <em>make_attack</em> and return the result

## SavingThrowSpell class
Subclass of Spell and SavingThrowAttack

No extra instance variables or methods; Python is good at multiple inheritance

## HealingSpell class
Subclass of Spell

#### Methods
Noteworthy changes are:

##### Other

```
make_attack(self, source, target, adv=0, level=None)
```
<b>Parameters</b>:
* source, the Combatant making the attack
* target, the Combatant that is being attacked
* adv, an integer indicating advantage
* level, an integer indicating the level the spell is being cast at

<b>Return value</b>: an integer

1. Deal with the spell level (as indicated in <em>Spell.make_attack</em>
2. Heal <em>target</em> for the hp calculated in <em>self.roll_damage</em>
3. Return the points healed