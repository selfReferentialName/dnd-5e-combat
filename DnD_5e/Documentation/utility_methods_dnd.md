# Utility methods

utility_methods_dnd.py contains functions and constants used by multiple other files

## Constants/Types

```TYPE_ROLL_RESULT = Tuple[int, int]```

A type that is a tuple containing a number (the result of a roll) and an int indicating whether the roll was a crit or not

```TYPE_DICE_TUPLE = Tuple[int, int]```

A type that is a tuple containing the number of dice (int) and the number of faces on the dice (int)

## Functions

```ability_to_mod(score: int) -> int```

<b>Parameters</b>:
* score, an integer indicating an ability score

<b>Return value</b>: an integer indicating an ability modifier

1. Check that <em>score</em> is valid (an integer between 1 and 30)
2. Return <em>score</em> - 10 divided by 2 (rounded down)

```validate_dice(dice) -> TYPE_DICE_TUPLE```

<b>Parameters</b>:
* dice, ideally a string or tuple

<b>Return value</b>: dice in the format of TYPE_DICE_TUPLE

1. First check to see if <em>dice</em> is already in the format of TYPE_DICE_TUPLE. If so, just check the values are reasonable
2. Else, see if <em>dice</em> is a string like "1d6". If so, parse it and convert it to TYPE_DICE_TUPLE format
3. If both cases failed, the input is invalid. Raise a ValueError.
4. If there was no failure, return <em>dice</em> (which has now been modified to have the correct format)

```roll_dice(dice_type: TYPE_DICE_TUPLE, num=1, modifier=0, adv=0, critable=False) -> TYPE_ROLL_RESULT```

<b>Parameters</b>:
* dice_type, an integer indicating the number of faces on the dice
* num, an integer indicating how many times to roll the dice
* modifier, a number to add the the final roll after all the dice have been rolled
* adv, an integer: 0 is normal, -1 means disadvantage (roll twice and take lower), and 1 means advantage (roll twice and take higher)
* critable, a boolean that indicates whether rolling the maximum number for the dice means anything special

<b>Return value</b>: a result in the format of ROLL_RESULT

1. For each number (integer) from 1 to <em>num</em>:
    1. Roll the dice and store the value
    2. If <em>adv</em> is not 0, apply advantage or disadvantage. (Note: if a truthy value not equal to 1 is provided, it will be treated as disadvantage)
    3. If <em>critable</em>, check to see if the roll is a crit (1 represents crit success, -1 represents crit failure, 0 is normal)
2. Return the result

```cr_to_xp(cr: int) -> int```

<b>Parameters</b>:
* cr, an integer indicating the challenge rating of a creature

<b>Return value</b>: an integer indicating the xp value

1. Return <em>cr</em> times 200

```calc_advantage(advs) -> int```

<b>Parameters</b>:
* advs, an tuple or list containing integers

<b>Return value</b>: an integer indicating advantage/disadvantage

1. Sum all numbers in <em>advs</em>
2. A sum greater than 0 means advantage, a sum less than 0 means disadvantage
3. Return -1, 0, or 1 indicating disadvantage, normal, or advantage

```time_to_rounds(time_var) -> int```

<b>Parameters</b>:
* time_var, which represents a duration

<b>Return value</b>: an integer indicating duration

1. If <em>time_var</em> is "instantaneous", duration is 0
2. Break the input into a number and a type (e.g., 1 minute)
3. If the type is not "rounds", convert the number to rounds (1 round is 6 seconds)
4. Return the number of rounds

```proficiency_bonus_per_level(level: int) -> int```

<b>Parameters</b>:
* level, an integer that represents a character level

<b>Return value</b>: an integer indicating proficiency bonus

1. Return (level - 1) / 4 + 2 (rounded down)

```proficency_bonus_by_cr(cr: int) -> int```

<b>Parameters</b>:
* level, an integer that represents a challenge rating

<b>Return value</b>: an integer indicating proficiency bonus

1. Return proficiency bonus based on the table in 5E SRD

```ability_from_abbreviation(name: str) -> str```

<b>Parameters</b>:
* name, a string abbreviation for an ability name; e.g., "str" for strength

<b>Return value</b>: a string for the full ability name

1. Return the full attribute name