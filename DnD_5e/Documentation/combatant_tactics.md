# CombatantTactic class
Subclass of Tactic (see [tactics.md](tactics.md)). This is for Tactics for selecting Combatants.

## LowestAcTactic
Subclass of CombatantTactic

Select the Combatant(s) with the lowest AC

Note: this assumes all Combatants have AC lower than 100. 
If your AC is higher than 100, I don't know what game you think you're playing.

## HighestAcTactic
Subclass of CombatantTactic

Select the Combatant(s) with the highest AC

Note: this assumes all Combatants have AC higher than 0

## LowAcTactic
Subclass of CombatantTactic

Select the Combatant(s) with AC lower than <em>self._threshold</em>

#### Instance variables
All of the instance variables in CombatantTactic, plus
* <b>threshold</b>, an integer indicating the AC below which to return results

#### Methods

##### Initialization

```
__init__(self, **kwargs)
```
<b>Parameters</b>:
* keyword arguments

<b>Return value</b>: None

<b>Postcondition</b>: <em>self</em> has all instance variables set correctly

1. Call the superclass's constructor
2. Try to set <em>self._threshold</em> directly
3. If no threshold is provided, look for the <em>attack</em> parameter
    1. Set <em>self._threshold</em> based on <em>attack.get_max_hit()</em> or <em>attack.get_average_hit()</em>, 
    depending on whether the parameter <em>use_max</em> is True or False (False by default)
    2. Note: the numbers obtained from step 3.1 are increased by 1 to account for the fact that the threshold is exclusive but hitting is inclusive

## LowAcTactic
Subclass of CombatantTactic

Select the Combatant(s) with AC higher than <em>self._threshold</em>

#### Instance variables
All of the instance variables in CombatantTactic, plus
* <b>threshold</b>, an integer indicating the AC above which to return results

## BloodiedTactic
Subclass of CombatantTactic

Select the Combatant(s) that are bloodied

## MaxHpTactic
Subclass of CombatantTactic

Select the Combatant(s) that are at max hp

## HpToMaxHighTactic
Subclass of CombatantTactic

Select the Combatant(s) whose current hp is more than <em>threshold</em> points below their max hp

#### Instance variables
All of the instance variables in CombatantTactic, plus
* <b>threshold</b>, an integer indicating the hp below max above which to return results

#### Methods

##### Initialization

```
__init__(self, **kwargs)
```
<b>Parameters</b>:
* keyword arguments

<b>Return value</b>: None

<b>Postcondition</b>: <em>self</em> has all instance variables set correctly

1. Call the superclass's constructor
2. Try to set <em>self._threshold</em> directly
3. If no threshold is provided, look for the <em>healing</em> parameter
    1. Set <em>self._threshold</em> based on <em>attack.get_max_damage()</em> or <em>attack.get_average_damage()</em>, 
    depending on whether the parameter <em>use_max</em> is True or False (True by default)
    2. Note: the numbers obtained from step 3.1 are decreased by 1 to account for the fact that the threshold is exclusive 
    but healing is inclusive

## MurderHoboTactic
Subclass of CombatantTactic

Select the Combatant(s) that are unconscious

## HasTempHpTactic
Subclass of CombatantTactic

Select the Combatant(s) that have temp hp

## NoTempHpTactic
Subclass of CombatantTactic

Select the Combatant(s) that don't have temp hp

## HasVulnerabilityTactic
Subclass of CombatantTactic

Select the Combatant(s) that are vulnerable to <em>self._damage_type</em>

#### Instance variables
All of the instance variables in CombatantTactic, plus
* <b>damage_type</b>, an string indicating the damage type to check

## HasResistanceTactic
Subclass of CombatantTactic

Select the Combatant(s) that are resistant to <em>self._damage_type</em>

#### Instance variables
All of the instance variables in CombatantTactic, plus
* <b>damage_type</b>, an string indicating the damage type to check

## HasImmunityTactic
Subclass of CombatantTactic

Select the Combatant(s) that are immune to <em>self._damage_type</em>

#### Instance variables
All of the instance variables in CombatantTactic, plus
* <b>damage_type</b>, an string indicating the damage type to check
