# Armory file

armory.py contains the weapons in the 5e SRD as subclasses of Weapon

* Every weapon in the armory is an instance of SimpleWeapon or MartialWeapon
and MeleeWeapon or RangedWeapon, all of which are subclasses of Weapon
that are used for type/instance checking (e.g., to determine if a Combatant is proficient with the weapon)
* Monk weapons are determined by the function <em>is_monk_weapon</em>. See [armory](../armory/__init__.py) or the 5E SRD for
an explanation
* Weapons in the armory define their damage dice, damage type, and other properties
* For more details on weapons, see [weapons.md](weapons.md)
