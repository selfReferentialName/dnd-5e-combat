import warnings
from typing import Optional
from DnD_5e.utility_methods_dnd import roll_dice, validate_dice, calc_advantage, time_to_rounds, TYPE_DICE_TUPLE, TYPE_ROLL_RESULT
from DnD_5e import weapons

class Attack:
    def __init__(self, **kwargs):
        name = kwargs.get("name")
        copy = kwargs.get("copy")
        if copy:
            self.copy_constructor(other=copy, name=name)
            return

        self._name = name
        if not isinstance(name, str):
            raise ValueError("Name should be a string")

        self._damage_dice = validate_dice(kwargs.get("damage_dice"))  # providing no dice will be handled by validate_dice

        attack_mod = kwargs.get("attack_mod", 0)
        if not isinstance(attack_mod, int):
            raise ValueError("Attack mod should be an integer")
        self._attack_mod = attack_mod

        damage_mod = kwargs.get("damage_mod", 0)
        if not isinstance(damage_mod, int):
            raise ValueError("Damage mod should be an integer")
        self._damage_mod = damage_mod

        damage_type = kwargs.get("damage_type", "")
        if not isinstance(damage_type, str):
            raise ValueError("Damage type should be a string")
        self._damage_type = damage_type

        range_val = kwargs.get("range", 0)
        if isinstance(range_val, int):
            self._range = range_val
        else:  # TODO: give a warning?
            warnings.warn("Non-integer value for range provided")
            self._range = range_val

        melee_range = kwargs.get("melee_range", 0)
        if isinstance(melee_range, int):
            self._melee_range = melee_range
        else:
            self._melee_range = 0

        if not self._range and not self._melee_range:  # assume this is a melee attack if not otherwise specified
            self._melee_range = 5

        adv = kwargs.get("adv", 0)
        if adv in [-1, 0, 1]:
            self._adv = adv
        else:
            raise ValueError("Advantage must be -1, 0, or 1")

        weapon = kwargs.get("weapon")
        if isinstance(weapon, weapons.Weapon):
            self._weapon = weapon
        elif weapon:
            raise ValueError("Weapon should be a Weapon object")
        else:
            self._weapon = None

    def copy_constructor(self, other, **kwargs):
        if not isinstance(other, Attack):
            raise ValueError("Cannot make self a copy of something that is not an Attack")

        if other.get_weapon():
            warnings.warn("Not recommended to copy an attack tied to a weapon. Copy the weapon itself and assign it to a person.")

        name = kwargs.get("name")
        if not name or not isinstance(name, str):
            name = other.get_name()

        Attack.__init__(self=self, damage_dice=other.get_damage_dice(), attack_mod=other.get_attack_mod(), damage_mod=other.get_damage_mod(),
                      damage_type=other.get_damage_type(), range=other.get_range(), melee_range=other.get_melee_range(),
                      adv=other.get_adv(), name=name, weapon=other.get_weapon())

    def get_copy(self, name=""):
        copy_obj = type(self)(copy=self, name=name)
        return copy_obj

    def get_damage_dice(self) -> TYPE_DICE_TUPLE:
        return self._damage_dice

    def get_attack_mod(self) -> int:
        return self._attack_mod

    def get_damage_mod(self) -> int:
        return self._damage_mod

    def get_damage_type(self) -> Optional[str]:
        return self._damage_type

    def get_range(self) -> int:
        return self._range

    def get_melee_range(self) -> int:
        return self._melee_range

    def get_adv(self) -> int:
        return self._adv

    def get_name(self) -> str:
        return self._name

    def get_weapon(self) -> Optional[weapons.Weapon]:
        return self._weapon

    def get_max_hit(self) -> int:
        return 20 + self.get_attack_mod()

    def get_average_hit(self) -> int:
        return 10 + self.get_attack_mod()

    def get_max_damage(self) -> int:
        return self.get_damage_dice()[0] * self.get_damage_dice()[1] + self.get_damage_mod()

    def get_average_damage(self) -> int:
        result = 0
        for i in range(self.get_damage_dice()[0]):  # pylint: disable=unused-variable
            result += self.get_damage_dice()[1] // 2
        result += self.get_damage_mod()
        return result

    def set_attack_mod(self, attack_mod: int):
        if isinstance(attack_mod, int):
            self._attack_mod = attack_mod
        else:
            raise ValueError("Attack mod must be an integer")

    def set_damage_mod(self, damage_mod: int):
        if isinstance(damage_mod, int):
            self._damage_mod = damage_mod
        else:
            raise ValueError("Damage mod must be an integer")

    def roll_attack(self, adv=0) -> TYPE_ROLL_RESULT:  # adv is the additional advantage afforded by circumstance
        return roll_dice(20, adv=calc_advantage([self._adv, adv]), modifier=self._attack_mod, critable=True)

    def roll_damage(self, crit=0, crit_multiplier=2) -> int:
        num = self._damage_dice[0]
        if crit:
            if not isinstance(crit_multiplier, int):
                raise ValueError("Crit multiplier must be an integer")
            num *= crit_multiplier
        return roll_dice(dice_type=self._damage_dice[1], num=num, modifier=self._damage_mod, critable=False)[0]  # don't need crit info

    def make_attack(self, source, target, adv=0) -> Optional[int]:
        result = self.roll_attack(adv=adv)
        verbose = source.get_verbose()
        try:
            if verbose:  # pragma: no cover
                print("%s attacks %s with %s and rolls a %d." % (source.get_name(), target.get_name(), self.get_name(),
                                                                  result[0]), end=" ")
            damage_taken = 0
            if target.take_attack(result, source=source, attack=self):  # take_attack returns True if attack hits
                if verbose:  # pragma: no cover
                    if result[1] == 1:
                        print("Critical hit!", end=" ")  # leave room for damage info
                    else:
                        print("Hit!", end=" ")
                if source.has_feature("brutal critical"):
                    crit_multiplier = 3
                else:
                    crit_multiplier = 2
                damage_taken = self.send_damage(target, crit=result[1], crit_multiplier=crit_multiplier)
                return damage_taken
            # if the attack failed to hit
            if verbose:  # pragma: no cover
                if result[1] == -1:
                    print("Critical miss.")
                else:
                    print("Miss.")
            return None  # explicity return None so it's clear from the return value whether the attack hit
        except NameError:
            raise ValueError("%s tried to attack something that can't take attacks" % source.get_name())

    def send_damage(self, target, crit=0, crit_multiplier=2):
        damage = self.roll_damage(crit=crit, crit_multiplier=crit_multiplier)
        return target.take_damage(damage, damage_type=self._damage_type)

class MultiAttack(Attack):
    def __init__(self, **kwargs):
        copy = kwargs.get("copy")
        if copy:
            self.copy_constructor(other=copy, **kwargs)
            return

        self._name = kwargs.get("name", "multiattack")
        if not isinstance(name, str):
            raise ValueError("Name should be a string")

        attack_list = kwargs.get("attacks")
        if not isinstance(attack_list, (list, tuple)):
            raise ValueError("Attacks must be a list or tuple")
        self._attack_list = []
        for attack in attack_list:
            self.add_attack(attack)
        if len(self._attack_list) < 2:
            raise ValueError("MultiAttack must contain 2 or more Attacks")

    def copy_constructor(self, other, **kwargs):
        name = kwargs.get("name")
        if not name or not isinstance(name, str):
            name = "multiattack"
        deep_copy = kwargs.get("deep_copy")
        for attack in other.get_attacks():
            if deep_copy:
                attack = attack.get_copy()
            self.add_attack(attack)

    def get_attacks(self) -> list:
        return self._attack_list

    def add_attack(self, attack: Attack):
        if not isinstance(attack, Attack):
            raise ValueError("Each attack must be an Attack object")
        self._attack_list.append(attack)

    def get_name(self) -> str:
        return self._name

    def make_attack(self, source, target, adv=0) -> Optional[int]:
        # target can be passed in as a single target (apply all attacks to target)
        # or as a list or tuple of targets (apply in order). If the numbers don't line up, the last target will be repeated as needed.
        # the same is true of the adv parameter
        if not isinstance(target, (list, tuple)):
            targets = [target for i in range(len(self.get_attacks()))]
        elif len(target) < len(self.get_attacks()):
            targets = target
            for i in range(len(target), len(self.get_attacks())):
                targets.append(target[-1])
        if not isinstance(adv, (list, tuple)):
            advs = [adv for i in range(len(self.get_attacks()))]
        elif len(adv) < len(self.get_attacks()):
            advs = adv
            for i in range(len(adv), len(self.get_attacks())):
                advs.append(adv[-1])
        total_damage = 0
        for i in range(len(self.get_attacks())):
            current_damage = self.get_attacks()[i].make_attack(source, target=targets[i], adv=advs[i])
            if current_damage:  # done to avoid errors from adding None
                total_damage += total_damage
        return total_damage

    def get_damage_dice(self) -> TYPE_DICE_TUPLE:
        raise NotImplementedError("Call get_damage_dice on one of the attacks in this multiattack")

    def get_attack_mod(self) -> int:
        raise NotImplementedError("Call get_attack_mod on one of the attacks in this multiattack")

    def get_damage_mod(self) -> int:
        raise NotImplementedError("Call get_damage_mod on one of the attacks in this multiattack")

    def get_damage_type(self) -> Optional[str]:
        raise NotImplementedError("Call get_damage_type on one of the attacks in this multiattack")

    def get_range(self) -> int:
        raise NotImplementedError("Call get_range on one of the attacks in this multiattack")

    def get_melee_range(self) -> int:
        raise NotImplementedError("Call get_melee_range on one of the attacks in this multiattack")

    def get_adv(self) -> int:
        raise NotImplementedError("Call get_adv on one of the attacks in this multiattack")

    def get_weapon(self) -> Optional[weapons.Weapon]:
        raise NotImplementedError("Call get_weapon on one of the attacks in this multiattack")

    def get_max_hit(self) -> int:
        raise NotImplementedError("Call get_max_hit on one of the attacks in this multiattack")

    def get_average_hit(self) -> int:
        raise NotImplementedError("Call get_average_hit on one of the attacks in this multiattack")

    def get_max_damage(self) -> int:
        raise NotImplementedError("Call get_max_damage on one of the attacks in this multiattack")

    def get_average_damage(self) -> int:
        raise NotImplementedError("Call get_average_damage on one of the attacks in this multiattack")

    def set_attack_mod(self, attack_mod: int):
        raise NotImplementedError("Call set_attack_mod on one of the attacks in this multiattack")

    def set_damage_mod(self, damage_mod: int):
        raise NotImplementedError("Call set_damage_mod on one of the attacks in this multiattack")

    def roll_attack(self, adv=0):
        raise NotImplementedError("Call roll_attack on one of the attacks in this multiattack")

    def roll_damage(self, crit=0, crit_multiplier=2):
        raise NotImplementedError("Call roll_damage on one of the attacks in this multiattack")

class SavingThrowAttack(Attack):
    def __init__(self, **kwargs):
        copy = kwargs.get("copy")
        if copy:
            self.copy_constructor(other=copy, **kwargs)
            return

        super().__init__(**kwargs)  # irrelevant keyword arguments will be ignored
        dc = kwargs.get("dc")
        if not dc or not isinstance(dc, int):
            raise ValueError("Must provide DC (an int)")
        self._dc = dc
        save_type = kwargs.get("save_type")
        if save_type in ["strength", "dexterity", "constitution", "intelligence", "wisdom", "charisma"]:
            self._save_type = save_type
        else:
            raise ValueError("Save type must be strength, dexterity, constitution, intelligence, wisdom, or charisma")
        damage_on_success = kwargs.get("damage_on_success")
        self._damage_on_success = bool(damage_on_success)  # half damage on successful save

    def copy_constructor(self, other, **kwargs):
        super().copy_constructor(other, **kwargs)
        dc = other.get_dc()
        if not dc or not isinstance(dc, int):
            raise ValueError("Must provide DC (an int)")
        self._dc = dc
        save_type = other.get_save_type()
        if save_type in ["strength", "dexterity", "constitution", "intelligence", "wisdom", "charisma"]:
            self._save_type = save_type
        else:
            raise ValueError("Save type must be strength, dexterity, constitution, intelligence, wisdom, or charisma")
        self._damage_on_success = other.get_damage_on_success()  # half damage on successful save

    def get_dc(self):
        return self._dc

    def get_save_type(self):
        return self._save_type

    def get_damage_on_success(self):
        return self._damage_on_success

    def make_attack(self, source, target, adv=0) -> Optional[int]:
        verbose = source.get_verbose()
        try:
            if verbose:  # pragma: no cover
                print("%s attacks %s with %s." % (source.get_name(), target.get_name(), self.get_name()), end=" ")
            if target.take_saving_throw(self._save_type, self._dc, self):  # pylint: disable=no-else-return
                # take_saving_throw returns True if target made the save
                if verbose:  # pragma: no cover
                    print("%s saves." % target.get_name())
                return self.send_damage(target, saved=True)
            else:
                if verbose:  # pragma: no cover
                    print("%s fails!" % target.get_name())
                return self.send_damage(target)
        except NameError:
            raise ValueError("%s tried to attack something that can't take attacks" % source.get_name())

    def send_damage(self, target, saved=False) -> Optional[int]:  # pylint: disable=arguments-differ
        damage = self.roll_damage()
        if not saved:
            if target.has_feature("evasion") and self._save_type == "dexterity":
                return target.take_damage(damage//2, damage_type=self._damage_type)
            return target.take_damage(damage, damage_type=self._damage_type)
        if self._damage_on_success:
            if target.has_feature("evasion") and self._save_type == "dexterity":
                return 0
            return target.take_damage(damage//2, damage_type=self._damage_type)
        return None

class Spell(Attack):
    """level=None, school=None, casting_time=None, components=None, duration=None,  damage_dice=None,
                 attack_mod=0, damage_mod=0, damage_type="", range=0, melee_range=0, adv=0, name="",
                 copy=None"""
    def __init__(self, **kwargs):
        # print("init Spell")
        copy = kwargs.get("copy")
        if copy:
            self.copy_constructor(copy, **kwargs)
            return
        super().__init__(**kwargs)
        level = kwargs.get("level")
        if isinstance(level, int) and 0 <= level < 10:
            self._level = level
        else:
            raise ValueError("Level must be an integer between 0 and 9")
        school = kwargs.get("school")
        self._school = school  # TODO: validation
        casting_time = kwargs.get("casting_time")
        if casting_time in ["1 action", "1 bonus action", "1 reaction"]:
            self._casting_time = casting_time
        else:
            self._casting_time = time_to_rounds(casting_time)
        components = kwargs.get("components")
        if not isinstance(components, (list, tuple)):
            raise ValueError("Components must be a list or tuple")
        self._components = []
        if "v" in components:
            self._components.append("v")
        if "s" in components:
            self._components.append("s")
        if "m" in components:
            self._components.append("m")
        duration = kwargs.get("duration")
        self._duration = time_to_rounds(duration)

    def copy_constructor(self, other, **kwargs):
        super().copy_constructor(other, **kwargs)
        self._level = other.get_level()
        self._school = other.get_school()  # TODO: validation
        self._casting_time = other.get_casting_time()
        self._components = other.get_components().copy()
        self._duration = other.get_duration()

    def get_level(self) -> int:
        return self._level

    def get_school(self) -> Optional[str]:
        return self._school

    def get_casting_time(self) -> int:
        return self._casting_time

    def get_components(self) -> list:
        return self._components

    def get_duration(self) -> int:
        return self._duration

    def make_attack(self, source, target, adv=0, level=None) -> int:  # pylint: disable=arguments-differ
        if level is None:
            level = self.get_level()
        elif level < self.get_level():
            raise ValueError("%s cannot cast a spell at a lower level than the spell's original level" % source.get_name())
        try:
            source.spend_spell_slot(level, self)
        except ValueError:
            if source.get_verbose():  # pragma: no cover
                raise ValueError("%s tried to cast a level %d spell even though they have no slots left for it" % (source.get_name(), level))
        return super().make_attack(source=source, target=target, adv=adv)  # To be overridden in subclasses

class SavingThrowSpell(Spell, SavingThrowAttack):
    """level=None, school=None, casting_time=None, components=None, duration=None,
                 dc=None, save_type="", damage_on_success=False, damage_dice=None, damage_mod=0,
                 damage_type="", range=0, melee_range=0, adv=0, name="", copy=None"""
    def __init__(self, **kwargs):
        copy = kwargs.get("copy")
        if copy:
            self.copy_constructor(copy, **kwargs)
            return
        super().__init__(**kwargs)

    def copy_constructor(self, other, **kwargs):
        super().copy_constructor(other, **kwargs)

class HealingSpell(Spell):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def make_attack(self, source, target, adv=0, level=None) -> int:
        verbose = source.get_verbose()
        if level is None:
            level = self._level
        elif level < self.get_level():
            raise ValueError("%s cannot cast a spell at a lower level than the spell's original level" % source.get_name())
        try:
            source.spend_spell_slot(level, self)
        except ValueError:
            if source.get_verbose():  # pragma: no cover
                raise ValueError("%s tried to cast a level %d spell even though they have no slots left for it" % (source.get_name(), level))
        if verbose:  # pragma: no cover
            print("%s heals %s" % (source.get_name(), target.get_name()))
        healing = self.roll_damage()
        return target.take_healing(healing)
