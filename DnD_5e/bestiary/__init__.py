from DnD_5e import combatant

# pylint: disable=pointless-string-statement

class Aboleth(combatant.Creature):
    def __init__(self, **kwargs):
        default_kwargs = {"size": "large", "creature_type": "aberration",
            "ac": 17, "max_hp": 135, "hit_dice": "18d10", "speed": 10,
            "strength": 21, "dexterity": 9, "constitution": 15, "intelligence": 18, "wisdom": 15, "charisma": 18}
        default_kwargs.update({"proficiencies": {'intelligence', 'constitution', 'wisdom'}})
        default_kwargs.update({'vision': "darkvision", 'cr': 10})
        # the following lines are things that may be features or attacks. Refer to the original source to see what you want to incorporate.
        """
        Amphibious
        Mucous Cloud
        Probing Telepathy
        Multiattack
        attack_class.Attack(dice=2d6, attack_mod=9, damage_mod=5, damage_type=bludgeoning, melee_range=10, range=0, name=Tentacle)  # there is more to this attack. Refer to the original source
        attack_class.Attack(dice=3d6, attack_mod=9, damage_mod=5, damage_type=bludgeoning, melee_range=10, range=0, name=Tail)
        Enslave (3/Day)
        Detect
        Tail Swipe
        Psychic Drain (Costs 2 Actions)
        """
        kwargs.update(default_kwargs)
        super().__init__(**kwargs)
