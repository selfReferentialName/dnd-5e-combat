from bs4 import BeautifulSoup

soup = BeautifulSoup(open("nosetests.html"), "html.parser")
soup = soup.body.find_all("section")[1].h2
text = soup.get_text()
fail_text = text.split("(")[1].split(" failures")[0]
fail_num = int(fail_text)
if fail_num != 0:
    raise ValueError("Nosetests failed %d tests" % fail_num)
